#!/usr/bin/env python

import sys, csv, json

csvFileInp = sys.stdin
jsonFileOut = sys.stdout

dialect = "excel-tab"
dialect_options = {}

hasHeader = True

csvFile = csv.reader(csvFileInp,dialect=dialect,**dialect_options)


res = { "aoColumns" : [],
        "aaData" : []
      }


if hasHeader:
    row = csvFile.next()
    res["aoColumns"] = [ {"sTitle" : x} for x in row ]

for row in csvFile:
    res["aaData"].append(row)

json.dump(res,jsonFileOut)

