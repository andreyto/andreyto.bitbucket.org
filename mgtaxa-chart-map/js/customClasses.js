/**
 * Created by IntelliJ IDEA.
 * User: hkim
 * Date: 2/27/12
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
//--------------ChartBox START-----------------------
OpenLayers.Marker.ChartBox = OpenLayers.Class(OpenLayers.Marker.Box, {
    chartValues: null,
    sampleId: null,
    isz: null, ipx: null,
	ci: null,
    env:null,
    setChartData: function(vals, id, ci, env) {
        this.chartValues=vals;
        this.sampleId=id;
        this.ci=ci;
        this.env=env;
    },
    setbg: function(ei, cgmax) {
        if(ei==='-')
            this.div.style.background='';
        else {
            var clr = $.xcolor.gradientlevel('#ffa500', '#ff0000', this.env[ei], cgmax).getRGB();
            this.div.style.background="rgba("+clr.r+", "+clr.g+","+clr.b+", 0.3)";
        }
    },
	setBorder: function (color, width) {
    },
    draw: function (px, sz) {
		if(this.isz==null) {
			this.isz=sz;
            this.ipx=px;
        }

		//retain initial size
		if(sz.w<=this.isz.w || sz.h<=this.isz.h) {
			sz.w=(sz.w+this.isz.w)/2;
			sz.h=(sz.h+this.isz.h)/2;
		} 
		else {
			sz.w=(Math.sqrt(sz.w+this.isz.w))+this.isz.w;
			sz.h=(Math.sqrt(sz.h+this.isz.h))+this.isz.h;
            //Horizontally append chart to previous one if there are more than one chart exist at the same location
            if(this.ci>0) {
                px.x=px.x+sz.w*this.ci;
            }
		}
			
        OpenLayers.Util.modifyDOMElement(this.div, null, px, sz);

        if(this.chartValues!=null) {
            var r=Raphael(this.div),
                c=r.barchart(0,0, sz.w, sz.h, this.chartValues,{type:"square",stretch:true});
            if(sz.w>=80)
                r.text(60, 5, this.sampleId).attr({ font: "12px sans-serif", "font-weight": "regular", "fill": "#333333"});
        }
        return this.div;
    },
    display: function (display) {
        this.div.style.display = (display) ? "" : "none";
    },
    checkRedraw: function () {
        var redraw = false;
        if (!this.layerStates.length || (this.map.layers.length != this.layerStates.length)) {
            redraw = true;
        } else {
            for (var i = 0, len = this.layerStates.length; i < len; i++) {
                var layerState = this.layerStates[i];
                var layer = this.map.layers[i];
                if ((layerState.name != layer.name) || (layerState.inRange != layer.inRange) || (layerState.id != layer.id) || (layerState.visibility != layer.visibility)) {
                    redraw = true;
                    break;
                }
            }
        }
        return redraw;
    },
    CLASS_NAME: "OpenLayers.Marker.ChartBox"
});

function drawbg(boxes) {

}
