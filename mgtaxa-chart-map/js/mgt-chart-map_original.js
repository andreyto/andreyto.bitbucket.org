/*!
 * MGTAXA JavaScript Library v1.0
 * http://andreyto.github.com/mgtaxa/
 *
 * Copyright 2011, J. Craig Venter Institute
 * Licensed under the GPL Version 3 license.
 *
 */

//namespace as a self-executing function
var mgt = (function() {
    //	/ This code uses Google Map API and Flot JavaScript libraries
	
	//map
	Map = function(){
		this.map = new Object();
	};   
	Map.prototype = {   
		put : function(key, value){   
			this.map[key] = value;
		},   
		get : function(key){   
			return this.map[key];
		},
		containsKey : function(key){    
			return key in this.map;
		},
		remove : function(key){    
			delete this.map[key];
		},
		keys : function(){   
			var keys = new Array();   
			for(var prop in this.map){   
				keys.push(prop);
			}   
			return keys;
		},
		size : function(){
			var count = 0;
			for (var prop in this.map) {
				count++;
			}
			return count;
		}
	};

    function setDefaults(options, defaultOptions) {
        for ( var index in defaultOptions) {
            if (typeof options[index] == "undefined")
                options[index] = defaultOptions[index];
        }
    }

    function assert(condition, msg) {
        if (!condition) {
            $.error(msg);
        }
    }

    function log(message) {
        if (window.console) {
            console.log(message);
        }
    }

    function dataTableFromJSON(selObjTbl, url, optsTbl, type) {
        $.getJSON(url, function(data) {
			var map = new Map();
			$.each(data.aaData, function(k_1, v_1) {
				map.put(v_1[0], v_1);
			});
			
            setDefaults(data, optsTbl);
            var oTables = selObjTbl.dataTable(data);
			
			TableMaps.prototype.setTypeData(type, map, oTables);
        });
    }

    var TableMaps = function(opts) {
        opts = opts || {};
        var defOpts = {
            "cont" : $("body")
        };
        setDefaults(opts, defOpts);
        assert(opts.cont);

        this.cont = $(opts.cont);
        this.initDOM();
        this.initTabs();
    }

    TableMaps.prototype.initDOM = function() {

        var c = this.cont;
        var ht =
                '<div id="tabs"> ' +
                '   <ul> ' +
                '       <li><a href="#tab_env"><span>Environmental Data</span></a></li> ' +
                '       <li><a href="#tab_taxa"><span>Taxonomic composition</span></a></li> ' +
                '       <li><a href="#tab_map"><span>Map</span></a></li> ' +
                '   </ul> ' +
                '   <div id="tab_env"></div> ' +
                '   <div id="tab_taxa"></div> ' +
                '   <div id="tab_map"> ' +
                '       <div id="map_canvas"></div> ' +
                '   </div> ' +
                '</div> ';
        c.append(ht);
    }

    TableMaps.prototype.initTabs = function() {
		var $curr = this;
		
        $('#tabs').tabs( {
			spinner: '',
			load: function() {
			},
            select: function(event, ui) {
				var tabID = "#ui-tabs-" + (ui.index + 1);
				$(tabID).html("<b>Fetching Data.... Please wait....</b>");
            }
        });
		
        $curr.initEnvTbl();
        $curr.initTaxaTbl();
        $curr.initGMap();
		
        $('#tabs').bind('tabsshow', function(event, ui) {
            $curr.maps.get('env_dt').fnAdjustColumnSizing(true);
            $curr.maps.get('taxa_dt').fnAdjustColumnSizing(true);
        });
    }
	
	TableMaps.prototype.setTypeData = function(type, map, dtable) {
		if( this.maps === undefined )
			this.maps = this.maps||new Map();
		var maps = this.maps;
		maps.put(type, map);
		maps.put(type+"_dt", dtable);
	}

    TableMaps.prototype.initEnvTbl = function() {
        this
            .findChild('#tab_env')
            .html(
            '<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_env"></table>');
        optsTbl = {
            "bJQueryUI" : true,
            "bProcessing" : true,
            "sScrollX" : "100%",
            "bScrollCollapse" : true
        };
        dataTableFromJSON(this.findChild("#tbl_env"), 'data/pred-taxa-env_genus.json', optsTbl, 'env');
    }

    TableMaps.prototype.initTaxaTbl = function() {
        this
            .findChild('#tab_taxa')
            .html(
            '<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_taxa"></table>');
        optsTbl = {
            "bJQueryUI" : true,
            "bProcessing" : true,
            "sScrollX" : "100%",
            "bScrollCollapse" : true
        };
        dataTableFromJSON(this.findChild("#tbl_taxa"), 'data/pred-taxa-pivot_family.json', optsTbl, 'taxa');
    }



    /**
     * Data for the markers consisting of a name, a LatLng and a zIndex for
     * the order in which these markers should display on top of each
     * other.
     */
    var beaches = [ [ 'Bondi Beach', -33.890542, 151.274856, 4 ],
        [ 'Coogee Beach', -33.923036, 151.259052, 5 ],
        [ 'Cronulla Beach', -34.028249, 151.157507, 3 ],
        [ 'Manly Beach', -33.80010128657071, 151.28747820854187, 2 ],
        [ 'Maroubra Beach', -33.950198, 151.259302, 1 ] ];

    TableMaps.prototype.initGMap = function() {
        // height should be specific number for GMap to show up
        jqMapCanv = this.findChild('#map_canvas');
        jqMapCanv.attr('style', 'width:100%;height:900px;');
        var myOptions = {
            zoom : 10,
            center : new google.maps.LatLng(-33.9, 151.2),
            mapTypeId : google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(jqMapCanv.get(0), myOptions);

        this.map = map;

        setChartOverlays(map, beaches);

        // w/o the event handler below the GMap will not fill
        // entire tab space
        this.findChild('#tabs').bind('tabsshow', function(event, ui) {
            if (ui.panel.id == "tab_map") {
                google.maps.event.trigger(map, 'resize');
                map.setZoom(map.getZoom());
            }
        });
    }

    /// for closure to bind correct marker and inforwindow instances,
    /// this has to be a separate function
    function attachInfoWinChart(map, marker, chartUrl) {
        var infoWinChartStr = '<div id="content">' + '<div id="siteNotice">'
            + '</div>'
            + '<h1 id="firstHeading" class="firstHeading">Chart</h1>'
            + '<div id="bodyContent">' + '<img src="' + chartUrl
            + '" alt=""/>' + '</div>' + '</div>';
        var infoWinChartStr = '<img src="' + chartUrl + '" alt=""/>';

        var infoWinChart = new google.maps.InfoWindow({
            content : infoWinChartStr
        });
        google.maps.event.addListener(marker, 'click', function() {
            infoWinChart.open(map, marker);
        });
    }

    TableMaps.prototype.findChild = function(expr) {
        return this.cont.find(expr);
    }

    function drawChart(div, data) {
        $.plot(div, data, {
            series : {
                pie : {
                    show : true,
                    label : {
                        show : false
                    },
                    background : {
                        opacity : 0.5
                    },
                    fill : 0.5,
                    fillColor : {
                        colors : [ {
                            opacity : 0.5
                        }, {
                            opacity : 0.1
                        } ]
                    },
                    grid : {
                        backgroundColor : {
                            colors : [ {
                                opacity : 0.5
                            }, {
                                opacity : 0.1
                            } ]
                        }
                    }
                },
                highlight : {
                    opacity : 0.5
                }
            },
            legend : {
                show : false
            }
        });
    }

    function setChartOverlays(map, locations) {
        boundsStep = Math
            .abs((locations[locations.length - 1][1] - locations[0][1])
            / locations.length);
        var data = [];
        for ( var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var swBound = new google.maps.LatLng(beach[1], beach[2]);
            // LatLng will wrap if necessary
            var neBound = new google.maps.LatLng(beach[1] + boundsStep,
                beach[2] + boundsStep);
            var bounds = new google.maps.LatLngBounds(swBound, neBound);
            var series = Math.floor(Math.random() * 10) + 1;
            data[i] = [];
            for ( var j = 0; j < series; j++) {
                data[i][j] = {
                    label : "Series" + (j + 1),
                    data : Math.floor(Math.random() * 100) + 1
                }
            }
            var chartOv = new ChartOverlay("ov_" + i, bounds, map, data[i],
                drawChart);
            // attachInfoWinChart(map,marker,chartUrl);
        }
    }

    //	/ An object that is a custom overlay on Google Map
    //	/ (derives from google.maps.OverlayView())
    //	/ and holds a chart that is positioned according to
    //	/ a coordinate point and zooms with the map zoom.

    function ChartOverlay(ovId, bounds, map, data, drawChart) {

        // Now initialize all properties.
        this.ovId_ = ovId;
        this.bounds_ = bounds;
        this.map_ = map;
        this.data_ = data;
        this.drawChart_ = drawChart;

        // We define a property to hold the chart's
        // div. We'll actually create this div
        // upon receipt of the add() method so we'll
        // leave it null for now.
        this.div_ = null;

        // Explicitly call setMap() on this overlay
        this.setMap(map);
    }

    ChartOverlay.prototype = new google.maps.OverlayView();

    ChartOverlay.prototype.onAdd = function() {

        // Note: an overlay's receipt of onAdd() indicates that
        // the map's panes are now available for attaching
        // the overlay to the map via the DOM.

        // Create the DIV and set some basic attributes.
        var div = document.createElement('div');
        div.setAttribute("id", 'div_' + this.ovId);
        div.style.border = "none";
        div.style.borderWidth = "0px";
        div.style.position = "absolute";

        // Set the overlay's div_ property to this DIV
        this.div_ = div;

        // We add an overlay to a map via one of the map's panes.
        // We'll add this overlay to the overlayImage pane.
        var panes = this.getPanes();
        //panes.overlayLayer.appendChild(div);
        panes.overlayMouseTarget.appendChild(div);
        var that = this;
        google.maps.event.addDomListener(div, 'click', function() {
            // from [http://stackoverflow.com/questions/3361823/make-custom-overlay-clickable-google-maps-api-v3]
            google.maps.event.trigger(that, 'click');
            alert("Clicked");
        });
    }

    ChartOverlay.prototype.draw = function() {

        // Size and position the overlay. We use a southwest and northeast
        // position of the overlay to peg it to the correct position and
        // size.
        // We need to retrieve the projection from this overlay to do this.
        var overlayProjection = this.getProjection();

        // Retrieve the southwest and northeast coordinates of this overlay
        // in latlngs and convert them to pixels coordinates.
        // We'll use these coordinates to resize the DIV.
        var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_
            .getSouthWest());
        var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_
            .getNorthEast());

        // Resize the charts's DIV to fit the indicated dimensions.
        var div = this.div_;
        div.style.left = sw.x + 'px';
        div.style.top = ne.y + 'px';
        div.style.width = (ne.x - sw.x) + 'px';
        // html coords are from top-left
        div.style.height = (sw.y - ne.y) + 'px';
        this.drawChart_(div, this.data_);
    }

    ChartOverlay.prototype.onRemove = function() {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }

    return {
        ChartOverlay : ChartOverlay,
        setChartOverlays : setChartOverlays,
        TableMaps : TableMaps
    }

    //	end of namespace definition
})();
