/*!
 * MGTAXA JavaScript Library v1.0
 * http://andreyto.github.com/mgtaxa/
 *
 * Copyright 2011, J. Craig Venter Institute
 * Licensed under the GPL Version 3 license.
 *
 */

//namespace as a self-executing function
var mgt = (function() {
    //	/ This code uses Google Map API and Flot JavaScript libraries

    //--Key-Value Pair Def---------------------------
    KVPMap = function(){
        this.kvpmap = new Object();
    }
    KVPMap.prototype = {
        put : function(key, value){
            this.kvpmap[key] = value;
        },
        get : function(key){
            return this.kvpmap[key];
        },
        containsKey : function(key){
            return key in this.kvpmap;
        },
        remove : function(key){
            delete this.kvpmap[key];
        },
        keys : function(){
            var keys = [];
            for(var prop in this.kvpmap){
                keys.push(prop);
            }
            return keys;
        },
        size : function(){
            var count = 0;
            for (var prop in this.kvpmap) {
                count++;
            }
            return count;
        }
    }

    function setDefaults(options, defaultOptions) {
        for ( var index in defaultOptions) {
            if (typeof options[index] == "undefined")
                options[index] = defaultOptions[index];
        }
    }

    function isNull(data) {
        return typeof data==='undefined'||data==null;
    }

    function transformLonLat(lon,lat) {
        return new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"),new OpenLayers.Projection("EPSG:900913"));
    }

    function dataTableFromJSON(selObjTbl, url, optsTbl, type, dataMap) {
        //build dataMap with given json data
        //KVPMap("c_e":[env columns], "c_t:[taxa columns], "s":[KVPMap(sampleId:KVPMap(k,v))]
        var regex=/^[ 0-9.]*$/;
        $.getJSON(url, function(data) {
            var c=[],d,t,id;
            $.each(data.aoColumns, function(i1, v1) {
                c.push(v1.sTitle);
            });
            dataMap.put('c_'+type, c);
            if(!dataMap.containsKey('s'))
                dataMap.put('s', new KVPMap());

            $.each(data.aaData, function(i1, v1) {
                id=v1[0];
                if(type==='env') { //env
                    d=new KVPMap();
                    $.each(c, function(i2,v2) {
                        t=v1[i2];
                        d.put(v2, (regex.test(t)?parseFloat(t):t));
                    });
                    d.put('taxa',[]);
                    dataMap.get('s').put(id, d);
                } else { //taxa
                    d=[];
                    $.each(v1, function(i2,v2) {
                        if(i2>0)
                            d.push(parseInt(v2));
                    });
                    id=id.split('_');
                    d.push(id[1]+"_"+id[2]);
                    dataMap.get('s').get(id[0]).get(type).push(d);
                }
            });
            setDefaults(data, optsTbl);
            selObjTbl.dataTable(data);
        });
    }

    //-----------------------------------------------
    //--TABLEMAPS START------------------------------------
    //-----------------------------------------------
    var TableMaps = function(opts) {
        opts = opts || {};
        var defOpts = {"cont" : $("body")};
        setDefaults(opts, defOpts);

        this.c_loc=['Latitude_DD','Longitute_DD'];

        this.cont = $(opts.cont);
        this.dataMap = new KVPMap();
        this.initDOM();
        this.initTabs();
    }

    TableMaps.prototype.initDOM = function() {

        var c = this.cont;
        var ht =
            '<div id="loading" style="display:none;">' +
                '   <p><img src="images/ajax-loader.gif" />Loading...</p>' +
                '</div>' +
                '<div id="tabs"> ' +
                '   <ul> ' +
                '       <li><a href="#tab_env"><span>Environmental Data</span></a></li> ' +
                '       <li><a href="#tab_taxa"><span>Taxonomic composition</span></a></li> ' +
                '       <li><a href="#tab_map"><span>Map</span></a></li> ' +
                '   </ul> ' +
                '   <div id="tab_env"></div> ' +
                '   <div id="tab_taxa"></div> ' +
                '   <div id="tab_map"> ' +
                '          <div id="map_canvas" class="smallmap">' +
                //'               <div id="labelLegend"></div>'+
                '          </div> ' +
                '   </div> ' +
                '</div> ';
        c.append(ht);
    }

    TableMaps.prototype.initTabs = function() {
        var $c = this;

        $c.initTbl('env', 'data/pred-taxa-env_genus.json');
        $c.initTbl('taxa', 'data/pred-taxa-pivot_family.json');

        $('#tabs').tabs( {
            event: 'click',
            show: function(event, ui) {
                if(ui.panel.id==='tab_map') {
                    $("div#map_canvas").html("");
                    $c.initMap();
                } else {
                    var oTable = $('div.dataTables_scrollBody>table.display', ui.panel).dataTable();
                    if(oTable.length>0) {
                        oTable.fnAdjustColumnSizing();
                    }
                }
                // $curr.findChild('#loading').hide();
            },
            spinner: '',
            load: function() {
            },
            select: function(event, ui) {
                // $curr.findChild('#loading').show();
            }
        });
    }

    TableMaps.prototype.initTbl = function(type, jsonFile) {
        this
            .findChild('#tab_'+type)
            .html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_'+type+'"></table>');
        optsTbl = {
            "bJQueryUI" : true,
            "bProcessing" : true,
            "sScrollX" : "100%",
            "bScrollCollapse" : true
        };
        dataTableFromJSON(this.findChild("#tbl_"+type), jsonFile, optsTbl, type, this.dataMap);
    }

    TableMaps.prototype.findChild = function(expr) {
        return this.cont.find(expr);
    }

    //-----------------------------------------------
    //--MAP START------------------------------------
    //-----------------------------------------------
    TableMaps.prototype.initMap = function() {
        var jqMapCanv, map, mapData, initZoomLevel=4;
        jqMapCanv = this.findChild("#map_canvas");
        jqMapCanv.attr("style", "width:100%;height:500px;");

        //get map data
        mapData=this.initMapData();
        console.log(mapData);

        var options = {
            theme: null,
            //controls: [],  // Remove all controls,
            units: "m",
            projection: new OpenLayers.Projection("EPSG:900913"),
            displayProjection: new OpenLayers.Projection("EPSG:4326")
        };

        map = new OpenLayers.Map( 'map_canvas', options );
        var ghyb = new OpenLayers.Layer.Google("Google Hybrid",{type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20}),
            gmap = new OpenLayers.Layer.Google("Google", {}),
            boxes  = new OpenLayers.Layer.Boxes("Taxonomic Chart");
        map.addLayers([gmap, ghyb, boxes]);

        var lonLat,ext,bounds, box, minx, miny, maxx, maxy, charts=[], cgmax=[]; //, ddl=[]
        $.each(mapData[0], function(i1,v1) {
            ext=[];
            lonLat=transformLonLat(v1[1][1], v1[1][0]);
            ext.push(lonLat.lon, lonLat.lat);
            //get min and max of longitude, latitude for centering the map
            if(i1===0) {
				minx=lonLat.lat; maxx=minx; miny=lonLat.lon; maxy=miny; //initial max&min lon&lat
                $.each(v1[2], function(i2,v2){ //check if env data is number type for dropdown list
                    //ddl[i2]=(typeof v2==='number');
                    cgmax[i2]=(typeof v2==='number'?v2:-1);
                });
			} else {
                minx=minx<lonLat.lat?minx:lonLat.lat;
                maxx=maxx<lonLat.lat?lonLat.lat:maxx;
                miny=miny<lonLat.lon?miny:lonLat.lon;
                maxy=maxy<lonLat.lon?lonLat.lon:maxy;
                //get max environmental value for chart background color gradient
                $.each(cgmax, function(i3, v3) {
                    if(v3!==-1) {
                        cgmax[i3]=v3<v1[2][i3]?v1[2][i3]:v3;
                    }
                });
            }
            //box size
            lonLat=transformLonLat(v1[1][1]+5, v1[1][0]+2);
            ext.push(lonLat.lon, lonLat.lat);
            //draw chart box
            $.each(v1[3], function(i2,v2) {
                bounds = OpenLayers.Bounds.fromArray(ext);
                box = new OpenLayers.Marker.ChartBox(bounds);
                box.setChartData(v2[1], v2[0], i2, v1[2]);
                boxes.addMarker(box);
                charts.push(box);
            });
        });

        //set center
        map.setCenter(new OpenLayers.LonLat(((miny+maxy)/2),((minx+maxx)/2)), initZoomLevel);

        var overview = new OpenLayers.Control.OverviewMap({
            maximized: true
        });
        map.addControl(overview);
        map.addControl(new OpenLayers.Control.LayerSwitcher());
        map.addControl(new OpenLayers.Control.Navigation());

        //chart legends
        jqMapCanv.append('<div id="labelLegend"></div>');
        var paper = Raphael("labelLegend", 177, 170),
            lcx=10, lr=7, tx=20, y,
            textattr={font:"12px sans-serif","font-weight":"regular","fill":"#333333","text-anchor":"start"},
            colors=Raphael.g.colors;
        $.each(mapData[1], function(i1,v1) {
            y=15*(i1+1);
            paper.circle(lcx,y,lr).attr({"fill":colors[i1], "stroke":"#fff"});
            paper.text(tx,y, v1).attr(textattr);
        });

        console.log(cgmax);
        //background select box
        var bs='<select id="bgSelectBox"><option value="-">select env field</option>',
            c_loc=this.c_loc;
        $.each(mapData[2], function(i1,v1) {
            if(c_loc.indexOf(v1)==-1 && cgmax[i1]!==-1) //ddl[i1]===true) //only number type fields (exclude lon&lat)
                bs+='<option value="'+i1+'">'+v1+'</option>';
        });
        bs+='</select>'
        this.findChild('#labelLegend').append('<div id="bgSelectDiv">'+bs+'</div>');
        this.findChild('#bgSelectDiv').change(function() {
            var so=$(this).find('option:selected');
            //if(so.val()!=='-') {
                $.each(charts, function(i1,v1) {
                    v1.setbg(so.val(), cgmax[so.val()]);
                });
            //}
        });
    }

    TableMaps.prototype.initMapData = function() {
        var dataMap = this.dataMap,
            max_c=10, //max number of columns on chart
            k, mapData, c_loc=this.c_loc;
        if(!isNull(dataMap)) {
            k=dataMap.keys();
            if(!isNull(k)&&k.length===3) {
                var c_t=dataMap.get('c_taxa'),s=dataMap.get('s'),
                    nc_t=c_t.length,l=s.size(),
                    e1=l/2,e2=(l/2)-1,o1=(l-1)/2,is_e=(l%2===0),
                    d, t, tarr, m, med=[];
                //gets median of each taxanomic composition then grabs top n(max_c)
                for(var i=0;i<nc_t;i++) {
                    tarr=[];
                    $.each(s.keys(), function(i1,v1) {
                        d=s.get(v1);
                        $.each(d.get('taxa'), function(i2,v2) {
                            tarr.push(v2[i]);
                        })
                    });
                    tarr.sort(function(a,b){return b-a;});
                    m=(is_e?((tarr[e1]+tarr[e2])/2):tarr[o1]);
                    med.push([i,m]);
                }
                med = med.sort(function(a,b){return b[1]-a[1];}).slice(0,max_c);

                //building map data [[samples],[env_columns],[taxa columns(max_c)]]
                mapData=[]; m=[];
                var stc=[], envd;
                $.each(s.keys(), function(i1,v1) {
                    d=s.get(v1); tarr=[], k=d.keys(); envd=[];
                    $.each(k, function(i2,v2) {
                        if(v2==='taxa') {
                            $.each(d.get(v2), function(i3,v3) {
                                t=[];
                                $.each(med, function(i4,v4) {
                                    if(i1===0)
                                        stc.push(c_t[v4[0]]);
                                    t.push(v3[v4[0]]);
                                });
                                tarr.push([v3[nc_t-1],t]);
                            });
                        } else {
                            envd.push(d.get(v2));
                        }
                    });

                    m.push([v1, [d.get(c_loc[0]),d.get(c_loc[1])], envd, tarr]); //[sampleid, [lat,lon], [[taxaid,[chartValues]]..]]
                });
                mapData.push(m, stc, dataMap.get('c_env'));
            }
        }
        return mapData;
    }
    //-----------------------------------------------
    //--MAP END--------------------------------------
    //-----------------------------------------------

    return {
        TableMaps : TableMaps
    }
    //	end of namespace definition
})();
