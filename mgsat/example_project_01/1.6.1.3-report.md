% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



#### \(1.6.1.3\) Taxonomic level: 6 of Subset: Patients samples



Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


Loaded OTU taxonomy file [`example_cons.taxonomy.file`](example_cons.taxonomy.file).


Loaded 100 records for 156 features from count file [`example_otu.shared.file`](example_otu.shared.file) for taxonomic level 6 
                       with taxa name sanitize setting TRUE with count basis seq with taxa count source shared


Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 156 features


After merging with metadata, 100 records left


After aggregating/subsetting, sample count is: 78


Filtering abundance matrix with arguments [ min_row_sum:2000, max_row_sum:4e+05]. sample


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 67 records for 147 features


**Wrote counts and metadata for raw counts Patients samples to files [`data/1.6.1.3.1-935623f24edsamples.raw.16s.l.6.count.tsv`](data/1.6.1.3.1-935623f24edsamples.raw.16s.l.6.count.tsv),[`data/1.6.1.3.1-935623f24edsamples.raw.16s.l.6.attr.tsv`](data/1.6.1.3.1-935623f24edsamples.raw.16s.l.6.attr.tsv)**


**Wrote counts and metadata for proportions counts Patients samples to files [`data/1.6.1.3.1-9351f9175ffsamples.proportions.16s.l.6.count.tsv`](data/1.6.1.3.1-9351f9175ffsamples.proportions.16s.l.6.count.tsv),[`data/1.6.1.3.1-9351f9175ffsamples.proportions.16s.l.6.attr.tsv`](data/1.6.1.3.1-9351f9175ffsamples.proportions.16s.l.6.attr.tsv)**



##### \(1.6.1.3.2\) Data analysis




##### \(1.6.1.3.2.1\) Abundance and diversity estimates Before count filtering

[`Subreport`](./1.6.1.3.2.1-report.html)

Filtering abundance matrix with arguments [ min_mean                :10, min_quant_incidence_frac:0.25, min_quant_mean_frac     :0.25]. feature


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 67 records for 43 features



##### \(1.6.1.3.2.2\) Abundance and diversity estimates After count filtering

[`Subreport`](./1.6.1.3.2.2-report.html)


##### \(1.6.1.3.2.3\) DESeq2 tests and data normalization



Love MI, Huber W and Anders S (2014). “Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2.”
_Genome Biology_, *15*, pp. 550. <URL: http://doi.org/10.1186/s13059-014-0550-8>, <URL:
http://dx.doi.org/10.1186/s13059-014-0550-8>.


\(1.6.1.3.2.3.1\) <a name="table.389"></a>[`Table 389.`](#table.389) DESeq2 results for task:;visit;[ list\(\)];log2 fold change \(MAP\): visit ;Wald test p-value: visit . Full dataset is also saved in a delimited text file [`data/1.6.1.3.2.3.1-9356693d8a6.1.6.1.3.2.3.1.a.nam.tsv`](data/1.6.1.3.2.3.1-9356693d8a6.1.6.1.3.2.3.1.a.nam.tsv)




|  &nbsp;  | feature                              | baseMean   | log2FoldChange   | lfcSE   | stat     | pvalue    | padj     |
|:--------:|:-------------------------------------|:-----------|:-----------------|:--------|:---------|:----------|:---------|
|  **1**   | Haemophilus                          | 7.3498     | 1.311624         | 0.3217  | 4.07778  | 4.547e-05 | 0.001955 |
|  **2**   | Roseburia                            | 43.4829    | 0.606056         | 0.1646  | 3.68301  | 2.305e-04 | 0.004956 |
|  **3**   | Lachnospiracea\_incertae\_sedis      | 94.8413    | 0.663742         | 0.1951  | 3.40251  | 6.677e-04 | 0.009570 |
|  **4**   | Unclassified\_Bacteria               | 19.9582    | -0.536914        | 0.1663  | -3.22781 | 1.247e-03 | 0.011334 |
|  **5**   | Unclassified\_Clostridiaceae\_1      | 3.3608     | 1.109940         | 0.3456  | 3.21204  | 1.318e-03 | 0.011334 |
|  **6**   | Unclassified\_Lachnospiraceae        | 168.8940   | 0.434386         | 0.1415  | 3.07022  | 2.139e-03 | 0.015330 |
|  **7**   | Prevotella                           | 62.8114    | -1.024878        | 0.3628  | -2.82511 | 4.727e-03 | 0.028921 |
|  **8**   | Enterococcus                         | 21.9362    | -1.323776        | 0.4758  | -2.78200 | 5.402e-03 | 0.028921 |
|  **9**   | Unclassified\_Bacteroidales          | 29.2481    | -0.688655        | 0.2509  | -2.74488 | 6.053e-03 | 0.028921 |
|  **10**  | Unclassified\_Ruminococcaceae        | 30.6560    | -0.418163        | 0.1710  | -2.44517 | 1.448e-02 | 0.062257 |
|  **11**  | Unclassified\_Burkholderiales        | 1.7914     | -1.319846        | 0.5497  | -2.40111 | 1.635e-02 | 0.063896 |
|  **12**  | Klebsiella                           | 0.7732     | 0.961791         | 0.4289  | 2.24252  | 2.493e-02 | 0.089325 |
|  **13**  | Akkermansia                          | 2.0895     | -1.138798        | 0.5184  | -2.19676 | 2.804e-02 | 0.092741 |
|  **14**  | Escherichia\_Shigella                | 5.9632     | 0.793135         | 0.3828  | 2.07188  | 3.828e-02 | 0.117564 |
|  **15**  | Alistipes                            | 118.1768   | -0.723093        | 0.3805  | -1.90035 | 5.739e-02 | 0.164511 |
|  **16**  | Gemmiger                             | 6.8963     | -0.512755        | 0.3399  | -1.50849 | 1.314e-01 | 0.353215 |
|  **17**  | Flavonifractor                       | 6.4267     | -0.272385        | 0.1868  | -1.45824 | 1.448e-01 | 0.366195 |
|  **18**  | Turicibacter                         | 1.5159     | -0.521712        | 0.3742  | -1.39413 | 1.633e-01 | 0.390054 |
|  **19**  | Veillonella                          | 7.2031     | 0.390203         | 0.3325  | 1.17370  | 2.405e-01 | 0.519460 |
|  **20**  | Clostridium\_IV                      | 13.8981    | -0.332798        | 0.2842  | -1.17097 | 2.416e-01 | 0.519460 |
|  **21**  | Bacteroides                          | 4119.4125  | -0.243647        | 0.2218  | -1.09867 | 2.719e-01 | 0.556770 |
|  **22**  | Bifidobacterium                      | 5.2115     | 0.301747         | 0.2910  | 1.03705  | 2.997e-01 | 0.585802 |
|  **23**  | Parabacteroides                      | 84.8517    | 0.303215         | 0.3249  | 0.93337  | 3.506e-01 | 0.605302 |
|  **24**  | Blautia                              | 56.0142    | -0.170084        | 0.1827  | -0.93087 | 3.519e-01 | 0.605302 |
|  **25**  | Ruminococcus                         | 19.7981    | 0.211108         | 0.2197  | 0.96110  | 3.365e-01 | 0.605302 |
|  **26**  | Phascolarctobacterium                | 3.9207     | -0.421522        | 0.4714  | -0.89418 | 3.712e-01 | 0.613948 |
|  **27**  | Sutterella                           | 27.1045    | -0.366165        | 0.4591  | -0.79759 | 4.251e-01 | 0.652846 |
|  **28**  | Clostridium\_XVIII                   | 10.9918    | -0.172934        | 0.2140  | -0.80792 | 4.191e-01 | 0.652846 |
|  **29**  | Faecalibacterium                     | 146.9935   | 0.130955         | 0.1797  | 0.72876  | 4.661e-01 | 0.680519 |
|  **30**  | Barnesiella                          | 1.8263     | 0.337747         | 0.4900  | 0.68934  | 4.906e-01 | 0.680519 |
|  **31**  | other                                | 82.9736    | -0.109992        | 0.1544  | -0.71259 | 4.761e-01 | 0.680519 |
|  **32**  | Paraprevotella                       | 1.2634     | 0.330874         | 0.5112  | 0.64729  | 5.174e-01 | 0.695315 |
|  **33**  | Streptococcus                        | 3.6307     | -0.129942        | 0.2190  | -0.59323 | 5.530e-01 | 0.720612 |
|  **34**  | Megamonas                            | 2.8249     | -0.290180        | 0.5524  | -0.52534 | 5.993e-01 | 0.757994 |
|  **35**  | Erysipelotrichaceae\_incertae\_sedis | 12.8870    | -0.091700        | 0.2195  | -0.41785 | 6.761e-01 | 0.830584 |
|  **36**  | Unclassified\_Clostridiales          | 43.3219    | -0.050192        | 0.1593  | -0.31505 | 7.527e-01 | 0.885563 |
|  **37**  | Clostridium\_XI                      | 39.9559    | 0.063972         | 0.2318  | 0.27594  | 7.826e-01 | 0.885563 |
|  **38**  | Unclassified\_Firmicutes             | 13.3942    | 0.059260         | 0.2011  | 0.29463  | 7.683e-01 | 0.885563 |
|  **39**  | Lactobacillus                        | 0.8257     | -0.063974        | 0.4536  | -0.14103 | 8.878e-01 | 0.956500 |
|  **40**  | Clostridium\_XlVa                    | 22.5827    | 0.018812         | 0.1702  | 0.11050  | 9.120e-01 | 0.956500 |
|  **41**  | Clostridium\_sensu\_stricto          | 9.1366     | 0.038926         | 0.3473  | 0.11209  | 9.108e-01 | 0.956500 |
|  **42**  | Odoribacter                          | 2.2667     | 0.035327         | 0.5236  | 0.06746  | 9.462e-01 | 0.968741 |
|  **43**  | Dorea                                | 11.1157    | -0.003748        | 0.2168  | -0.01729 | 9.862e-01 | 0.986208 |




**Count normalization method for data analysis (unless modified by specific methods) : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.clr"]**



##### \(1.6.1.3.2.4\) Stability selection analysis for response ( visit )



\(1.6.1.3.2.4\)  Summary of response variable visit.



```````
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  1.000   1.000   2.000   2.254   3.000   4.000 
```````



Hofner B and Hothorn T (2015). _stabs: Stability Selection with Error Control_. R package version R package version
0.5-1, <URL: http://CRAN.R-project.org/package=stabs>.

Hofner B, Boccuto L and Göker M (2014). “Controlling false discoveries in high-dimensional situations: Boosting with
stability selection.” arXiv:1411.1285. <URL: http://arxiv.org/abs/1411.1285>.


**This multivariate feature selection method implements 
                  stability selection procedure by Meinshausen and Buehlmann (2010) 
                  and the improved error bounds by Shah and Samworth (2013). 
                  The features (e.g. taxonomic features)
                   are ranked according to their probability to be selected
                   by models built on multiple random subsamples of the input dataset.**


**Base selection method parameters that were chosen based on
                         cross-validation are: [ alpha:0.7]**


**All base selection method parameters are: [ family     :"gaussian", standardize:TRUE, alpha      :0.7]**


**Stability selection method parameters are: [ PFER         :0.05, sampling.type:"SS", assumption   :"r-concave", q            :8]**


\(1.6.1.3.2.4\) <a name="table.390"></a>[`Table 390.`](#table.390) Selection probability for the variables. Probability cutoff=0.97 corresponds to per family error rate PFER=0.0431. Full dataset is also saved in a delimited text file [`data/1.6.1.3.2.4-9355c587356.1.6.1.3.2.4.a.name..tsv`](data/1.6.1.3.2.4-9355c587356.1.6.1.3.2.4.a.name..tsv)




|                   &nbsp;                   | Prob(selection)   |
|:------------------------------------------:|:------------------|
|               **Roseburia**                | 0.88              |
|     **Unclassified\_Lachnospiraceae**      | 0.68              |
|                **Gemmiger**                | 0.59              |
|              **Akkermansia**               | 0.51              |
|    **Unclassified\_Clostridiaceae\_1**     | 0.44              |
|     **Unclassified\_Burkholderiales**      | 0.43              |
|                 **Dorea**                  | 0.37              |
|            **Clostridium\_XI**             | 0.35              |
|            **Clostridium\_IV**             | 0.35              |
|         **Unclassified\_Bacteria**         | 0.33              |
|              **Haemophilus**               | 0.30              |
|               **Prevotella**               | 0.26              |
|               **Alistipes**                | 0.22              |
|         **Escherichia\_Shigella**          | 0.19              |
|               **Klebsiella**               | 0.19              |
|         **Phascolarctobacterium**          | 0.17              |
|            **Bifidobacterium**             | 0.16              |
|            **Faecalibacterium**            | 0.14              |
|           **Clostridium\_XVIII**           | 0.14              |
|            **Parabacteroides**             | 0.13              |
|    **Lachnospiracea\_incertae\_sedis**     | 0.13              |
|               **Sutterella**               | 0.12              |
|               **Megamonas**                | 0.12              |
|              **Turicibacter**              | 0.12              |
|             **Lactobacillus**              | 0.10              |
|      **Unclassified\_Bacteroidales**       | 0.10              |
|             **Streptococcus**              | 0.10              |
|                **Blautia**                 | 0.09              |
|  **Erysipelotrichaceae\_incertae\_sedis**  | 0.09              |
|              **Barnesiella**               | 0.07              |
|              **Odoribacter**               | 0.06              |
|     **Unclassified\_Ruminococcaceae**      | 0.05              |
|        **Unclassified\_Firmicutes**        | 0.05              |
|              **Ruminococcus**              | 0.04              |
|              **Veillonella**               | 0.04              |
|             **Paraprevotella**             | 0.04              |
|              **Bacteroides**               | 0.03              |
|           **Clostridium\_XlVa**            | 0.03              |
|             **Flavonifractor**             | 0.03              |
|      **Unclassified\_Clostridiales**       | 0.01              |
|      **Clostridium\_sensu\_stricto**       | 0.01              |
|              **Enterococcus**              | 0.00              |







\(1.6.1.3.2.4\) <a name="figure.1728"></a>[`Figure 1728.`](#figure.1728) Selection probability for the top ranked variables. Probability cutoff=0.97 corresponds to per family error rate PFER=0.0431 (vertical line).  Image file: [`plots/93532fc4277.png`](plots/93532fc4277.png). High resolution image file: [`plots/93532fc4277-hires.png`](plots/93532fc4277-hires.png).
[![](plots/93532fc4277.png)](plots/93532fc4277-hires.png)


##### \(1.6.1.3.2.5\) PermANOVA (adonis) analysis of  normalized counts



Oksanen J, Blanchet FG, Kindt R, Legendre P, Minchin PR, O'Hara RB, Simpson GL, Solymos P, Stevens MHH and Wagner H
(2015). _vegan: Community Ecology Package_. R package version 2.2-1, <URL: http://CRAN.R-project.org/package=vegan>.


**Non-parametric multivariate test for association between
                           normalized counts and meta-data variables. Dissimilarity index is euclidean.**


\(1.6.1.3.2.5\)  Association with visit paired by subject with formula count\~visit with strata =  SubjectID.



```````

Call:
adonis(formula = as.formula(formula_str), data = m_a$attr, permutations = n.perm,      method = dist.metr, strata = if (!is.null(strata)) m_a$attr[,          strata] else NULL) 

Blocks:  strata 
Permutation: free
Number of permutations: 4000

Terms added sequentially (first to last)

          Df SumsOfSqs MeanSqs F.Model      R2   Pr(>F)   
visit      1     480.9  480.95  2.1461 0.03196 0.002749 **
Residuals 65   14566.7  224.10         0.96804            
Total     66   15047.6                 1.00000            
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```````



\(1.6.1.3.2.5\) <a name="table.391"></a>[`Table 391.`](#table.391) Association with visit paired by subject AOV Table. Full dataset is also saved in a delimited text file [`data/1.6.1.3.2.5-9353c05e732.1.6.1.3.2.5.a.name..tsv`](data/1.6.1.3.2.5-9353c05e732.1.6.1.3.2.5.a.name..tsv)




|     &nbsp;      | Df   | SumsOfSqs   | MeanSqs   | F.Model   | R2      | Pr(>F)   |
|:---------------:|:-----|:------------|:----------|:----------|:--------|:---------|
|    **visit**    | 1    | 480.9       | 480.9     | 2.146     | 0.03196 | 0.002749 |
|  **Residuals**  | 65   | 14566.7     | 224.1     | NA        | 0.96804 | NA       |
|    **Total**    | 66   | 15047.6     | NA        | NA        | 1.00000 | NA       |




**Count normalization method for abundance plots : [ drop.features:"other", method       :"norm.prop"]**



##### \(1.6.1.3.2.6\) Plots of Abundance.

[`Subreport`](./1.6.1.3.2.6-report.html)




\(1.6.1.3.2.6\) <a name="figure.1753"></a>[`Figure 1753.`](#figure.1753) Heatmap of abundance profile.  Image file: [`plots/93579e1453f.png`](plots/93579e1453f.png). High resolution image file: [`plots/93579e1453f-hires.png`](plots/93579e1453f-hires.png).
[![](plots/93579e1453f.png)](plots/93579e1453f-hires.png)
