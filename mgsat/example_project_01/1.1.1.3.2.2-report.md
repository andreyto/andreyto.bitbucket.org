% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



##### \(1.1.1.3.2.2\) Abundance and diversity estimates After count filtering



**Counts are rarefied to the lowest library size, abundance-based and
                   incidence-based alpha diversity indices and richness estimates are computed
                   (if requested).
                   This is repeated multiple times (n=400), and the results are averaged.
                   Beta diversity matrix is also computed by averaging over multiple 
                   rarefications. Samples are not grouped.**


Oksanen J, Blanchet FG, Kindt R, Legendre P, Minchin PR, O'Hara RB, Simpson GL, Solymos P, Stevens MHH and Wagner H
(2015). _vegan: Community Ecology Package_. R package version 2.2-1, <URL: http://CRAN.R-project.org/package=vegan>.


\(1.1.1.3.2.2.1\) <a name="table.29"></a>[`Table 29.`](#table.29) Incidence based rihcness estimates and corresponding standard errors for all samples. Full dataset is also saved in a delimited text file [`data/1.1.1.3.2.2.1-9355d27b25f.1.1.1.3.2.2.1.a.nam.tsv`](data/1.1.1.3.2.2.1-9355d27b25f.1.1.1.3.2.2.1.a.nam.tsv)




| Species   | chao   | chao.se   | jack1   | jack1.se   | jack2   | boot   | boot.se   | n   |
|:----------|:-------|:----------|:--------|:-----------|:--------|:-------|:----------|:----|
| 43        | 43     | 0         | 43      | NA         | 43      | 43     | 0.01658   | 87  |







\(1.1.1.3.2.2.1\) <a name="figure.155"></a>[`Figure 155.`](#figure.155) Incidence based rihcness estimates and corresponding standard errors for all samples.  Image file: [`plots/935592faac0.png`](plots/935592faac0.png). High resolution image file: [`plots/935592faac0-hires.png`](plots/935592faac0-hires.png).
[![](plots/935592faac0.png)](plots/935592faac0-hires.png)


##### \(1.1.1.3.2.2.2\) Plots of Abundance-based richness estimates With rarefication.

[`Subreport`](./1.1.1.3.2.2.2-report.html)


##### \(1.1.1.3.2.2.3\) Plots of Abundance-based diversity indices (Hill numbers) With rarefication.

[`Subreport`](./1.1.1.3.2.2.3-report.html)

**Wrote counts and metadata for Abundance based richness and diversity to files [`data/1.1.1.3.2.2.3-93532529945divrich.counts.count.tsv`](data/1.1.1.3.2.2.3-93532529945divrich.counts.count.tsv),[`data/1.1.1.3.2.2.3-93532529945divrich.counts.attr.tsv`](data/1.1.1.3.2.2.3-93532529945divrich.counts.attr.tsv)**





\(1.1.1.3.2.2.3.1\) <a name="figure.228"></a>[`Figure 228.`](#figure.228) Accumulation curves for extrapolated richness indices 
        for random ordering of samples (function poolaccum of package vegan;
             estimation is based on incidence data). Samples were rarefied
             to the the minimum sample size (2161).  Image file: [`plots/9354ccc9ad2.png`](plots/9354ccc9ad2.png). High resolution image file: [`plots/9354ccc9ad2-hires.png`](plots/9354ccc9ad2-hires.png).
[![](plots/9354ccc9ad2.png)](plots/9354ccc9ad2-hires.png)




\(1.1.1.3.2.2.3.1\)  Accumulation curves for extrapolated richness indices 
        for random ordering of samples (function estaccumR of package vegan;
             estimation is based on abundance data). Samples were rarefied
             to the the minimum sample size (2161).





\(1.1.1.3.2.2.3.1\) <a name="figure.229"></a>[`Figure 229.`](#figure.229) Accumulation curve for expected number of species (features)
             for a given number of samples (function specaccum of package vegan,
             using method 'exact'. Samples were rarefied
             to the the minimum sample size (2161).  Image file: [`plots/9353d57707c.png`](plots/9353d57707c.png). High resolution image file: [`plots/9353d57707c-hires.png`](plots/9353d57707c-hires.png).
[![](plots/9353d57707c.png)](plots/9353d57707c-hires.png)

**Computed beta-diversity matrix using function betadiver {vegan}
                   with method 2 "-1" = (b+c)/(2*a+b+c), where number of shared species in two sites is a, 
                      and the numbers of species unique to each site are b and c.**
