% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



##### \(1.4.1.4.2.1.3\) Plots of Abundance-based diversity indices (Hill numbers) With rarefication.



**Plots are shown with relation to various combinations of meta 
                   data variables and in different graphical representations. Lots of plots here.**



##### \(1.4.1.4.2.1.3.2\) Iterating over all combinations of grouping variables




##### \(1.4.1.4.2.1.3.2.1\) Grouping variables DietStatus




##### \(1.4.1.4.2.1.3.2.2\) Iterating over Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order




##### \(1.4.1.4.2.1.3.2.2.1\) Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order: original




##### \(1.4.1.4.2.1.3.2.2.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.4.1.4.2.1.3.2.2.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.4.1.4.2.1.3.2.2.2.1.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.4.1.4.2.1.3.2.2.2.1.1.0\) <a name="table.315"></a>[`Table 315.`](#table.315) Data table used for plots. Data grouped by DietStatus. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.1.3.2.2.2.1.1.0-935238eba9b.1.4.1.4.2.1.3.2.2.2.tsv`](data/1.4.1.4.2.1.3.2.2.2.1.1.0-935238eba9b.1.4.1.4.2.1.3.2.2.2.tsv)




|  &nbsp;  | .record.id   | DietStatus   | feature   | index   |
|:--------:|:-------------|:-------------|:----------|:--------|
|  **1**   | SM1          | before.diet  | N1        | 29.139  |
|  **2**   | SM11         | after.diet   | N1        | 27.795  |
|  **3**   | SM12         | before.diet  | N1        | 27.202  |
|  **4**   | SM13         | before.diet  | N1        | 24.425  |
|  **5**   | SM14         | before.diet  | N1        | 19.408  |
|  **6**   | SM17         | before.diet  | N1        | 20.415  |
|  **7**   | SM19         | after.diet   | N1        | 33.140  |
|  **8**   | SM2          | before.diet  | N1        | 10.793  |
|  **9**   | SM23         | after.diet   | N1        | 2.532   |
|  **10**  | SM3          | after.diet   | N1        | 51.043  |
|  **11**  | SM33         | before.diet  | N1        | 6.630   |
|  **12**  | SM38         | after.diet   | N1        | 15.358  |
|  **13**  | SM41         | before.diet  | N1        | 43.505  |
|  **14**  | SM45         | after.diet   | N1        | 1.021   |
|  **15**  | SM49         | before.diet  | N1        | 17.267  |
|  **16**  | SM52         | before.diet  | N1        | 36.830  |
|  **17**  | SM53         | after.diet   | N1        | 22.762  |
|  **18**  | SM56         | after.diet   | N1        | 6.034   |
|  **19**  | SM57         | before.diet  | N1        | 11.371  |
|  **20**  | SM58         | after.diet   | N1        | 10.158  |
|  **21**  | SM62         | before.diet  | N1        | 7.018   |
|  **22**  | SM64         | after.diet   | N1        | 16.080  |
|  **23**  | SM77         | after.diet   | N1        | 41.015  |
|  **24**  | SM79         | after.diet   | N1        | 25.531  |
|  **25**  | SM8          | before.diet  | N1        | 21.409  |
|  **26**  | SM83         | before.diet  | N1        | 15.798  |
|  **27**  | SM84         | before.diet  | N1        | 52.336  |
|  **28**  | SM86         | after.diet   | N1        | 29.420  |
|  **29**  | SM87         | before.diet  | N1        | 33.974  |
|  **30**  | SM88         | after.diet   | N1        | 24.465  |
|  **31**  | SM9          | after.diet   | N1        | 28.438  |
|  **32**  | SM90         | after.diet   | N1        | 58.528  |
|  **33**  | SM96         | before.diet  | N1        | 15.435  |
|  **34**  | SM1          | before.diet  | N2        | 13.028  |
|  **35**  | SM11         | after.diet   | N2        | 11.703  |
|  **36**  | SM12         | before.diet  | N2        | 10.829  |
|  **37**  | SM13         | before.diet  | N2        | 8.854   |
|  **38**  | SM14         | before.diet  | N2        | 5.659   |
|  **39**  | SM17         | before.diet  | N2        | 8.527   |
|  **40**  | SM19         | after.diet   | N2        | 9.964   |
|  **41**  | SM2          | before.diet  | N2        | 4.634   |
|  **42**  | SM23         | after.diet   | N2        | 1.808   |
|  **43**  | SM3          | after.diet   | N2        | 23.265  |
|  **44**  | SM33         | before.diet  | N2        | 3.637   |
|  **45**  | SM38         | after.diet   | N2        | 8.136   |
|  **46**  | SM41         | before.diet  | N2        | 16.092  |
|  **47**  | SM45         | after.diet   | N2        | 1.006   |
|  **48**  | SM49         | before.diet  | N2        | 9.501   |
|  **49**  | SM52         | before.diet  | N2        | 17.185  |
|  **50**  | SM53         | after.diet   | N2        | 9.492   |
|  **51**  | SM56         | after.diet   | N2        | 2.202   |
|  **52**  | SM57         | before.diet  | N2        | 5.109   |
|  **53**  | SM58         | after.diet   | N2        | 4.101   |
|  **54**  | SM62         | before.diet  | N2        | 3.262   |
|  **55**  | SM64         | after.diet   | N2        | 6.273   |
|  **56**  | SM77         | after.diet   | N2        | 15.600  |
|  **57**  | SM79         | after.diet   | N2        | 9.451   |
|  **58**  | SM8          | before.diet  | N2        | 9.014   |
|  **59**  | SM83         | before.diet  | N2        | 7.995   |
|  **60**  | SM84         | before.diet  | N2        | 25.074  |
|  **61**  | SM86         | after.diet   | N2        | 9.868   |
|  **62**  | SM87         | before.diet  | N2        | 16.257  |
|  **63**  | SM88         | after.diet   | N2        | 11.263  |
|  **64**  | SM9          | after.diet   | N2        | 14.085  |
|  **65**  | SM90         | after.diet   | N2        | 26.813  |
|  **66**  | SM96         | before.diet  | N2        | 4.436   |




\(1.4.1.4.2.1.3.2.2.2.1.1.1\) <a name="table.316"></a>[`Table 316.`](#table.316) Summary table. Data grouped by DietStatus. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.1.3.2.2.2.1.1.1-9356f7d2859.1.4.1.4.2.1.3.2.2.2.tsv`](data/1.4.1.4.2.1.3.2.2.2.1.1.1-9356f7d2859.1.4.1.4.2.1.3.2.2.2.tsv)




| feature   | DietStatus   | mean   | sd     | median   | incidence   |
|:----------|:-------------|:-------|:-------|:---------|:------------|
| N1        | after.diet   | 24.582 | 16.329 | 24.998   | 1           |
| N1        | before.diet  | 23.115 | 12.801 | 20.415   | 1           |
| N2        | after.diet   | 10.314 | 7.170  | 9.680    | 1           |
| N2        | before.diet  | 9.947  | 5.927  | 8.854    | 1           |







\(1.4.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1430"></a>[`Figure 1430.`](#figure.1430) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  bar (sample mean) plot.  Image file: [`plots/9351e4243e.png`](plots/9351e4243e.png). High resolution image file: [`plots/9351e4243e-hires.png`](plots/9351e4243e-hires.png).
[![](plots/9351e4243e.png)](plots/9351e4243e-hires.png)




\(1.4.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1431"></a>[`Figure 1431.`](#figure.1431) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  violin plot.  Image file: [`plots/9352907636a.png`](plots/9352907636a.png). High resolution image file: [`plots/9352907636a-hires.png`](plots/9352907636a-hires.png).
[![](plots/9352907636a.png)](plots/9352907636a-hires.png)




\(1.4.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1432"></a>[`Figure 1432.`](#figure.1432) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  boxplot plot.  Image file: [`plots/935240fd931.png`](plots/935240fd931.png). High resolution image file: [`plots/935240fd931-hires.png`](plots/935240fd931-hires.png).
[![](plots/935240fd931.png)](plots/935240fd931-hires.png)


##### \(1.4.1.4.2.1.3.2.2.2.1.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1433"></a>[`Figure 1433.`](#figure.1433) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  bar_stacked plot.  Image file: [`plots/935c7c3276.png`](plots/935c7c3276.png). High resolution image file: [`plots/935c7c3276-hires.png`](plots/935c7c3276-hires.png).
[![](plots/935c7c3276.png)](plots/935c7c3276-hires.png)




\(1.4.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1434"></a>[`Figure 1434.`](#figure.1434) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  bar (sample mean) plot.  Image file: [`plots/9352dd4e761.png`](plots/9352dd4e761.png). High resolution image file: [`plots/9352dd4e761-hires.png`](plots/9352dd4e761-hires.png).
[![](plots/9352dd4e761.png)](plots/9352dd4e761-hires.png)




\(1.4.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1435"></a>[`Figure 1435.`](#figure.1435) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  violin plot.  Image file: [`plots/93574a2a8c8.png`](plots/93574a2a8c8.png). High resolution image file: [`plots/93574a2a8c8-hires.png`](plots/93574a2a8c8-hires.png).
[![](plots/93574a2a8c8.png)](plots/93574a2a8c8-hires.png)




\(1.4.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1436"></a>[`Figure 1436.`](#figure.1436) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  boxplot plot.  Image file: [`plots/93562f71e4a.png`](plots/93562f71e4a.png). High resolution image file: [`plots/93562f71e4a-hires.png`](plots/93562f71e4a-hires.png).
[![](plots/93562f71e4a.png)](plots/93562f71e4a-hires.png)


##### \(1.4.1.4.2.1.3.2.2.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.4.1.4.2.1.3.2.2.2.2.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1437"></a>[`Figure 1437.`](#figure.1437) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  bar (sample mean) plot.  Image file: [`plots/9354de5bf08.png`](plots/9354de5bf08.png). High resolution image file: [`plots/9354de5bf08-hires.png`](plots/9354de5bf08-hires.png).
[![](plots/9354de5bf08.png)](plots/9354de5bf08-hires.png)




\(1.4.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1438"></a>[`Figure 1438.`](#figure.1438) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  violin plot.  Image file: [`plots/9352660ce2c.png`](plots/9352660ce2c.png). High resolution image file: [`plots/9352660ce2c-hires.png`](plots/9352660ce2c-hires.png).
[![](plots/9352660ce2c.png)](plots/9352660ce2c-hires.png)




\(1.4.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1439"></a>[`Figure 1439.`](#figure.1439) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  boxplot plot.  Image file: [`plots/9351d7fbe63.png`](plots/9351d7fbe63.png). High resolution image file: [`plots/9351d7fbe63-hires.png`](plots/9351d7fbe63-hires.png).
[![](plots/9351d7fbe63.png)](plots/9351d7fbe63-hires.png)


##### \(1.4.1.4.2.1.3.2.2.2.2.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1440"></a>[`Figure 1440.`](#figure.1440) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  bar (sample mean) plot.  Image file: [`plots/9352dac64b6.png`](plots/9352dac64b6.png). High resolution image file: [`plots/9352dac64b6-hires.png`](plots/9352dac64b6-hires.png).
[![](plots/9352dac64b6.png)](plots/9352dac64b6-hires.png)




\(1.4.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1441"></a>[`Figure 1441.`](#figure.1441) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  violin plot.  Image file: [`plots/93511080027.png`](plots/93511080027.png). High resolution image file: [`plots/93511080027-hires.png`](plots/93511080027-hires.png).
[![](plots/93511080027.png)](plots/93511080027-hires.png)




\(1.4.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1442"></a>[`Figure 1442.`](#figure.1442) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus.  boxplot plot.  Image file: [`plots/9352442092d.png`](plots/9352442092d.png). High resolution image file: [`plots/9352442092d-hires.png`](plots/9352442092d-hires.png).
[![](plots/9352442092d.png)](plots/9352442092d-hires.png)


##### \(1.4.1.4.2.1.3.2.3\) Grouping variables DietStatus,Drug.Before.Diet




##### \(1.4.1.4.2.1.3.2.4\) Iterating over Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order




##### \(1.4.1.4.2.1.3.2.4.1\) Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order: original




##### \(1.4.1.4.2.1.3.2.4.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.4.1.4.2.1.3.2.4.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.4.1.4.2.1.3.2.4.2.1.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.4.1.4.2.1.3.2.4.2.1.1.0\) <a name="table.317"></a>[`Table 317.`](#table.317) Data table used for plots. Data grouped by DietStatus,Drug.Before.Diet. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.1.3.2.4.2.1.1.0-9356b687bf3.1.4.1.4.2.1.3.2.4.2.tsv`](data/1.4.1.4.2.1.3.2.4.2.1.1.0-9356b687bf3.1.4.1.4.2.1.3.2.4.2.tsv)




|  &nbsp;  | .record.id   | DietStatus   | Drug.Before.Diet   | feature   | index   |
|:--------:|:-------------|:-------------|:-------------------|:----------|:--------|
|  **1**   | SM1          | before.diet  | TRUE               | N1        | 29.139  |
|  **2**   | SM11         | after.diet   | TRUE               | N1        | 27.795  |
|  **3**   | SM12         | before.diet  | TRUE               | N1        | 27.202  |
|  **4**   | SM13         | before.diet  | FALSE              | N1        | 24.425  |
|  **5**   | SM14         | before.diet  | FALSE              | N1        | 19.408  |
|  **6**   | SM17         | before.diet  | TRUE               | N1        | 20.415  |
|  **7**   | SM19         | after.diet   | FALSE              | N1        | 33.140  |
|  **8**   | SM2          | before.diet  | FALSE              | N1        | 10.793  |
|  **9**   | SM23         | after.diet   | TRUE               | N1        | 2.532   |
|  **10**  | SM3          | after.diet   | TRUE               | N1        | 51.043  |
|  **11**  | SM33         | before.diet  | TRUE               | N1        | 6.630   |
|  **12**  | SM38         | after.diet   | TRUE               | N1        | 15.358  |
|  **13**  | SM41         | before.diet  | TRUE               | N1        | 43.505  |
|  **14**  | SM45         | after.diet   | FALSE              | N1        | 1.021   |
|  **15**  | SM49         | before.diet  | TRUE               | N1        | 17.267  |
|  **16**  | SM52         | before.diet  | FALSE              | N1        | 36.830  |
|  **17**  | SM53         | after.diet   | TRUE               | N1        | 22.762  |
|  **18**  | SM56         | after.diet   | TRUE               | N1        | 6.034   |
|  **19**  | SM57         | before.diet  | TRUE               | N1        | 11.371  |
|  **20**  | SM58         | after.diet   | FALSE              | N1        | 10.158  |
|  **21**  | SM62         | before.diet  | TRUE               | N1        | 7.018   |
|  **22**  | SM64         | after.diet   | TRUE               | N1        | 16.080  |
|  **23**  | SM77         | after.diet   | TRUE               | N1        | 41.015  |
|  **24**  | SM79         | after.diet   | FALSE              | N1        | 25.531  |
|  **25**  | SM8          | before.diet  | FALSE              | N1        | 21.409  |
|  **26**  | SM83         | before.diet  | TRUE               | N1        | 15.798  |
|  **27**  | SM84         | before.diet  | TRUE               | N1        | 52.336  |
|  **28**  | SM86         | after.diet   | TRUE               | N1        | 29.420  |
|  **29**  | SM87         | before.diet  | TRUE               | N1        | 33.974  |
|  **30**  | SM88         | after.diet   | TRUE               | N1        | 24.465  |
|  **31**  | SM9          | after.diet   | FALSE              | N1        | 28.438  |
|  **32**  | SM90         | after.diet   | FALSE              | N1        | 58.528  |
|  **33**  | SM96         | before.diet  | FALSE              | N1        | 15.435  |
|  **34**  | SM1          | before.diet  | TRUE               | N2        | 13.028  |
|  **35**  | SM11         | after.diet   | TRUE               | N2        | 11.703  |
|  **36**  | SM12         | before.diet  | TRUE               | N2        | 10.829  |
|  **37**  | SM13         | before.diet  | FALSE              | N2        | 8.854   |
|  **38**  | SM14         | before.diet  | FALSE              | N2        | 5.659   |
|  **39**  | SM17         | before.diet  | TRUE               | N2        | 8.527   |
|  **40**  | SM19         | after.diet   | FALSE              | N2        | 9.964   |
|  **41**  | SM2          | before.diet  | FALSE              | N2        | 4.634   |
|  **42**  | SM23         | after.diet   | TRUE               | N2        | 1.808   |
|  **43**  | SM3          | after.diet   | TRUE               | N2        | 23.265  |
|  **44**  | SM33         | before.diet  | TRUE               | N2        | 3.637   |
|  **45**  | SM38         | after.diet   | TRUE               | N2        | 8.136   |
|  **46**  | SM41         | before.diet  | TRUE               | N2        | 16.092  |
|  **47**  | SM45         | after.diet   | FALSE              | N2        | 1.006   |
|  **48**  | SM49         | before.diet  | TRUE               | N2        | 9.501   |
|  **49**  | SM52         | before.diet  | FALSE              | N2        | 17.185  |
|  **50**  | SM53         | after.diet   | TRUE               | N2        | 9.492   |
|  **51**  | SM56         | after.diet   | TRUE               | N2        | 2.202   |
|  **52**  | SM57         | before.diet  | TRUE               | N2        | 5.109   |
|  **53**  | SM58         | after.diet   | FALSE              | N2        | 4.101   |
|  **54**  | SM62         | before.diet  | TRUE               | N2        | 3.262   |
|  **55**  | SM64         | after.diet   | TRUE               | N2        | 6.273   |
|  **56**  | SM77         | after.diet   | TRUE               | N2        | 15.600  |
|  **57**  | SM79         | after.diet   | FALSE              | N2        | 9.451   |
|  **58**  | SM8          | before.diet  | FALSE              | N2        | 9.014   |
|  **59**  | SM83         | before.diet  | TRUE               | N2        | 7.995   |
|  **60**  | SM84         | before.diet  | TRUE               | N2        | 25.074  |
|  **61**  | SM86         | after.diet   | TRUE               | N2        | 9.868   |
|  **62**  | SM87         | before.diet  | TRUE               | N2        | 16.257  |
|  **63**  | SM88         | after.diet   | TRUE               | N2        | 11.263  |
|  **64**  | SM9          | after.diet   | FALSE              | N2        | 14.085  |
|  **65**  | SM90         | after.diet   | FALSE              | N2        | 26.813  |
|  **66**  | SM96         | before.diet  | FALSE              | N2        | 4.436   |




\(1.4.1.4.2.1.3.2.4.2.1.1.1\) <a name="table.318"></a>[`Table 318.`](#table.318) Summary table. Data grouped by DietStatus,Drug.Before.Diet. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.1.3.2.4.2.1.1.1-93518a1c59c.1.4.1.4.2.1.3.2.4.2.tsv`](data/1.4.1.4.2.1.3.2.4.2.1.1.1-93518a1c59c.1.4.1.4.2.1.3.2.4.2.tsv)




| feature   | DietStatus   | Drug.Before.Diet   | mean   | sd     | median   | incidence   |
|:----------|:-------------|:-------------------|:-------|:-------|:---------|:------------|
| N1        | after.diet   | FALSE              | 26.136 | 19.950 | 26.984   | 1           |
| N1        | after.diet   | TRUE               | 23.650 | 14.856 | 23.613   | 1           |
| N1        | before.diet  | FALSE              | 21.383 | 8.936  | 20.408   | 1           |
| N1        | before.diet  | TRUE               | 24.060 | 14.814 | 20.415   | 1           |
| N2        | after.diet   | FALSE              | 10.903 | 9.060  | 9.708    | 1           |
| N2        | after.diet   | TRUE               | 9.961  | 6.301  | 9.680    | 1           |
| N2        | before.diet  | FALSE              | 8.297  | 4.798  | 7.257    | 1           |
| N2        | before.diet  | TRUE               | 10.846 | 6.494  | 9.501    | 1           |







\(1.4.1.4.2.1.3.2.4.2.1.1.1\) <a name="figure.1443"></a>[`Figure 1443.`](#figure.1443) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/93524b49166.png`](plots/93524b49166.png). High resolution image file: [`plots/93524b49166-hires.png`](plots/93524b49166-hires.png).
[![](plots/93524b49166.png)](plots/93524b49166-hires.png)




\(1.4.1.4.2.1.3.2.4.2.1.1.1\) <a name="figure.1444"></a>[`Figure 1444.`](#figure.1444) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  violin plot.  Image file: [`plots/9354ab5d25d.png`](plots/9354ab5d25d.png). High resolution image file: [`plots/9354ab5d25d-hires.png`](plots/9354ab5d25d-hires.png).
[![](plots/9354ab5d25d.png)](plots/9354ab5d25d-hires.png)




\(1.4.1.4.2.1.3.2.4.2.1.1.1\) <a name="figure.1445"></a>[`Figure 1445.`](#figure.1445) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/9356705fdfc.png`](plots/9356705fdfc.png). High resolution image file: [`plots/9356705fdfc-hires.png`](plots/9356705fdfc-hires.png).
[![](plots/9356705fdfc.png)](plots/9356705fdfc-hires.png)


##### \(1.4.1.4.2.1.3.2.4.2.1.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.4.2.1.2.1\) <a name="figure.1446"></a>[`Figure 1446.`](#figure.1446) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/9353a65e03b.png`](plots/9353a65e03b.png). High resolution image file: [`plots/9353a65e03b-hires.png`](plots/9353a65e03b-hires.png).
[![](plots/9353a65e03b.png)](plots/9353a65e03b-hires.png)




\(1.4.1.4.2.1.3.2.4.2.1.2.1\) <a name="figure.1447"></a>[`Figure 1447.`](#figure.1447) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  violin plot.  Image file: [`plots/93576645f70.png`](plots/93576645f70.png). High resolution image file: [`plots/93576645f70-hires.png`](plots/93576645f70-hires.png).
[![](plots/93576645f70.png)](plots/93576645f70-hires.png)




\(1.4.1.4.2.1.3.2.4.2.1.2.1\) <a name="figure.1448"></a>[`Figure 1448.`](#figure.1448) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/93537b5eec9.png`](plots/93537b5eec9.png). High resolution image file: [`plots/93537b5eec9-hires.png`](plots/93537b5eec9-hires.png).
[![](plots/93537b5eec9.png)](plots/93537b5eec9-hires.png)


##### \(1.4.1.4.2.1.3.2.4.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.4.1.4.2.1.3.2.4.2.2.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.4.2.2.1.1\) <a name="figure.1449"></a>[`Figure 1449.`](#figure.1449) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/93594feab2.png`](plots/93594feab2.png). High resolution image file: [`plots/93594feab2-hires.png`](plots/93594feab2-hires.png).
[![](plots/93594feab2.png)](plots/93594feab2-hires.png)




\(1.4.1.4.2.1.3.2.4.2.2.1.1\) <a name="figure.1450"></a>[`Figure 1450.`](#figure.1450) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  violin plot.  Image file: [`plots/9355db950e4.png`](plots/9355db950e4.png). High resolution image file: [`plots/9355db950e4-hires.png`](plots/9355db950e4-hires.png).
[![](plots/9355db950e4.png)](plots/9355db950e4-hires.png)




\(1.4.1.4.2.1.3.2.4.2.2.1.1\) <a name="figure.1451"></a>[`Figure 1451.`](#figure.1451) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/935316dd8ce.png`](plots/935316dd8ce.png). High resolution image file: [`plots/935316dd8ce-hires.png`](plots/935316dd8ce-hires.png).
[![](plots/935316dd8ce.png)](plots/935316dd8ce-hires.png)


##### \(1.4.1.4.2.1.3.2.4.2.2.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.4.1.4.2.1.3.2.4.2.2.2.1\) <a name="figure.1452"></a>[`Figure 1452.`](#figure.1452) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/93522b4d5bc.png`](plots/93522b4d5bc.png). High resolution image file: [`plots/93522b4d5bc-hires.png`](plots/93522b4d5bc-hires.png).
[![](plots/93522b4d5bc.png)](plots/93522b4d5bc-hires.png)




\(1.4.1.4.2.1.3.2.4.2.2.2.1\) <a name="figure.1453"></a>[`Figure 1453.`](#figure.1453) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  violin plot.  Image file: [`plots/935e3c3b71.png`](plots/935e3c3b71.png). High resolution image file: [`plots/935e3c3b71-hires.png`](plots/935e3c3b71-hires.png).
[![](plots/935e3c3b71.png)](plots/935e3c3b71-hires.png)




\(1.4.1.4.2.1.3.2.4.2.2.2.1\) <a name="figure.1454"></a>[`Figure 1454.`](#figure.1454) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by DietStatus,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/93514627f04.png`](plots/93514627f04.png). High resolution image file: [`plots/93514627f04-hires.png`](plots/93514627f04-hires.png).
[![](plots/93514627f04.png)](plots/93514627f04-hires.png)
