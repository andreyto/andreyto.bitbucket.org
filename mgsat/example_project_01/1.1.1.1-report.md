% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



#### \(1.1.1.1\) Taxonomic level: 2 of Subset: All samples, no aggregation, no tests, only plots



Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


Loaded OTU taxonomy file [`example_cons.taxonomy.file`](example_cons.taxonomy.file).


Loaded 100 records for 11 features from count file [`example_otu.shared.file`](example_otu.shared.file) for taxonomic level 2 
                       with taxa name sanitize setting TRUE with count basis seq with taxa count source shared


Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 11 features


After merging with metadata, 100 records left


After aggregating/subsetting, sample count is: 100


Filtering abundance matrix with arguments [ min_row_sum:2000, max_row_sum:4e+05]. sample


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 87 records for 11 features



##### \(1.1.1.1.2\) Summary of metadata variables




##### \(1.1.1.1.3\) Summary of metadata variables after filtering samples






\(1.1.1.1.3\)  Summary of metadata variables.


| MatchedGroupID   | SubjectID   | SampleID   | Sample.type   | Sample.type.1          | DietStatus     | Drug          | Complaints    | age            | age.quant      | visit         | Drug.Before.Diet   | Sample.type.Drug.Before   | visit.max     | visit.min     | visit.1       | visit.2       | has.matched.subject   |
|:-----------------|:------------|:-----------|:--------------|:-----------------------|:---------------|:--------------|:--------------|:---------------|:---------------|:--------------|:-------------------|:--------------------------|:--------------|:--------------|:--------------|:--------------|:----------------------|
| MG1    : 5       | SB18   : 4  | SM1    : 1 | control:20    | control            :20 | after.diet :44 | Mode :logical | Mode :logical | Min.   : 3.182 | [3.18,6.27]:22 | Min.   :1.000 | Mode :logical      | 1              :20        | Min.   :1.000 | Min.   :1.000 | Mode :logical | Mode :logical | Mode :logical         |
| MG19   : 5       | SB29   : 4  | SM10   : 1 | patient:67    | patient.after.diet :44 | before.diet:43 | FALSE:48      | FALSE:80      | 1st Qu.: 6.238 | (6.27,8.71]:21 | 1st Qu.:1.000 | FALSE:48           | patient FALSE .:29        | 1st Qu.:2.000 | 1st Qu.:1.000 | FALSE:1       | FALSE:21      | FALSE:17              |
| MG22   : 5       | SB39   : 4  | SM100  : 1 | NA            | patient.before.diet:23 | NA             | TRUE :39      | TRUE :7       | Median : 8.779 | (8.71,15.3]:21 | Median :2.000 | TRUE :39           | patient TRUE . :38        | Median :4.000 | Median :1.000 | TRUE :86      | TRUE :66      | TRUE :70              |
| MG3    : 5       | SB4    : 4  | SM11   : 1 | NA            | NA                     | NA             | NA's :0       | NA's :0       | Mean   :11.290 | (15.3,26.4]:23 | Mean   :1.989 | NA's :0            | NA                        | Mean   :3.356 | Mean   :1.011 | NA's :0       | NA's :0       | NA's :0               |
| MG8    : 5       | SB41   : 4  | SM12   : 1 | NA            | NA                     | NA             | NA            | NA            | 3rd Qu.:16.343 | NA             | 3rd Qu.:3.000 | NA                 | NA                        | 3rd Qu.:4.000 | 3rd Qu.:1.000 | NA            | NA            | NA                    |
| MG10   : 4       | SB44   : 4  | SM13   : 1 | NA            | NA                     | NA             | NA            | NA            | Max.   :26.366 | NA             | Max.   :4.000 | NA                 | NA                        | Max.   :7.000 | Max.   :2.000 | NA            | NA            | NA                    |
| (Other):58       | (Other):63  | (Other):81 | NA            | NA                     | NA             | NA            | NA            | NA             | NA             | NA            | NA                 | NA                        | NA            | NA            | NA            | NA            | NA                    |





\(1.1.1.1.3\)  Sample cross tabulation ~Sample.type+DietStatus.


|    &nbsp;     | after.diet   | before.diet   |
|:-------------:|:-------------|:--------------|
|  **control**  | 0            | 20            |
|  **patient**  | 44           | 23            |



```````
Call: xtabs(formula = as.formula(xtabs.formula), data = meta, drop.unused.levels = T)
Number of cases in table: 87 
Number of factors: 2 
Test for independence of all factors:
	Chisq = 26.574, df = 1, p-value = 2.536e-07
```````






\(1.1.1.1.3\)  Sample cross tabulation ~Drug.Before.Diet + Sample.type.


|   &nbsp;    | control   | patient   |
|:-----------:|:----------|:----------|
|  **FALSE**  | 19        | 29        |
|  **TRUE**   | 1         | 38        |



```````
Call: xtabs(formula = as.formula(xtabs.formula), data = meta, drop.unused.levels = T)
Number of cases in table: 87 
Number of factors: 2 
Test for independence of all factors:
	Chisq = 16.656, df = 1, p-value = 4.48e-05
```````






\(1.1.1.1.3\)  Sample cross tabulation ~Complaints + Sample.type.


|   &nbsp;    | control   | patient   |
|:-----------:|:----------|:----------|
|  **FALSE**  | 20        | 60        |
|  **TRUE**   | 0         | 7         |



```````
Call: xtabs(formula = as.formula(xtabs.formula), data = meta, drop.unused.levels = T)
Number of cases in table: 87 
Number of factors: 2 
Test for independence of all factors:
	Chisq = 2.2724, df = 1, p-value = 0.1317
	Chi-squared approximation may be incorrect
```````






\(1.1.1.1.3\)  Sample cross tabulation ~Sample.type+visit.


|    &nbsp;     | 1   | 2   | 3   | 4   |
|:-------------:|:----|:----|:----|:----|
|  **control**  | 18  | 2   | 0   | 0   |
|  **patient**  | 23  | 17  | 14  | 13  |



```````
Call: xtabs(formula = as.formula(xtabs.formula), data = meta, drop.unused.levels = T)
Number of cases in table: 87 
Number of factors: 2 
Test for independence of all factors:
	Chisq = 19.856, df = 3, p-value = 0.0001818
	Chi-squared approximation may be incorrect
```````






\(1.1.1.1.3\)  Sample cross tabulation ~MatchedGroupID.

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
| 5 | 4 | 3 | 1 | 2 | 3 | 4 | 2 | 4 | 3 | 5 | 4 | 4 | 3 | 5 | 4 | 2 | 4 | 1 | 5 | 4 | 1 | 4 | 3 | 5 | 2 |



```````
Number of cases in table: 87 
Number of factors: 1 
```````






\(1.1.1.1.3\)  Sample cross tabulation ~Sample.type.1.

|    |    |    |
|:---|:---|:---|
| 20 | 44 | 23 |



```````
Number of cases in table: 87 
Number of factors: 1 
```````






\(1.1.1.1.3\)  Sample cross tabulation ~SubjectID.

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
| 1 | 1 | 2 | 1 | 1 | 1 | 1 | 2 | 1 | 4 | 1 | 1 | 3 | 2 | 2 | 1 | 1 | 2 | 1 | 1 | 3 | 4 | 1 | 1 | 1 | 1 | 1 | 2 | 3 | 3 | 3 | 1 | 4 | 4 | 1 | 4 | 2 | 2 | 4 | 1 | 3 | 3 | 1 | 1 | 3 |



```````
Number of cases in table: 87 
Number of factors: 1 
```````



\(1.1.1.1.3\)  ANOVA for age and sample type.



```````
            Df Sum Sq Mean Sq F value Pr(>F)
Sample.type  1      6    6.34   0.153  0.696
Residuals   85   3519   41.39               
```````






\(1.1.1.1.3\) <a name="figure.1"></a>[`Figure 1.`](#figure.1) Violin plot for age and sample type.  Image file: [`plots/9355dc7585d.png`](plots/9355dc7585d.png). High resolution image file: [`plots/9355dc7585d-hires.png`](plots/9355dc7585d-hires.png).
[![](plots/9355dc7585d.png)](plots/9355dc7585d-hires.png)




\(1.1.1.1.3\)  Spearman RHO for age and visit.


| Test statistic   | P value   | Alternative hypothesis   |
|:-----------------|:----------|:-------------------------|
| 131260           | _0.0686_  | two.sided                |

Table: Spearman's rank correlation rho: `age` and `visit`





\(1.1.1.1.3\)  Spearman RHO for age and visit, patients only.


| Test statistic   | P value   | Alternative hypothesis   |
|:-----------------|:----------|:-------------------------|
| 59864            | _0.1148_  | two.sided                |

Table: Spearman's rank correlation rho: `age` and `visit`





\(1.1.1.1.3\) <a name="figure.2"></a>[`Figure 2.`](#figure.2) Plot for age and visit with Loess trend line.  Image file: [`plots/9353eaf76be.png`](plots/9353eaf76be.png). High resolution image file: [`plots/9353eaf76be-hires.png`](plots/9353eaf76be-hires.png).
[![](plots/9353eaf76be.png)](plots/9353eaf76be-hires.png)


##### \(1.1.1.1.4\) Summary of total counts per sample

[`Subreport`](./1.1.1.1.4-report.html)

**Wrote counts and metadata for raw counts All samples, no aggregation, no tests, only plots to files [`data/1.1.1.1.4-93536f12276samples.raw.16s.l.2.count.tsv`](data/1.1.1.1.4-93536f12276samples.raw.16s.l.2.count.tsv),[`data/1.1.1.1.4-93536f12276samples.raw.16s.l.2.attr.tsv`](data/1.1.1.1.4-93536f12276samples.raw.16s.l.2.attr.tsv)**


**Wrote counts and metadata for proportions counts All samples, no aggregation, no tests, only plots to files [`data/1.1.1.1.4-935110d8d28samples.proportions.16s.l.2.count.tsv`](data/1.1.1.1.4-935110d8d28samples.proportions.16s.l.2.count.tsv),[`data/1.1.1.1.4-935110d8d28samples.proportions.16s.l.2.attr.tsv`](data/1.1.1.1.4-935110d8d28samples.proportions.16s.l.2.attr.tsv)**



##### \(1.1.1.1.5\) Data analysis



Filtering abundance matrix with arguments [ min_mean                :10, min_quant_incidence_frac:0.25, min_quant_mean_frac     :0.25]. feature


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 87 records for 7 features


**Count normalization method for data analysis (unless modified by specific methods) : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.clr"]**


**Count normalization method for abundance plots : [ drop.features:"other", method       :"norm.prop"]**



##### \(1.1.1.1.5.2\) Plots of Abundance.

[`Subreport`](./1.1.1.1.5.2-report.html)




\(1.1.1.1.5.2\) <a name="figure.41"></a>[`Figure 41.`](#figure.41) Heatmap of abundance profile.  Image file: [`plots/93539225d4d.png`](plots/93539225d4d.png). High resolution image file: [`plots/93539225d4d-hires.png`](plots/93539225d4d-hires.png).
[![](plots/93539225d4d.png)](plots/93539225d4d-hires.png)
