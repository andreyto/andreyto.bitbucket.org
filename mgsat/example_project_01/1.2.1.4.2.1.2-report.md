% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



##### \(1.2.1.4.2.1.2\) Plots of Abundance-based richness estimates With rarefication.



**Plots are shown with relation to various combinations of meta 
                   data variables and in different graphical representations. Lots of plots here.**



##### \(1.2.1.4.2.1.2.2\) Iterating over all combinations of grouping variables




##### \(1.2.1.4.2.1.2.2.1\) Grouping variables Sample.type




##### \(1.2.1.4.2.1.2.2.2\) Iterating over Abundance-based richness estimates With rarefication. profile sorting order




##### \(1.2.1.4.2.1.2.2.2.1\) Abundance-based richness estimates With rarefication. profile sorting order: original




##### \(1.2.1.4.2.1.2.2.2.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.2.1.4.2.1.2.2.2.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.2.1.4.2.1.2.2.2.2.1.1\) Abundance-based richness estimates With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.2.1.4.2.1.2.2.2.2.1.1.0\) <a name="table.136"></a>[`Table 136.`](#table.136) Data table used for plots. Data grouped by Sample.type. Full dataset is also saved in a delimited text file [`data/1.2.1.4.2.1.2.2.2.2.1.1.0-93579253481.1.2.1.4.2.1.2.2.2.2.tsv`](data/1.2.1.4.2.1.2.2.2.2.1.1.0-93579253481.1.2.1.4.2.1.2.2.2.2.tsv)




|  &nbsp;  | .record.id   | Sample.type   | feature   | Richness.Estimate   |
|:--------:|:-------------|:--------------|:----------|:--------------------|
|  **1**   | SB1          | control       | S.obs     | 201.15              |
|  **2**   | SB10         | control       | S.obs     | 282.53              |
|  **3**   | SB11         | patient       | S.obs     | 149.00              |
|  **4**   | SB12         | control       | S.obs     | 39.95               |
|  **5**   | SB13         | control       | S.obs     | 194.89              |
|  **6**   | SB14         | control       | S.obs     | 202.38              |
|  **7**   | SB15         | control       | S.obs     | 191.52              |
|  **8**   | SB16         | patient       | S.obs     | 137.71              |
|  **9**   | SB17         | patient       | S.obs     | 173.14              |
|  **10**  | SB18         | patient       | S.obs     | 103.26              |
|  **11**  | SB19         | control       | S.obs     | 208.38              |
|  **12**  | SB21         | control       | S.obs     | 266.50              |
|  **13**  | SB22         | patient       | S.obs     | 211.97              |
|  **14**  | SB23         | patient       | S.obs     | 163.88              |
|  **15**  | SB24         | control       | S.obs     | 252.81              |
|  **16**  | SB25         | control       | S.obs     | 237.56              |
|  **17**  | SB26         | control       | S.obs     | 179.04              |
|  **18**  | SB28         | patient       | S.obs     | 51.05               |
|  **19**  | SB29         | patient       | S.obs     | 110.79              |
|  **20**  | SB3          | control       | S.obs     | 201.06              |
|  **21**  | SB30         | patient       | S.obs     | 100.57              |
|  **22**  | SB31         | control       | S.obs     | 235.07              |
|  **23**  | SB32         | control       | S.obs     | 242.04              |
|  **24**  | SB33         | control       | S.obs     | 186.53              |
|  **25**  | SB34         | patient       | S.obs     | 154.66              |
|  **26**  | SB35         | patient       | S.obs     | 45.71               |
|  **27**  | SB36         | patient       | S.obs     | 135.16              |
|  **28**  | SB38         | patient       | S.obs     | 171.11              |
|  **29**  | SB39         | patient       | S.obs     | 194.19              |
|  **30**  | SB4          | patient       | S.obs     | 108.46              |
|  **31**  | SB40         | control       | S.obs     | 220.71              |
|  **32**  | SB41         | patient       | S.obs     | 148.81              |
|  **33**  | SB42         | patient       | S.obs     | 142.10              |
|  **34**  | SB43         | patient       | S.obs     | 171.16              |
|  **35**  | SB44         | patient       | S.obs     | 114.41              |
|  **36**  | SB45         | control       | S.obs     | 150.61              |
|  **37**  | SB5          | patient       | S.obs     | 188.31              |
|  **38**  | SB6          | patient       | S.obs     | 109.77              |
|  **39**  | SB7          | control       | S.obs     | 219.06              |
|  **40**  | SB8          | patient       | S.obs     | 264.73              |
|  **41**  | SB9          | patient       | S.obs     | 164.62              |




\(1.2.1.4.2.1.2.2.2.2.1.1.1\) <a name="table.137"></a>[`Table 137.`](#table.137) Summary table. Data grouped by Sample.type. Full dataset is also saved in a delimited text file [`data/1.2.1.4.2.1.2.2.2.2.1.1.1-935799a3f49.1.2.1.4.2.1.2.2.2.2.tsv`](data/1.2.1.4.2.1.2.2.2.2.1.1.1-935799a3f49.1.2.1.4.2.1.2.2.2.2.tsv)




| feature   | Sample.type   | mean   | sd    | median   | incidence   |
|:----------|:--------------|:-------|:------|:---------|:------------|
| S.obs     | control       | 206.2  | 52.79 | 205.4    | 1           |
| S.obs     | patient       | 144.1  | 49.25 | 148.8    | 1           |







\(1.2.1.4.2.1.2.2.2.2.1.1.1\) <a name="figure.669"></a>[`Figure 669.`](#figure.669) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  bar (sample mean) plot.  Image file: [`plots/9357db10ffd.png`](plots/9357db10ffd.png). High resolution image file: [`plots/9357db10ffd-hires.png`](plots/9357db10ffd-hires.png).
[![](plots/9357db10ffd.png)](plots/9357db10ffd-hires.png)




\(1.2.1.4.2.1.2.2.2.2.1.1.1\) <a name="figure.670"></a>[`Figure 670.`](#figure.670) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  violin plot.  Image file: [`plots/9357390eb82.png`](plots/9357390eb82.png). High resolution image file: [`plots/9357390eb82-hires.png`](plots/9357390eb82-hires.png).
[![](plots/9357390eb82.png)](plots/9357390eb82-hires.png)




\(1.2.1.4.2.1.2.2.2.2.1.1.1\) <a name="figure.671"></a>[`Figure 671.`](#figure.671) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  boxplot plot.  Image file: [`plots/9352f55df14.png`](plots/9352f55df14.png). High resolution image file: [`plots/9352f55df14-hires.png`](plots/9352f55df14-hires.png).
[![](plots/9352f55df14.png)](plots/9352f55df14-hires.png)


##### \(1.2.1.4.2.1.2.2.2.2.1.2\) Abundance-based richness estimates With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.2.2.1.2.1\) <a name="figure.672"></a>[`Figure 672.`](#figure.672) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  bar (sample mean) plot.  Image file: [`plots/9352ea8b253.png`](plots/9352ea8b253.png). High resolution image file: [`plots/9352ea8b253-hires.png`](plots/9352ea8b253-hires.png).
[![](plots/9352ea8b253.png)](plots/9352ea8b253-hires.png)




\(1.2.1.4.2.1.2.2.2.2.1.2.1\) <a name="figure.673"></a>[`Figure 673.`](#figure.673) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  violin plot.  Image file: [`plots/93545537f13.png`](plots/93545537f13.png). High resolution image file: [`plots/93545537f13-hires.png`](plots/93545537f13-hires.png).
[![](plots/93545537f13.png)](plots/93545537f13-hires.png)




\(1.2.1.4.2.1.2.2.2.2.1.2.1\) <a name="figure.674"></a>[`Figure 674.`](#figure.674) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  boxplot plot.  Image file: [`plots/9356cbbdbf3.png`](plots/9356cbbdbf3.png). High resolution image file: [`plots/9356cbbdbf3-hires.png`](plots/9356cbbdbf3-hires.png).
[![](plots/9356cbbdbf3.png)](plots/9356cbbdbf3-hires.png)


##### \(1.2.1.4.2.1.2.2.2.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.2.1.4.2.1.2.2.2.2.2.1\) Abundance-based richness estimates With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.2.2.2.1.1\) <a name="figure.675"></a>[`Figure 675.`](#figure.675) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  bar (sample mean) plot.  Image file: [`plots/9352165664a.png`](plots/9352165664a.png). High resolution image file: [`plots/9352165664a-hires.png`](plots/9352165664a-hires.png).
[![](plots/9352165664a.png)](plots/9352165664a-hires.png)




\(1.2.1.4.2.1.2.2.2.2.2.1.1\) <a name="figure.676"></a>[`Figure 676.`](#figure.676) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  violin plot.  Image file: [`plots/93581e9567.png`](plots/93581e9567.png). High resolution image file: [`plots/93581e9567-hires.png`](plots/93581e9567-hires.png).
[![](plots/93581e9567.png)](plots/93581e9567-hires.png)




\(1.2.1.4.2.1.2.2.2.2.2.1.1\) <a name="figure.677"></a>[`Figure 677.`](#figure.677) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  boxplot plot.  Image file: [`plots/935fe2a15b.png`](plots/935fe2a15b.png). High resolution image file: [`plots/935fe2a15b-hires.png`](plots/935fe2a15b-hires.png).
[![](plots/935fe2a15b.png)](plots/935fe2a15b-hires.png)


##### \(1.2.1.4.2.1.2.2.2.2.2.2\) Abundance-based richness estimates With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.2.2.2.2.1\) <a name="figure.678"></a>[`Figure 678.`](#figure.678) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  bar (sample mean) plot.  Image file: [`plots/93567cf6582.png`](plots/93567cf6582.png). High resolution image file: [`plots/93567cf6582-hires.png`](plots/93567cf6582-hires.png).
[![](plots/93567cf6582.png)](plots/93567cf6582-hires.png)




\(1.2.1.4.2.1.2.2.2.2.2.2.1\) <a name="figure.679"></a>[`Figure 679.`](#figure.679) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  violin plot.  Image file: [`plots/93561116f0c.png`](plots/93561116f0c.png). High resolution image file: [`plots/93561116f0c-hires.png`](plots/93561116f0c-hires.png).
[![](plots/93561116f0c.png)](plots/93561116f0c-hires.png)




\(1.2.1.4.2.1.2.2.2.2.2.2.1\) <a name="figure.680"></a>[`Figure 680.`](#figure.680) Abundance-based richness estimates With rarefication. Data grouped by Sample.type.  boxplot plot.  Image file: [`plots/9353f91ae9d.png`](plots/9353f91ae9d.png). High resolution image file: [`plots/9353f91ae9d-hires.png`](plots/9353f91ae9d-hires.png).
[![](plots/9353f91ae9d.png)](plots/9353f91ae9d-hires.png)


##### \(1.2.1.4.2.1.2.2.3\) Grouping variables Sample.type,age.quant




##### \(1.2.1.4.2.1.2.2.4\) Iterating over Abundance-based richness estimates With rarefication. profile sorting order




##### \(1.2.1.4.2.1.2.2.4.1\) Abundance-based richness estimates With rarefication. profile sorting order: original




##### \(1.2.1.4.2.1.2.2.4.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.2.1.4.2.1.2.2.4.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.2.1.4.2.1.2.2.4.2.1.1\) Abundance-based richness estimates With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.2.1.4.2.1.2.2.4.2.1.1.0\) <a name="table.138"></a>[`Table 138.`](#table.138) Data table used for plots. Data grouped by Sample.type,age.quant. Full dataset is also saved in a delimited text file [`data/1.2.1.4.2.1.2.2.4.2.1.1.0-935755ee605.1.2.1.4.2.1.2.2.4.2.tsv`](data/1.2.1.4.2.1.2.2.4.2.1.1.0-935755ee605.1.2.1.4.2.1.2.2.4.2.tsv)




|  &nbsp;  | .record.id   | Sample.type   | age.quant    | feature   | Richness.Estimate   |
|:--------:|:-------------|:--------------|:-------------|:----------|:--------------------|
|  **1**   | SB1          | control       | \(8.71,15.3] | S.obs     | 201.15              |
|  **2**   | SB10         | control       | \(15.3,26.4] | S.obs     | 282.53              |
|  **3**   | SB11         | patient       | \(8.71,15.3] | S.obs     | 149.00              |
|  **4**   | SB12         | control       | [3.18,6.27]  | S.obs     | 39.95               |
|  **5**   | SB13         | control       | \(6.27,8.71] | S.obs     | 194.89              |
|  **6**   | SB14         | control       | \(8.71,15.3] | S.obs     | 202.38              |
|  **7**   | SB15         | control       | \(8.71,15.3] | S.obs     | 191.52              |
|  **8**   | SB16         | patient       | \(15.3,26.4] | S.obs     | 137.71              |
|  **9**   | SB17         | patient       | \(15.3,26.4] | S.obs     | 173.14              |
|  **10**  | SB18         | patient       | \(15.3,26.4] | S.obs     | 103.26              |
|  **11**  | SB19         | control       | \(15.3,26.4] | S.obs     | 208.38              |
|  **12**  | SB21         | control       | [3.18,6.27]  | S.obs     | 266.50              |
|  **13**  | SB22         | patient       | \(6.27,8.71] | S.obs     | 211.97              |
|  **14**  | SB23         | patient       | \(15.3,26.4] | S.obs     | 163.88              |
|  **15**  | SB24         | control       | \(8.71,15.3] | S.obs     | 252.81              |
|  **16**  | SB25         | control       | \(8.71,15.3] | S.obs     | 237.56              |
|  **17**  | SB26         | control       | \(6.27,8.71] | S.obs     | 179.04              |
|  **18**  | SB28         | patient       | \(6.27,8.71] | S.obs     | 51.05               |
|  **19**  | SB29         | patient       | \(6.27,8.71] | S.obs     | 110.79              |
|  **20**  | SB3          | control       | [3.18,6.27]  | S.obs     | 201.06              |
|  **21**  | SB30         | patient       | \(6.27,8.71] | S.obs     | 100.57              |
|  **22**  | SB31         | control       | \(8.71,15.3] | S.obs     | 235.07              |
|  **23**  | SB32         | control       | \(15.3,26.4] | S.obs     | 242.04              |
|  **24**  | SB33         | control       | \(8.71,15.3] | S.obs     | 186.53              |
|  **25**  | SB34         | patient       | \(15.3,26.4] | S.obs     | 154.66              |
|  **26**  | SB35         | patient       | \(8.71,15.3] | S.obs     | 45.71               |
|  **27**  | SB36         | patient       | [3.18,6.27]  | S.obs     | 135.16              |
|  **28**  | SB38         | patient       | \(15.3,26.4] | S.obs     | 171.11              |
|  **29**  | SB39         | patient       | \(8.71,15.3] | S.obs     | 194.19              |
|  **30**  | SB4          | patient       | [3.18,6.27]  | S.obs     | 108.46              |
|  **31**  | SB40         | control       | [3.18,6.27]  | S.obs     | 220.71              |
|  **32**  | SB41         | patient       | \(6.27,8.71] | S.obs     | 148.81              |
|  **33**  | SB42         | patient       | [3.18,6.27]  | S.obs     | 142.10              |
|  **34**  | SB43         | patient       | \(15.3,26.4] | S.obs     | 171.16              |
|  **35**  | SB44         | patient       | [3.18,6.27]  | S.obs     | 114.41              |
|  **36**  | SB45         | control       | \(15.3,26.4] | S.obs     | 150.61              |
|  **37**  | SB5          | patient       | \(15.3,26.4] | S.obs     | 188.31              |
|  **38**  | SB6          | patient       | \(6.27,8.71] | S.obs     | 109.77              |
|  **39**  | SB7          | control       | \(15.3,26.4] | S.obs     | 219.06              |
|  **40**  | SB8          | patient       | \(15.3,26.4] | S.obs     | 264.73              |
|  **41**  | SB9          | patient       | \(8.71,15.3] | S.obs     | 164.62              |




\(1.2.1.4.2.1.2.2.4.2.1.1.1\) <a name="table.139"></a>[`Table 139.`](#table.139) Summary table. Data grouped by Sample.type,age.quant. Full dataset is also saved in a delimited text file [`data/1.2.1.4.2.1.2.2.4.2.1.1.1-935294f8e76.1.2.1.4.2.1.2.2.4.2.tsv`](data/1.2.1.4.2.1.2.2.4.2.1.1.1-935294f8e76.1.2.1.4.2.1.2.2.4.2.tsv)




| feature   | Sample.type   | age.quant    | mean   | sd    | median   | incidence   |
|:----------|:--------------|:-------------|:-------|:------|:---------|:------------|
| S.obs     | control       | [3.18,6.27]  | 182.1  | 98.63 | 210.9    | 1           |
| S.obs     | control       | \(6.27,8.71] | 187.0  | 11.21 | 187.0    | 1           |
| S.obs     | control       | \(8.71,15.3] | 215.3  | 25.99 | 202.4    | 1           |
| S.obs     | control       | \(15.3,26.4] | 220.5  | 48.34 | 219.1    | 1           |
| S.obs     | patient       | [3.18,6.27]  | 125.0  | 16.14 | 124.8    | 1           |
| S.obs     | patient       | \(6.27,8.71] | 122.2  | 54.00 | 110.3    | 1           |
| S.obs     | patient       | \(8.71,15.3] | 138.4  | 64.56 | 156.8    | 1           |
| S.obs     | patient       | \(15.3,26.4] | 169.8  | 43.43 | 171.1    | 1           |







\(1.2.1.4.2.1.2.2.4.2.1.1.1\) <a name="figure.681"></a>[`Figure 681.`](#figure.681) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  bar (sample mean) plot.  Image file: [`plots/9352611fa2a.png`](plots/9352611fa2a.png). High resolution image file: [`plots/9352611fa2a-hires.png`](plots/9352611fa2a-hires.png).
[![](plots/9352611fa2a.png)](plots/9352611fa2a-hires.png)




\(1.2.1.4.2.1.2.2.4.2.1.1.1\) <a name="figure.682"></a>[`Figure 682.`](#figure.682) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  violin plot.  Image file: [`plots/935663eeeec.png`](plots/935663eeeec.png). High resolution image file: [`plots/935663eeeec-hires.png`](plots/935663eeeec-hires.png).
[![](plots/935663eeeec.png)](plots/935663eeeec-hires.png)




\(1.2.1.4.2.1.2.2.4.2.1.1.1\) <a name="figure.683"></a>[`Figure 683.`](#figure.683) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  boxplot plot.  Image file: [`plots/9352db7fc65.png`](plots/9352db7fc65.png). High resolution image file: [`plots/9352db7fc65-hires.png`](plots/9352db7fc65-hires.png).
[![](plots/9352db7fc65.png)](plots/9352db7fc65-hires.png)


##### \(1.2.1.4.2.1.2.2.4.2.1.2\) Abundance-based richness estimates With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.4.2.1.2.1\) <a name="figure.684"></a>[`Figure 684.`](#figure.684) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  bar (sample mean) plot.  Image file: [`plots/935a1b6256.png`](plots/935a1b6256.png). High resolution image file: [`plots/935a1b6256-hires.png`](plots/935a1b6256-hires.png).
[![](plots/935a1b6256.png)](plots/935a1b6256-hires.png)




\(1.2.1.4.2.1.2.2.4.2.1.2.1\) <a name="figure.685"></a>[`Figure 685.`](#figure.685) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  violin plot.  Image file: [`plots/935bd50149.png`](plots/935bd50149.png). High resolution image file: [`plots/935bd50149-hires.png`](plots/935bd50149-hires.png).
[![](plots/935bd50149.png)](plots/935bd50149-hires.png)




\(1.2.1.4.2.1.2.2.4.2.1.2.1\) <a name="figure.686"></a>[`Figure 686.`](#figure.686) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  boxplot plot.  Image file: [`plots/9354d4765b0.png`](plots/9354d4765b0.png). High resolution image file: [`plots/9354d4765b0-hires.png`](plots/9354d4765b0-hires.png).
[![](plots/9354d4765b0.png)](plots/9354d4765b0-hires.png)


##### \(1.2.1.4.2.1.2.2.4.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.2.1.4.2.1.2.2.4.2.2.1\) Abundance-based richness estimates With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.4.2.2.1.1\) <a name="figure.687"></a>[`Figure 687.`](#figure.687) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  bar (sample mean) plot.  Image file: [`plots/935a652d73.png`](plots/935a652d73.png). High resolution image file: [`plots/935a652d73-hires.png`](plots/935a652d73-hires.png).
[![](plots/935a652d73.png)](plots/935a652d73-hires.png)




\(1.2.1.4.2.1.2.2.4.2.2.1.1\) <a name="figure.688"></a>[`Figure 688.`](#figure.688) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  violin plot.  Image file: [`plots/935788ade59.png`](plots/935788ade59.png). High resolution image file: [`plots/935788ade59-hires.png`](plots/935788ade59-hires.png).
[![](plots/935788ade59.png)](plots/935788ade59-hires.png)




\(1.2.1.4.2.1.2.2.4.2.2.1.1\) <a name="figure.689"></a>[`Figure 689.`](#figure.689) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  boxplot plot.  Image file: [`plots/935650be2e2.png`](plots/935650be2e2.png). High resolution image file: [`plots/935650be2e2-hires.png`](plots/935650be2e2-hires.png).
[![](plots/935650be2e2.png)](plots/935650be2e2-hires.png)


##### \(1.2.1.4.2.1.2.2.4.2.2.2\) Abundance-based richness estimates With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.2.1.4.2.1.2.2.4.2.2.2.1\) <a name="figure.690"></a>[`Figure 690.`](#figure.690) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  bar (sample mean) plot.  Image file: [`plots/9356f5c9741.png`](plots/9356f5c9741.png). High resolution image file: [`plots/9356f5c9741-hires.png`](plots/9356f5c9741-hires.png).
[![](plots/9356f5c9741.png)](plots/9356f5c9741-hires.png)




\(1.2.1.4.2.1.2.2.4.2.2.2.1\) <a name="figure.691"></a>[`Figure 691.`](#figure.691) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  violin plot.  Image file: [`plots/93527ce6585.png`](plots/93527ce6585.png). High resolution image file: [`plots/93527ce6585-hires.png`](plots/93527ce6585-hires.png).
[![](plots/93527ce6585.png)](plots/93527ce6585-hires.png)




\(1.2.1.4.2.1.2.2.4.2.2.2.1\) <a name="figure.692"></a>[`Figure 692.`](#figure.692) Abundance-based richness estimates With rarefication. Data grouped by Sample.type,age.quant.  boxplot plot.  Image file: [`plots/9355f6b132d.png`](plots/9355f6b132d.png). High resolution image file: [`plots/9355f6b132d-hires.png`](plots/9355f6b132d-hires.png).
[![](plots/9355f6b132d.png)](plots/9355f6b132d-hires.png)
