% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



##### \(1.3.1.1.2.3\) Plots of Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'.



**Plots are shown with relation to various combinations of meta 
                   data variables and in different graphical representations. Lots of plots here.**



##### \(1.3.1.1.2.3.2\) Iterating over all combinations of grouping variables




##### \(1.3.1.1.2.3.2.1\) Entire pool of samples




##### \(1.3.1.1.2.3.2.2\) Iterating over Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. profile sorting order




##### \(1.3.1.1.2.3.2.2.1\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. profile sorting order: GeneSelector paired test ranking




##### \(1.3.1.1.2.3.2.2.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.3.1.1.2.3.2.2.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.3.1.1.2.3.2.2.2.1.1\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.3.1.1.2.3.2.2.2.1.1.0\) <a name="table.173"></a>[`Table 173.`](#table.173) Data table used for plots. Data for all pooled samples. Full dataset is also saved in a delimited text file [`data/1.3.1.1.2.3.2.2.2.1.1.0-9354883c634.1.3.1.1.2.3.2.2.2.1.tsv`](data/1.3.1.1.2.3.2.2.2.1.1.0-9354883c634.1.3.1.1.2.3.2.2.2.1.tsv)




|  &nbsp;  | .record.id   | feature                | abundance.diff   |
|:--------:|:-------------|:-----------------------|:-----------------|
|  **1**   | MG1          | Bacteroidetes          | 6.550e-01        |
|  **2**   | MG10         | Bacteroidetes          | 2.868e-01        |
|  **3**   | MG13         | Bacteroidetes          | 4.840e-02        |
|  **4**   | MG14         | Bacteroidetes          | 3.051e-01        |
|  **5**   | MG16         | Bacteroidetes          | 1.563e-01        |
|  **6**   | MG17         | Bacteroidetes          | -4.654e-01       |
|  **7**   | MG19         | Bacteroidetes          | 3.994e-01        |
|  **8**   | MG2          | Bacteroidetes          | -3.468e-01       |
|  **9**   | MG21         | Bacteroidetes          | 1.663e-01        |
|  **10**  | MG22         | Bacteroidetes          | 1.853e-02        |
|  **11**  | MG23         | Bacteroidetes          | 6.049e-01        |
|  **12**  | MG25         | Bacteroidetes          | 8.007e-02        |
|  **13**  | MG3          | Bacteroidetes          | 1.071e-01        |
|  **14**  | MG4          | Bacteroidetes          | 7.753e-02        |
|  **15**  | MG6          | Bacteroidetes          | 1.394e-01        |
|  **16**  | MG8          | Bacteroidetes          | 2.859e-01        |
|  **17**  | MG1          | Firmicutes             | -5.965e-01       |
|  **18**  | MG10         | Firmicutes             | -1.505e-01       |
|  **19**  | MG13         | Firmicutes             | -3.326e-02       |
|  **20**  | MG14         | Firmicutes             | -3.095e-01       |
|  **21**  | MG16         | Firmicutes             | -1.255e-01       |
|  **22**  | MG17         | Firmicutes             | 3.660e-01        |
|  **23**  | MG19         | Firmicutes             | -3.790e-01       |
|  **24**  | MG2          | Firmicutes             | 3.835e-01        |
|  **25**  | MG21         | Firmicutes             | -1.517e-01       |
|  **26**  | MG22         | Firmicutes             | -1.758e-01       |
|  **27**  | MG23         | Firmicutes             | -5.525e-01       |
|  **28**  | MG25         | Firmicutes             | -1.253e-01       |
|  **29**  | MG3          | Firmicutes             | -9.897e-02       |
|  **30**  | MG4          | Firmicutes             | -7.568e-02       |
|  **31**  | MG6          | Firmicutes             | -1.425e-01       |
|  **32**  | MG8          | Firmicutes             | -2.700e-01       |
|  **33**  | MG1          | Proteobacteria         | -8.274e-03       |
|  **34**  | MG10         | Proteobacteria         | -1.212e-01       |
|  **35**  | MG13         | Proteobacteria         | 2.071e-03        |
|  **36**  | MG14         | Proteobacteria         | 9.452e-03        |
|  **37**  | MG16         | Proteobacteria         | -2.791e-02       |
|  **38**  | MG17         | Proteobacteria         | 1.037e-01        |
|  **39**  | MG19         | Proteobacteria         | -1.789e-03       |
|  **40**  | MG2          | Proteobacteria         | -9.936e-03       |
|  **41**  | MG21         | Proteobacteria         | 1.326e-02        |
|  **42**  | MG22         | Proteobacteria         | -4.526e-03       |
|  **43**  | MG23         | Proteobacteria         | -5.351e-02       |
|  **44**  | MG25         | Proteobacteria         | 4.941e-02        |
|  **45**  | MG3          | Proteobacteria         | -6.959e-03       |
|  **46**  | MG4          | Proteobacteria         | 6.906e-03        |
|  **47**  | MG6          | Proteobacteria         | -6.172e-03       |
|  **48**  | MG8          | Proteobacteria         | -7.267e-03       |
|  **49**  | MG1          | Actinobacteria         | -3.821e-02       |
|  **50**  | MG10         | Actinobacteria         | -2.326e-03       |
|  **51**  | MG13         | Actinobacteria         | 4.249e-04        |
|  **52**  | MG14         | Actinobacteria         | 3.389e-04        |
|  **53**  | MG16         | Actinobacteria         | -4.819e-05       |
|  **54**  | MG17         | Actinobacteria         | -6.551e-04       |
|  **55**  | MG19         | Actinobacteria         | -4.201e-03       |
|  **56**  | MG2          | Actinobacteria         | 7.374e-03        |
|  **57**  | MG21         | Actinobacteria         | -4.585e-03       |
|  **58**  | MG22         | Actinobacteria         | 2.086e-02        |
|  **59**  | MG23         | Actinobacteria         | -7.181e-03       |
|  **60**  | MG25         | Actinobacteria         | -2.450e-03       |
|  **61**  | MG3          | Actinobacteria         | -1.321e-03       |
|  **62**  | MG4          | Actinobacteria         | 4.949e-03        |
|  **63**  | MG6          | Actinobacteria         | -2.304e-03       |
|  **64**  | MG8          | Actinobacteria         | -6.715e-03       |
|  **65**  | MG1          | Unclassified\_Bacteria | -1.184e-02       |
|  **66**  | MG10         | Unclassified\_Bacteria | -1.364e-02       |
|  **67**  | MG13         | Unclassified\_Bacteria | -1.100e-03       |
|  **68**  | MG14         | Unclassified\_Bacteria | -5.432e-03       |
|  **69**  | MG16         | Unclassified\_Bacteria | -2.259e-03       |
|  **70**  | MG17         | Unclassified\_Bacteria | -5.929e-03       |
|  **71**  | MG19         | Unclassified\_Bacteria | -1.386e-02       |
|  **72**  | MG2          | Unclassified\_Bacteria | -3.411e-02       |
|  **73**  | MG21         | Unclassified\_Bacteria | -1.595e-02       |
|  **74**  | MG22         | Unclassified\_Bacteria | -7.447e-03       |
|  **75**  | MG23         | Unclassified\_Bacteria | 9.992e-03        |
|  **76**  | MG25         | Unclassified\_Bacteria | -1.314e-03       |
|  **77**  | MG3          | Unclassified\_Bacteria | 1.327e-04        |
|  **78**  | MG4          | Unclassified\_Bacteria | -9.982e-03       |
|  **79**  | MG6          | Unclassified\_Bacteria | 3.134e-03        |
|  **80**  | MG8          | Unclassified\_Bacteria | -1.927e-03       |
|  **81**  | MG1          | Verrucomicrobia        | 0.000e\+00       |
|  **82**  | MG10         | Verrucomicrobia        | 8.029e-04        |
|  **83**  | MG13         | Verrucomicrobia        | 0.000e\+00       |
|  **84**  | MG14         | Verrucomicrobia        | 0.000e\+00       |
|  **85**  | MG16         | Verrucomicrobia        | 0.000e\+00       |
|  **86**  | MG17         | Verrucomicrobia        | 0.000e\+00       |
|  **87**  | MG19         | Verrucomicrobia        | -2.686e-04       |
|  **88**  | MG2          | Verrucomicrobia        | 0.000e\+00       |
|  **89**  | MG21         | Verrucomicrobia        | -7.218e-03       |
|  **90**  | MG22         | Verrucomicrobia        | 1.577e-01        |
|  **91**  | MG23         | Verrucomicrobia        | -2.129e-03       |
|  **92**  | MG25         | Verrucomicrobia        | -3.868e-04       |
|  **93**  | MG3          | Verrucomicrobia        | 8.853e-05        |
|  **94**  | MG4          | Verrucomicrobia        | 0.000e\+00       |
|  **95**  | MG6          | Verrucomicrobia        | 7.979e-03        |
|  **96**  | MG8          | Verrucomicrobia        | 0.000e\+00       |




\(1.3.1.1.2.3.2.2.2.1.1.1\) <a name="table.174"></a>[`Table 174.`](#table.174) Summary table. Data for all pooled samples. Full dataset is also saved in a delimited text file [`data/1.3.1.1.2.3.2.2.2.1.1.1-9354349a51d.1.3.1.1.2.3.2.2.2.1.tsv`](data/1.3.1.1.2.3.2.2.2.1.1.1-9354349a51d.1.3.1.1.2.3.2.2.2.1.tsv)




| feature                | mean      | sd      | median    | incidence   |
|:-----------------------|:----------|:--------|:----------|:------------|
| Unclassified\_Bacteria | -0.006971 | 0.01000 | -0.005680 | 0.1875      |
| Bacteroidetes          | 0.157405  | 0.28868 | 0.147869  | 0.8750      |
| Firmicutes             | -0.152325 | 0.26180 | -0.146474 | 0.1250      |
| Actinobacteria         | -0.002253 | 0.01168 | -0.001812 | 0.3125      |
| Proteobacteria         | -0.003922 | 0.04612 | -0.005349 | 0.3750      |
| Verrucomicrobia        | 0.009786  | 0.03955 | 0.000000  | 0.2500      |







\(1.3.1.1.2.3.2.2.2.1.1.1\) <a name="figure.827"></a>[`Figure 827.`](#figure.827) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/93517f7356e.png`](plots/93517f7356e.png). High resolution image file: [`plots/93517f7356e-hires.png`](plots/93517f7356e-hires.png).
[![](plots/93517f7356e.png)](plots/93517f7356e-hires.png)




\(1.3.1.1.2.3.2.2.2.1.1.1\) <a name="figure.828"></a>[`Figure 828.`](#figure.828) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/93566d4d50c.png`](plots/93566d4d50c.png). High resolution image file: [`plots/93566d4d50c-hires.png`](plots/93566d4d50c-hires.png).
[![](plots/93566d4d50c.png)](plots/93566d4d50c-hires.png)




\(1.3.1.1.2.3.2.2.2.1.1.1\) <a name="figure.829"></a>[`Figure 829.`](#figure.829) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/9351eef3b92.png`](plots/9351eef3b92.png). High resolution image file: [`plots/9351eef3b92-hires.png`](plots/9351eef3b92-hires.png).
[![](plots/9351eef3b92.png)](plots/9351eef3b92-hires.png)


##### \(1.3.1.1.2.3.2.2.2.1.2\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.3.1.1.2.3.2.2.2.1.2.1\) <a name="figure.830"></a>[`Figure 830.`](#figure.830) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. bar_stacked plot.  Image file: [`plots/9356c2ffe1b.png`](plots/9356c2ffe1b.png). High resolution image file: [`plots/9356c2ffe1b-hires.png`](plots/9356c2ffe1b-hires.png).
[![](plots/9356c2ffe1b.png)](plots/9356c2ffe1b-hires.png)




\(1.3.1.1.2.3.2.2.2.1.2.1\) <a name="figure.831"></a>[`Figure 831.`](#figure.831) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/93542d3d61a.png`](plots/93542d3d61a.png). High resolution image file: [`plots/93542d3d61a-hires.png`](plots/93542d3d61a-hires.png).
[![](plots/93542d3d61a.png)](plots/93542d3d61a-hires.png)




\(1.3.1.1.2.3.2.2.2.1.2.1\) <a name="figure.832"></a>[`Figure 832.`](#figure.832) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/935618d673c.png`](plots/935618d673c.png). High resolution image file: [`plots/935618d673c-hires.png`](plots/935618d673c-hires.png).
[![](plots/935618d673c.png)](plots/935618d673c-hires.png)




\(1.3.1.1.2.3.2.2.2.1.2.1\) <a name="figure.833"></a>[`Figure 833.`](#figure.833) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data for all pooled samples. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/935a74c62d.png`](plots/935a74c62d.png). High resolution image file: [`plots/935a74c62d-hires.png`](plots/935a74c62d-hires.png).
[![](plots/935a74c62d.png)](plots/935a74c62d-hires.png)


##### \(1.3.1.1.2.3.2.3\) Grouping variables age.quant




##### \(1.3.1.1.2.3.2.4\) Iterating over Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. profile sorting order




##### \(1.3.1.1.2.3.2.4.1\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. profile sorting order: GeneSelector paired test ranking




##### \(1.3.1.1.2.3.2.4.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.3.1.1.2.3.2.4.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.3.1.1.2.3.2.4.2.1.1\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.3.1.1.2.3.2.4.2.1.1.0\) <a name="table.175"></a>[`Table 175.`](#table.175) Data table used for plots. Data grouped by age.quant. Full dataset is also saved in a delimited text file [`data/1.3.1.1.2.3.2.4.2.1.1.0-935787eb9b7.1.3.1.1.2.3.2.4.2.1.tsv`](data/1.3.1.1.2.3.2.4.2.1.1.0-935787eb9b7.1.3.1.1.2.3.2.4.2.1.tsv)




|  &nbsp;  | .record.id   | age.quant    | feature                | abundance.diff   |
|:--------:|:-------------|:-------------|:-----------------------|:-----------------|
|  **1**   | MG1          | \(15.3,26.4] | Bacteroidetes          | 6.550e-01        |
|  **2**   | MG10         | \(8.71,15.3] | Bacteroidetes          | 2.868e-01        |
|  **3**   | MG13         | \(15.3,26.4] | Bacteroidetes          | 4.840e-02        |
|  **4**   | MG14         | \(15.3,26.4] | Bacteroidetes          | 3.051e-01        |
|  **5**   | MG16         | \(15.3,26.4] | Bacteroidetes          | 1.563e-01        |
|  **6**   | MG17         | \(15.3,26.4] | Bacteroidetes          | -4.654e-01       |
|  **7**   | MG19         | \(6.27,8.71] | Bacteroidetes          | 3.994e-01        |
|  **8**   | MG2          | [3.18,6.27]  | Bacteroidetes          | -3.468e-01       |
|  **9**   | MG21         | \(8.71,15.3] | Bacteroidetes          | 1.663e-01        |
|  **10**  | MG22         | \(8.71,15.3] | Bacteroidetes          | 1.853e-02        |
|  **11**  | MG23         | \(6.27,8.71] | Bacteroidetes          | 6.049e-01        |
|  **12**  | MG25         | \(8.71,15.3] | Bacteroidetes          | 8.007e-02        |
|  **13**  | MG3          | \(8.71,15.3] | Bacteroidetes          | 1.071e-01        |
|  **14**  | MG4          | \(8.71,15.3] | Bacteroidetes          | 7.753e-02        |
|  **15**  | MG6          | \(8.71,15.3] | Bacteroidetes          | 1.394e-01        |
|  **16**  | MG8          | \(8.71,15.3] | Bacteroidetes          | 2.859e-01        |
|  **17**  | MG1          | \(15.3,26.4] | Firmicutes             | -5.965e-01       |
|  **18**  | MG10         | \(8.71,15.3] | Firmicutes             | -1.505e-01       |
|  **19**  | MG13         | \(15.3,26.4] | Firmicutes             | -3.326e-02       |
|  **20**  | MG14         | \(15.3,26.4] | Firmicutes             | -3.095e-01       |
|  **21**  | MG16         | \(15.3,26.4] | Firmicutes             | -1.255e-01       |
|  **22**  | MG17         | \(15.3,26.4] | Firmicutes             | 3.660e-01        |
|  **23**  | MG19         | \(6.27,8.71] | Firmicutes             | -3.790e-01       |
|  **24**  | MG2          | [3.18,6.27]  | Firmicutes             | 3.835e-01        |
|  **25**  | MG21         | \(8.71,15.3] | Firmicutes             | -1.517e-01       |
|  **26**  | MG22         | \(8.71,15.3] | Firmicutes             | -1.758e-01       |
|  **27**  | MG23         | \(6.27,8.71] | Firmicutes             | -5.525e-01       |
|  **28**  | MG25         | \(8.71,15.3] | Firmicutes             | -1.253e-01       |
|  **29**  | MG3          | \(8.71,15.3] | Firmicutes             | -9.897e-02       |
|  **30**  | MG4          | \(8.71,15.3] | Firmicutes             | -7.568e-02       |
|  **31**  | MG6          | \(8.71,15.3] | Firmicutes             | -1.425e-01       |
|  **32**  | MG8          | \(8.71,15.3] | Firmicutes             | -2.700e-01       |
|  **33**  | MG1          | \(15.3,26.4] | Proteobacteria         | -8.274e-03       |
|  **34**  | MG10         | \(8.71,15.3] | Proteobacteria         | -1.212e-01       |
|  **35**  | MG13         | \(15.3,26.4] | Proteobacteria         | 2.071e-03        |
|  **36**  | MG14         | \(15.3,26.4] | Proteobacteria         | 9.452e-03        |
|  **37**  | MG16         | \(15.3,26.4] | Proteobacteria         | -2.791e-02       |
|  **38**  | MG17         | \(15.3,26.4] | Proteobacteria         | 1.037e-01        |
|  **39**  | MG19         | \(6.27,8.71] | Proteobacteria         | -1.789e-03       |
|  **40**  | MG2          | [3.18,6.27]  | Proteobacteria         | -9.936e-03       |
|  **41**  | MG21         | \(8.71,15.3] | Proteobacteria         | 1.326e-02        |
|  **42**  | MG22         | \(8.71,15.3] | Proteobacteria         | -4.526e-03       |
|  **43**  | MG23         | \(6.27,8.71] | Proteobacteria         | -5.351e-02       |
|  **44**  | MG25         | \(8.71,15.3] | Proteobacteria         | 4.941e-02        |
|  **45**  | MG3          | \(8.71,15.3] | Proteobacteria         | -6.959e-03       |
|  **46**  | MG4          | \(8.71,15.3] | Proteobacteria         | 6.906e-03        |
|  **47**  | MG6          | \(8.71,15.3] | Proteobacteria         | -6.172e-03       |
|  **48**  | MG8          | \(8.71,15.3] | Proteobacteria         | -7.267e-03       |
|  **49**  | MG1          | \(15.3,26.4] | Actinobacteria         | -3.821e-02       |
|  **50**  | MG10         | \(8.71,15.3] | Actinobacteria         | -2.326e-03       |
|  **51**  | MG13         | \(15.3,26.4] | Actinobacteria         | 4.249e-04        |
|  **52**  | MG14         | \(15.3,26.4] | Actinobacteria         | 3.389e-04        |
|  **53**  | MG16         | \(15.3,26.4] | Actinobacteria         | -4.819e-05       |
|  **54**  | MG17         | \(15.3,26.4] | Actinobacteria         | -6.551e-04       |
|  **55**  | MG19         | \(6.27,8.71] | Actinobacteria         | -4.201e-03       |
|  **56**  | MG2          | [3.18,6.27]  | Actinobacteria         | 7.374e-03        |
|  **57**  | MG21         | \(8.71,15.3] | Actinobacteria         | -4.585e-03       |
|  **58**  | MG22         | \(8.71,15.3] | Actinobacteria         | 2.086e-02        |
|  **59**  | MG23         | \(6.27,8.71] | Actinobacteria         | -7.181e-03       |
|  **60**  | MG25         | \(8.71,15.3] | Actinobacteria         | -2.450e-03       |
|  **61**  | MG3          | \(8.71,15.3] | Actinobacteria         | -1.321e-03       |
|  **62**  | MG4          | \(8.71,15.3] | Actinobacteria         | 4.949e-03        |
|  **63**  | MG6          | \(8.71,15.3] | Actinobacteria         | -2.304e-03       |
|  **64**  | MG8          | \(8.71,15.3] | Actinobacteria         | -6.715e-03       |
|  **65**  | MG1          | \(15.3,26.4] | Unclassified\_Bacteria | -1.184e-02       |
|  **66**  | MG10         | \(8.71,15.3] | Unclassified\_Bacteria | -1.364e-02       |
|  **67**  | MG13         | \(15.3,26.4] | Unclassified\_Bacteria | -1.100e-03       |
|  **68**  | MG14         | \(15.3,26.4] | Unclassified\_Bacteria | -5.432e-03       |
|  **69**  | MG16         | \(15.3,26.4] | Unclassified\_Bacteria | -2.259e-03       |
|  **70**  | MG17         | \(15.3,26.4] | Unclassified\_Bacteria | -5.929e-03       |
|  **71**  | MG19         | \(6.27,8.71] | Unclassified\_Bacteria | -1.386e-02       |
|  **72**  | MG2          | [3.18,6.27]  | Unclassified\_Bacteria | -3.411e-02       |
|  **73**  | MG21         | \(8.71,15.3] | Unclassified\_Bacteria | -1.595e-02       |
|  **74**  | MG22         | \(8.71,15.3] | Unclassified\_Bacteria | -7.447e-03       |
|  **75**  | MG23         | \(6.27,8.71] | Unclassified\_Bacteria | 9.992e-03        |
|  **76**  | MG25         | \(8.71,15.3] | Unclassified\_Bacteria | -1.314e-03       |
|  **77**  | MG3          | \(8.71,15.3] | Unclassified\_Bacteria | 1.327e-04        |
|  **78**  | MG4          | \(8.71,15.3] | Unclassified\_Bacteria | -9.982e-03       |
|  **79**  | MG6          | \(8.71,15.3] | Unclassified\_Bacteria | 3.134e-03        |
|  **80**  | MG8          | \(8.71,15.3] | Unclassified\_Bacteria | -1.927e-03       |
|  **81**  | MG1          | \(15.3,26.4] | Verrucomicrobia        | 0.000e\+00       |
|  **82**  | MG10         | \(8.71,15.3] | Verrucomicrobia        | 8.029e-04        |
|  **83**  | MG13         | \(15.3,26.4] | Verrucomicrobia        | 0.000e\+00       |
|  **84**  | MG14         | \(15.3,26.4] | Verrucomicrobia        | 0.000e\+00       |
|  **85**  | MG16         | \(15.3,26.4] | Verrucomicrobia        | 0.000e\+00       |
|  **86**  | MG17         | \(15.3,26.4] | Verrucomicrobia        | 0.000e\+00       |
|  **87**  | MG19         | \(6.27,8.71] | Verrucomicrobia        | -2.686e-04       |
|  **88**  | MG2          | [3.18,6.27]  | Verrucomicrobia        | 0.000e\+00       |
|  **89**  | MG21         | \(8.71,15.3] | Verrucomicrobia        | -7.218e-03       |
|  **90**  | MG22         | \(8.71,15.3] | Verrucomicrobia        | 1.577e-01        |
|  **91**  | MG23         | \(6.27,8.71] | Verrucomicrobia        | -2.129e-03       |
|  **92**  | MG25         | \(8.71,15.3] | Verrucomicrobia        | -3.868e-04       |
|  **93**  | MG3          | \(8.71,15.3] | Verrucomicrobia        | 8.853e-05        |
|  **94**  | MG4          | \(8.71,15.3] | Verrucomicrobia        | 0.000e\+00       |
|  **95**  | MG6          | \(8.71,15.3] | Verrucomicrobia        | 7.979e-03        |
|  **96**  | MG8          | \(8.71,15.3] | Verrucomicrobia        | 0.000e\+00       |




\(1.3.1.1.2.3.2.4.2.1.1.1\) <a name="table.176"></a>[`Table 176.`](#table.176) Summary table. Data grouped by age.quant. Full dataset is also saved in a delimited text file [`data/1.3.1.1.2.3.2.4.2.1.1.1-93547d2db2e.1.3.1.1.2.3.2.4.2.1.tsv`](data/1.3.1.1.2.3.2.4.2.1.1.1-93547d2db2e.1.3.1.1.2.3.2.4.2.1.tsv)




|  &nbsp;  | feature                | age.quant    | mean       | sd       | median     | incidence   |
|:--------:|:-----------------------|:-------------|:-----------|:---------|:-----------|:------------|
|  **1**   | Unclassified\_Bacteria | [3.18,6.27]  | -0.0341131 | NA       | -3.411e-02 | 0.000       |
|  **2**   | Unclassified\_Bacteria | \(6.27,8.71] | -0.0019362 | 0.016870 | -1.936e-03 | 0.500       |
|  **3**   | Unclassified\_Bacteria | \(8.71,15.3] | -0.0058737 | 0.006915 | -4.687e-03 | 0.250       |
|  **4**   | Unclassified\_Bacteria | \(15.3,26.4] | -0.0053126 | 0.004187 | -5.432e-03 | 0.000       |
|  **5**   | Bacteroidetes          | [3.18,6.27]  | -0.3468222 | NA       | -3.468e-01 | 0.000       |
|  **6**   | Bacteroidetes          | \(6.27,8.71] | 0.5021451  | 0.145346 | 5.021e-01  | 1.000       |
|  **7**   | Bacteroidetes          | \(8.71,15.3] | 0.1452008  | 0.097580 | 1.232e-01  | 1.000       |
|  **8**   | Bacteroidetes          | \(15.3,26.4] | 0.1398822  | 0.408540 | 1.563e-01  | 0.800       |
|  **9**   | Firmicutes             | [3.18,6.27]  | 0.3834974  | NA       | 3.835e-01  | 1.000       |
|  **10**  | Firmicutes             | \(6.27,8.71] | -0.4657221 | 0.122674 | -4.657e-01 | 0.000       |
|  **11**  | Firmicutes             | \(8.71,15.3] | -0.1488068 | 0.058363 | -1.465e-01 | 0.000       |
|  **12**  | Firmicutes             | \(15.3,26.4] | -0.1397613 | 0.355221 | -1.255e-01 | 0.200       |
|  **13**  | Actinobacteria         | [3.18,6.27]  | 0.0073740  | NA       | 7.374e-03  | 1.000       |
|  **14**  | Actinobacteria         | \(6.27,8.71] | -0.0056908 | 0.002107 | -5.691e-03 | 0.000       |
|  **15**  | Actinobacteria         | \(8.71,15.3] | 0.0007641  | 0.008781 | -2.315e-03 | 0.250       |
|  **16**  | Actinobacteria         | \(15.3,26.4] | -0.0076308 | 0.017102 | -4.819e-05 | 0.400       |
|  **17**  | Proteobacteria         | [3.18,6.27]  | -0.0099361 | NA       | -9.936e-03 | 0.000       |
|  **18**  | Proteobacteria         | \(6.27,8.71] | -0.0276484 | 0.036571 | -2.765e-02 | 0.000       |
|  **19**  | Proteobacteria         | \(8.71,15.3] | -0.0095640 | 0.048952 | -5.349e-03 | 0.375       |
|  **20**  | Proteobacteria         | \(15.3,26.4] | 0.0157999  | 0.051086 | 2.071e-03  | 0.600       |
|  **21**  | Verrucomicrobia        | [3.18,6.27]  | 0.0000000  | NA       | 0.000e\+00 | 0.000       |
|  **22**  | Verrucomicrobia        | \(6.27,8.71] | -0.0011987 | 0.001315 | -1.199e-03 | 0.000       |
|  **23**  | Verrucomicrobia        | \(8.71,15.3] | 0.0198716  | 0.055843 | 4.426e-05  | 0.500       |
|  **24**  | Verrucomicrobia        | \(15.3,26.4] | 0.0000000  | 0.000000 | 0.000e\+00 | 0.000       |







\(1.3.1.1.2.3.2.4.2.1.1.1\) <a name="figure.834"></a>[`Figure 834.`](#figure.834) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/9356433cdd8.png`](plots/9356433cdd8.png). High resolution image file: [`plots/9356433cdd8-hires.png`](plots/9356433cdd8-hires.png).
[![](plots/9356433cdd8.png)](plots/9356433cdd8-hires.png)




\(1.3.1.1.2.3.2.4.2.1.1.1\) <a name="figure.835"></a>[`Figure 835.`](#figure.835) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/93550f534d.png`](plots/93550f534d.png). High resolution image file: [`plots/93550f534d-hires.png`](plots/93550f534d-hires.png).
[![](plots/93550f534d.png)](plots/93550f534d-hires.png)




\(1.3.1.1.2.3.2.4.2.1.1.1\) <a name="figure.836"></a>[`Figure 836.`](#figure.836) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/9353125e6d3.png`](plots/9353125e6d3.png). High resolution image file: [`plots/9353125e6d3-hires.png`](plots/9353125e6d3-hires.png).
[![](plots/9353125e6d3.png)](plots/9353125e6d3-hires.png)


##### \(1.3.1.1.2.3.2.4.2.1.2\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.3.1.1.2.3.2.4.2.1.2.1\) <a name="figure.837"></a>[`Figure 837.`](#figure.837) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. bar_stacked plot.  Image file: [`plots/9352f553fda.png`](plots/9352f553fda.png). High resolution image file: [`plots/9352f553fda-hires.png`](plots/9352f553fda-hires.png).
[![](plots/9352f553fda.png)](plots/9352f553fda-hires.png)




\(1.3.1.1.2.3.2.4.2.1.2.1\) <a name="figure.838"></a>[`Figure 838.`](#figure.838) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/9355d3197d.png`](plots/9355d3197d.png). High resolution image file: [`plots/9355d3197d-hires.png`](plots/9355d3197d-hires.png).
[![](plots/9355d3197d.png)](plots/9355d3197d-hires.png)




\(1.3.1.1.2.3.2.4.2.1.2.1\) <a name="figure.839"></a>[`Figure 839.`](#figure.839) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/935662e6087.png`](plots/935662e6087.png). High resolution image file: [`plots/935662e6087-hires.png`](plots/935662e6087-hires.png).
[![](plots/935662e6087.png)](plots/935662e6087-hires.png)




\(1.3.1.1.2.3.2.4.2.1.2.1\) <a name="figure.840"></a>[`Figure 840.`](#figure.840) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/9356ec37379.png`](plots/9356ec37379.png). High resolution image file: [`plots/9356ec37379-hires.png`](plots/9356ec37379-hires.png).
[![](plots/9356ec37379.png)](plots/9356ec37379-hires.png)


##### \(1.3.1.1.2.3.2.4.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.3.1.1.2.3.2.4.2.2.1\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.3.1.1.2.3.2.4.2.2.1.1\) <a name="figure.841"></a>[`Figure 841.`](#figure.841) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/93561d245be.png`](plots/93561d245be.png). High resolution image file: [`plots/93561d245be-hires.png`](plots/93561d245be-hires.png).
[![](plots/93561d245be.png)](plots/93561d245be-hires.png)




\(1.3.1.1.2.3.2.4.2.2.1.1\) <a name="figure.842"></a>[`Figure 842.`](#figure.842) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/93533e0ef1e.png`](plots/93533e0ef1e.png). High resolution image file: [`plots/93533e0ef1e-hires.png`](plots/93533e0ef1e-hires.png).
[![](plots/93533e0ef1e.png)](plots/93533e0ef1e-hires.png)




\(1.3.1.1.2.3.2.4.2.2.1.1\) <a name="figure.843"></a>[`Figure 843.`](#figure.843) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/9357472b52d.png`](plots/9357472b52d.png). High resolution image file: [`plots/9357472b52d-hires.png`](plots/9357472b52d-hires.png).
[![](plots/9357472b52d.png)](plots/9357472b52d-hires.png)


##### \(1.3.1.1.2.3.2.4.2.2.2\) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.3.1.1.2.3.2.4.2.2.2.1\) <a name="figure.844"></a>[`Figure 844.`](#figure.844) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. bar (sample mean) plot.  Image file: [`plots/93516d4d915.png`](plots/93516d4d915.png). High resolution image file: [`plots/93516d4d915-hires.png`](plots/93516d4d915-hires.png).
[![](plots/93516d4d915.png)](plots/93516d4d915-hires.png)




\(1.3.1.1.2.3.2.4.2.2.2.1\) <a name="figure.845"></a>[`Figure 845.`](#figure.845) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. violin plot.  Image file: [`plots/9356ff7fd68.png`](plots/9356ff7fd68.png). High resolution image file: [`plots/9356ff7fd68-hires.png`](plots/9356ff7fd68-hires.png).
[![](plots/9356ff7fd68.png)](plots/9356ff7fd68-hires.png)




\(1.3.1.1.2.3.2.4.2.2.2.1\) <a name="figure.846"></a>[`Figure 846.`](#figure.846) Abundance difference between paired samples. Samples are paired according to attribute MatchedGroupID, resulting in 16 samples. When fold change or difference is computed, this is done as 'patient by control'. Data grouped by age.quant. Sorting order of features is GeneSelector paired test ranking. boxplot plot.  Image file: [`plots/93521df046.png`](plots/93521df046.png). High resolution image file: [`plots/93521df046-hires.png`](plots/93521df046-hires.png).
[![](plots/93521df046.png)](plots/93521df046-hires.png)
