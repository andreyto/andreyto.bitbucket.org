% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



#### \(1.4.1.4\) Taxonomic level: otu of Subset: Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples



Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


Loaded OTU taxonomy file [`example_cons.taxonomy.file`](example_cons.taxonomy.file).


Loaded 100 records for 2669 features from count file [`example_otu.shared.file`](example_otu.shared.file) for taxonomic level otu 
                       with taxa name sanitize setting TRUE with count basis seq with taxa count source shared


Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


After merging with metadata, 100 records left


After aggregating/subsetting, sample count is: 40


Filtering abundance matrix with arguments [ min_row_sum:2000, max_row_sum:4e+05]. sample


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 33 records for 1580 features


**Wrote counts and metadata for raw counts Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples to files [`data/1.4.1.4.1-9352fa70c6bsamples.raw.16s.l.otu.count.tsv`](data/1.4.1.4.1-9352fa70c6bsamples.raw.16s.l.otu.count.tsv),[`data/1.4.1.4.1-9352fa70c6bsamples.raw.16s.l.otu.attr.tsv`](data/1.4.1.4.1-9352fa70c6bsamples.raw.16s.l.otu.attr.tsv)**


**Wrote counts and metadata for proportions counts Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples to files [`data/1.4.1.4.1-935205d3esamples.proportions.16s.l.otu.count.tsv`](data/1.4.1.4.1-935205d3esamples.proportions.16s.l.otu.count.tsv),[`data/1.4.1.4.1-935205d3esamples.proportions.16s.l.otu.attr.tsv`](data/1.4.1.4.1-935205d3esamples.proportions.16s.l.otu.attr.tsv)**



##### \(1.4.1.4.2\) Data analysis




##### \(1.4.1.4.2.1\) Abundance and diversity estimates Before count filtering

[`Subreport`](./1.4.1.4.2.1-report.html)

Filtering abundance matrix with arguments [ min_mean                :10, min_quant_incidence_frac:0.25, min_quant_mean_frac     :0.25]. feature


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 33 records for 102 features



##### \(1.4.1.4.2.2\) Abundance and diversity estimates After count filtering

[`Subreport`](./1.4.1.4.2.2-report.html)


##### \(1.4.1.4.2.3\) DESeq2 tests and data normalization



Love MI, Huber W and Anders S (2014). “Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2.”
_Genome Biology_, *15*, pp. 550. <URL: http://doi.org/10.1186/s13059-014-0550-8>, <URL:
http://dx.doi.org/10.1186/s13059-014-0550-8>.


\(1.4.1.4.2.3.1\) <a name="table.352"></a>[`Table 352.`](#table.352) DESeq2 results for task:;DietStatus;[ list\(\)];log2 fold change \(MAP\): DietStatus before.diet vs after.diet ;Wald test p-value: DietStatus before.diet vs after.diet . Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.3.1-935661b527d.1.4.1.4.2.3.1.a.nam.tsv`](data/1.4.1.4.2.3.1-935661b527d.1.4.1.4.2.3.1.a.nam.tsv)




|  &nbsp;   | feature                                      | baseMean   | log2FoldChange   | lfcSE   | stat      | pvalue   | padj    |
|:---------:|:---------------------------------------------|:-----------|:-----------------|:--------|:----------|:---------|:--------|
|   **1**   | Unclassified\_Lachnospiraceae.Otu2512        | 2.56052    | -3.148071        | 1.0122  | -3.110052 | 0.001871 | 0.09353 |
|   **2**   | Clostridium\_XlVa.Otu0236                    | 7.74648    | 2.028498         | 0.6488  | 3.126353  | 0.001770 | 0.09353 |
|   **3**   | Lachnospiracea\_incertae\_sedis.Otu1990      | 1.62920    | -3.213785        | 1.1892  | -2.702366 | 0.006885 | 0.20792 |
|   **4**   | Lachnospiracea\_incertae\_sedis.Otu2746      | 1.21215    | -3.111477        | 1.1791  | -2.638925 | 0.008317 | 0.20792 |
|   **5**   | Bacteroides.Otu1201                          | 29.53564   | 2.240898         | 0.8963  | 2.500253  | 0.012410 | 0.24821 |
|   **6**   | Bacteroides.Otu2654                          | 20.44069   | 2.017882         | 0.9885  | 2.041284  | 0.041223 | 0.29445 |
|   **7**   | Erysipelotrichaceae\_incertae\_sedis.Otu0818 | 8.12105    | 1.777147         | 0.8630  | 2.059167  | 0.039478 | 0.29445 |
|   **8**   | Escherichia\_Shigella.Otu2393                | 1.75355    | 3.182001         | 1.5566  | 2.044178  | 0.040936 | 0.29445 |
|   **9**   | Akkermansia.Otu1935                          | 3.52569    | 3.326736         | 1.6100  | 2.066265  | 0.038803 | 0.29445 |
|  **10**   | Unclassified\_Lactobacillales.Otu0504        | 0.50025    | -3.878929        | 1.7867  | -2.171048 | 0.029928 | 0.29445 |
|  **11**   | Lachnospiracea\_incertae\_sedis.Otu0778      | 11.80718   | -2.451484        | 1.0607  | -2.311303 | 0.020816 | 0.29445 |
|  **12**   | Lachnospiracea\_incertae\_sedis.Otu0113      | 4.91126    | -1.815994        | 0.8523  | -2.130755 | 0.033109 | 0.29445 |
|  **13**   | Bacteroides.Otu1976                          | 0.85226    | 3.954396         | 1.9027  | 2.078292  | 0.037683 | 0.29445 |
|  **14**   | Bacteroides.Otu2534                          | 0.72903    | 4.198895         | 1.7983  | 2.334914  | 0.019548 | 0.29445 |
|  **15**   | Unclassified\_Burkholderiales.Otu0023        | 0.72340    | 3.819705         | 1.9039  | 2.006230  | 0.044832 | 0.29633 |
|  **16**   | Unclassified\_Lachnospiraceae.Otu0898        | 2.23394    | 1.387245         | 0.6997  | 1.982595  | 0.047413 | 0.29633 |
|  **17**   | Sutterella.Otu1997                           | 0.47797    | 3.540370         | 1.9065  | 1.857009  | 0.063310 | 0.37241 |
|  **18**   | Prevotella.Otu1803                           | 0.18756    | 3.269618         | 1.8027  | 1.813746  | 0.069717 | 0.38732 |
|  **19**   | Lactobacillus.Otu1332                        | 0.16777    | 2.710888         | 1.6698  | 1.623434  | 0.104497 | 0.42604 |
|  **20**   | Bacteroides.Otu2625                          | 14.59357   | 1.864298         | 1.1484  | 1.623371  | 0.104510 | 0.42604 |
|  **21**   | Clostridium\_XI.Otu1804                      | 13.52753   | 1.439546         | 0.8750  | 1.645242  | 0.099920 | 0.42604 |
|  **22**   | Bacteroides.Otu2120                          | 9.36688    | -1.676657        | 1.0763  | -1.557750 | 0.119293 | 0.42604 |
|  **23**   | Roseburia.Otu2728                            | 1.56050    | -2.120136        | 1.3024  | -1.627887 | 0.103549 | 0.42604 |
|  **24**   | Bacteroides.Otu2460                          | 6.65199    | 1.427431         | 0.8994  | 1.587142  | 0.112480 | 0.42604 |
|  **25**   | Unclassified\_Clostridiales.Otu1597          | 0.24324    | 2.991120         | 1.9123  | 1.564175  | 0.117776 | 0.42604 |
|  **26**   | Unclassified\_Lachnospiraceae.Otu0790        | 4.96322    | 1.582276         | 1.0084  | 1.569095  | 0.116626 | 0.42604 |
|  **27**   | Prevotella.Otu0882                           | 0.36495    | -3.115655        | 1.9095  | -1.631621 | 0.102759 | 0.42604 |
|  **28**   | Bacteroides.Otu2383                          | 0.67757    | 1.292680         | 0.8160  | 1.584218  | 0.113144 | 0.42604 |
|  **29**   | Prevotella.Otu2227                           | 0.20395    | 2.890710         | 1.9134  | 1.510770  | 0.130847 | 0.45120 |
|  **30**   | Clostridium\_XI.Otu2681                      | 1.83095    | 1.837001         | 1.2515  | 1.467813  | 0.142155 | 0.47385 |
|  **31**   | Bacteroides.Otu0480                          | 2.01491    | 2.360216         | 1.6740  | 1.409925  | 0.158562 | 0.48328 |
|  **32**   | Prevotella.Otu2602                           | 0.23571    | 2.625780         | 1.8665  | 1.406812  | 0.159483 | 0.48328 |
|  **33**   | Prevotella.Otu1319                           | 0.16983    | 2.727178         | 1.9153  | 1.423867  | 0.154485 | 0.48328 |
|  **34**   | Parabacteroides.Otu1378                      | 4.72791    | 1.963912         | 1.4412  | 1.362707  | 0.172975 | 0.49637 |
|  **35**   | Blautia.Otu0620                              | 1.34040    | 1.040205         | 0.7647  | 1.360323  | 0.173728 | 0.49637 |
|  **36**   | Bacteroides.Otu0863                          | 16.71224   | 1.229155         | 0.9261  | 1.327170  | 0.184452 | 0.49852 |
|  **37**   | Unclassified\_Lachnospiraceae.Otu2222        | 3.32480    | -1.631281        | 1.2261  | -1.330441 | 0.183373 | 0.49852 |
|  **38**   | Prevotella.Otu1987                           | 2.06060    | 2.016473         | 1.5431  | 1.306726  | 0.191306 | 0.50301 |
|  **39**   | Bacteroides.Otu2065                          | 7.55606    | 2.410385         | 1.8790  | 1.282772  | 0.199572 | 0.50301 |
|  **40**   | Prevotella.Otu2501                           | 0.81700    | -2.367913        | 1.8526  | -1.278129 | 0.201204 | 0.50301 |
|  **41**   | Bacteroides.Otu2216                          | 2.74594    | 2.294364         | 1.8496  | 1.240469  | 0.214802 | 0.50987 |
|  **42**   | Parabacteroides.Otu1107                      | 0.28424    | 2.341586         | 1.9060  | 1.228541  | 0.219244 | 0.50987 |
|  **43**   | Bacteroides.Otu1800                          | 0.29395    | 2.068707         | 1.6764  | 1.234000  | 0.217203 | 0.50987 |
|  **44**   | Faecalibacterium.Otu0751                     | 12.01152   | -0.855421        | 0.7045  | -1.214287 | 0.224638 | 0.51054 |
|  **45**   | Prevotella.Otu2666                           | 0.06977    | -2.014311        | 1.6916  | -1.190767 | 0.233745 | 0.51943 |
|  **46**   | Bacteroides.Otu2520                          | 162.24143  | 1.124205         | 0.9673  | 1.162226  | 0.245144 | 0.53292 |
|  **47**   | Parabacteroides.Otu1736                      | 13.58835   | -1.460233        | 1.3143  | -1.111043 | 0.266550 | 0.56713 |
|  **48**   | Blautia.Otu2475                              | 13.30612   | -0.825347        | 0.7594  | -1.086886 | 0.277087 | 0.57707 |
|  **49**   | Sutterella.Otu0171                           | 3.64024    | -1.704861        | 1.5872  | -1.074132 | 0.282764 | 0.57707 |
|  **50**   | Roseburia.Otu2084                            | 4.79865    | -0.873991        | 0.8389  | -1.041799 | 0.297505 | 0.59501 |
|  **51**   | Prevotella.Otu1563                           | 0.39612    | 1.920315         | 1.8960  | 1.012831  | 0.311141 | 0.61008 |
|  **52**   | Faecalibacterium.Otu0067                     | 65.60238   | -0.688041        | 0.7139  | -0.963843 | 0.335125 | 0.61024 |
|  **53**   | Bacteroides.Otu0848                          | 50.10739   | 0.828329         | 0.8635  | 0.959282  | 0.337416 | 0.61024 |
|  **54**   | Megamonas.Otu2657                            | 0.01208    | 1.314811         | 1.3662  | 0.962414  | 0.335842 | 0.61024 |
|  **55**   | Prevotella.Otu2327                           | 0.84789    | -1.709919        | 1.7985  | -0.950741 | 0.341736 | 0.61024 |
|  **56**   | Bacteroides.Otu1411                          | 0.72624    | 0.911465         | 0.9480  | 0.961461  | 0.336320 | 0.61024 |
|  **57**   | Lachnospiracea\_incertae\_sedis.Otu0651      | 2.25800    | -0.938177        | 1.0193  | -0.920402 | 0.357363 | 0.62695 |
|  **58**   | Bacteroides.Otu2566                          | 0.32551    | -1.272462        | 1.4486  | -0.878421 | 0.379715 | 0.65468 |
|  **59**   | Bacteroides.Otu2375                          | 9.40214    | -0.719203        | 0.8396  | -0.856650 | 0.391638 | 0.66379 |
|  **60**   | Bacteroides.Otu2038                          | 19.27357   | -0.647958        | 0.8322  | -0.778651 | 0.436185 | 0.70057 |
|  **61**   | Gemmiger.Otu0907                             | 2.40672    | -0.736280        | 0.9512  | -0.774030 | 0.438913 | 0.70057 |
|  **62**   | Bacteroides.Otu1854                          | 1.27001    | 0.898832         | 1.1679  | 0.769590  | 0.441543 | 0.70057 |
|  **63**   | Prevotella.Otu0773                           | 0.11771    | -1.459423        | 1.9134  | -0.762722 | 0.445629 | 0.70057 |
|  **64**   | Bacteroides.Otu0752                          | 3.09850    | 0.617155         | 0.8140  | 0.758139  | 0.448368 | 0.70057 |
|  **65**   | Alistipes.Otu2508                            | 11.93770   | -0.903326        | 1.4189  | -0.636643 | 0.524358 | 0.74908 |
|  **66**   | Bacteroides.Otu2765                          | 24.71841   | 0.548352         | 0.8244  | 0.665119  | 0.505974 | 0.74908 |
|  **67**   | Bacteroides.Otu2104                          | 11.75211   | 0.598166         | 0.8753  | 0.683379  | 0.494368 | 0.74908 |
|  **68**   | Blautia.Otu2495                              | 1.92284    | -0.598092        | 0.9287  | -0.644023 | 0.519560 | 0.74908 |
|  **69**   | Dorea.Otu1142                                | 2.85462    | 0.546764         | 0.8253  | 0.662524  | 0.507636 | 0.74908 |
|  **70**   | Bacteroides.Otu2482                          | 1.19787    | 0.991072         | 1.5446  | 0.641627  | 0.521115 | 0.74908 |
|  **71**   | Bacteroides.Otu1260                          | 2.56835    | -0.451258        | 0.7906  | -0.570766 | 0.568158 | 0.80022 |
|  **72**   | Roseburia.Otu1051                            | 6.27862    | 0.418964         | 0.7633  | 0.548862  | 0.583100 | 0.80986 |
|  **73**   | Bacteroides.Otu0006                          | 27.97890   | -0.450058        | 0.8624  | -0.521845 | 0.601778 | 0.82435 |
|  **74**   | other                                        | 431.65501  | 0.171748         | 0.3828  | 0.448716  | 0.653636 | 0.88329 |
|  **75**   | Bacteroides.Otu1669                          | 4.53057    | 0.431386         | 0.9974  | 0.432532  | 0.665355 | 0.88714 |
|  **76**   | Bacteroides.Otu0001                          | 283.40670  | -0.260145        | 0.9191  | -0.283054 | 0.777135 | 0.96943 |
|  **77**   | Bacteroides.Otu0929                          | 16.83663   | -0.206101        | 0.9030  | -0.228231 | 0.819467 | 0.96943 |
|  **78**   | Bacteroides.Otu2431                          | 38.76526   | 0.209284         | 0.8565  | 0.244351  | 0.806959 | 0.96943 |
|  **79**   | Bacteroides.Otu0069                          | 34.13484   | -0.184843        | 0.8804  | -0.209945 | 0.833711 | 0.96943 |
|  **80**   | Bacteroides.Otu0661                          | 34.19711   | -0.322426        | 1.0331  | -0.312091 | 0.754972 | 0.96943 |
|  **81**   | Alistipes.Otu1466                            | 10.85083   | -0.349903        | 1.4960  | -0.233890 | 0.815071 | 0.96943 |
|  **82**   | Unclassified\_Bacteroidales.Otu0696          | 0.27606    | 0.418382         | 1.8289  | 0.228759  | 0.819056 | 0.96943 |
|  **83**   | Unclassified\_Bacteroidales.Otu1168          | 0.51046    | 0.348634         | 1.4356  | 0.242847  | 0.808124 | 0.96943 |
|  **84**   | Bacteroides.Otu0775                          | 3.41934    | 0.220388         | 1.0462  | 0.210648  | 0.833162 | 0.96943 |
|  **85**   | Bacteroides.Otu0312                          | 0.13699    | -0.407327        | 1.4747  | -0.276211 | 0.782386 | 0.96943 |
|  **86**   | Bacteroides.Otu0763                          | 3.10537    | -0.231615        | 0.7178  | -0.322695 | 0.746926 | 0.96943 |
|  **87**   | Bacteroides.Otu0786                          | 8.76973    | 0.154155         | 1.3649  | 0.112942  | 0.910076 | 0.97343 |
|  **88**   | Bacteroides.Otu2343                          | 6.36429    | 0.132393         | 1.2407  | 0.106708  | 0.915021 | 0.97343 |
|  **89**   | Bacteroides.Otu2437                          | 7.17289    | 0.160617         | 1.3227  | 0.121427  | 0.903353 | 0.97343 |
|  **90**   | Bacteroides.Otu0877                          | 8.33918    | 0.150121         | 0.8394  | 0.178841  | 0.858062 | 0.97343 |
|  **91**   | Bacteroides.Otu1624                          | 0.15125    | -0.248934        | 1.2992  | -0.191608 | 0.848049 | 0.97343 |
|  **92**   | Unclassified\_Lachnospiraceae.Otu2599        | 4.35388    | 0.127230         | 0.8066  | 0.157737  | 0.874664 | 0.97343 |
|  **93**   | Prevotella.Otu0634                           | 1.01846    | 0.216679         | 1.5794  | 0.137191  | 0.890880 | 0.97343 |
|  **94**   | Unclassified\_Lachnospiraceae.Otu1734        | 1.83340    | -0.080178        | 0.7486  | -0.107106 | 0.914705 | 0.97343 |
|  **95**   | Enterococcus.Otu1019                         | 0.71062    | 0.109181         | 1.4413  | 0.075752  | 0.939616 | 0.97877 |
|  **96**   | Bacteroides.Otu1565                          | 0.40231    | -0.102911        | 1.3345  | -0.077118 | 0.938530 | 0.97877 |
|  **97**   | Bacteroides.Otu2416                          | 2.18599    | -0.041956        | 0.8313  | -0.050470 | 0.959748 | 0.98384 |
|  **98**   | Turicibacter.Otu0468                         | 0.64340    | 0.072639         | 1.6169  | 0.044925  | 0.964167 | 0.98384 |
|  **99**   | Bacteroides.Otu2069                          | 2.72004    | -0.031784        | 1.2949  | -0.024545 | 0.980418 | 0.99032 |
|  **100**  | Prevotella.Otu1633                           | 2.07183    | -0.006417        | 1.4418  | -0.004451 | 0.996449 | 0.99645 |
|  **101**  | Prevotella.Otu1994                           | 0.00000    | 0.000000         | 0.0000  | 0.000000  | 1.000000 | NA      |
|  **102**  | Unclassified\_Bacteria.Otu0435               | 0.00000    | 0.000000         | 0.0000  | 0.000000  | 1.000000 | NA      |




**Count normalization method for data analysis (unless modified by specific methods) : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.clr"]**



##### \(1.4.1.4.2.4\) GeneSelector stability ranking



Slawski M and Boulesteix. A (2009). _GeneSelector: Stability and Aggregation of ranked gene lists_. R package version
2.18.0.


**Count normalization method for GeneSelector : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.prop"]**


**Wilcoxon test (rank-sum for independent samples and signed-rank for paired samples) 
                   is applied to each feature (feature, gene) on random
                   subsamples of the data. Consensus ranking is found with a
                   Monte Carlo procedure ((method AggregateMC in GeneSelector package). 
                   features ordered according to the consensus ranking
                   are returned, along with the p-values, statistic and effect size 
                   computed on the full
                   original dataset. In a special case when no replications are requested,
                   features are ordered by the adjuested p-value. 
                   P-values are reported with and without the 
                   multiple testing correction of Benjamini & Hochberg. The effect sizes
                   for Wilcoxon tests are reported as: common-language effect
                   size (proportion of pairs where observations from the second group
                   are larger than observations from the first group; no effect
                   corresponds to 0.5); rank-biserial
                   correlation (common language effect size minus its complement; no
                   effect corresponds to 0; range is [-1;1]) and
                   absolute value of r (as defined in Cohen, J. (1988). Statistical power 
                   analysis for the behavioral sciences (2nd ed.). Hillsdale, NJ: Erlbaum.).
                   For paired samples, when calculating the common language effect size,
                   only paired observations are used, and one half of the number of ties is 
                   added to the numerator (Grissom, R. J., and J. J. Kim. "Effect Sizes for Research: Univariate 
                   and Multivariate Applications, 2nd Edn New York." NY: Taylor and Francis (2012)).
                   Logarithm in base 2 of the fold change (l2fc) is also reported if requested.
                   For independent samples, the fold change is computed between the sample means of
                   the groups (last to first). For paired samples - as the sample median of the logfold change
                   in each matched pair of observations.**



##### \(1.4.1.4.2.5\) Stability selection analysis for response ( DietStatus )



\(1.4.1.4.2.5\)  Summary of response variable DietStatus.



```````
 after.diet before.diet 
         16          17 
```````



Hofner B and Hothorn T (2015). _stabs: Stability Selection with Error Control_. R package version R package version
0.5-1, <URL: http://CRAN.R-project.org/package=stabs>.

Hofner B, Boccuto L and Göker M (2014). “Controlling false discoveries in high-dimensional situations: Boosting with
stability selection.” arXiv:1411.1285. <URL: http://arxiv.org/abs/1411.1285>.


**This multivariate feature selection method implements 
                  stability selection procedure by Meinshausen and Buehlmann (2010) 
                  and the improved error bounds by Shah and Samworth (2013). 
                  The features (e.g. taxonomic features)
                   are ranked according to their probability to be selected
                   by models built on multiple random subsamples of the input dataset.**


**Base selection method parameters that were chosen based on
                         cross-validation are: [ alpha:0.001]**


**All base selection method parameters are: [ family     :"binomial", standardize:TRUE, alpha      :0.001]**


**Stability selection method parameters are: [ PFER         :0.05, sampling.type:"SS", assumption   :"r-concave", q            :6]**


\(1.4.1.4.2.5\) <a name="table.353"></a>[`Table 353.`](#table.353) Selection probability for the variables. Probability cutoff=0.83 corresponds to per family error rate PFER=0.0491. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.5-9354d4ee2b5.1.4.1.4.2.5.a.name..tsv`](data/1.4.1.4.2.5-9354d4ee2b5.1.4.1.4.2.5.a.name..tsv)




|                       &nbsp;                       | Prob(selection)   |
|:--------------------------------------------------:|:------------------|
|    **Lachnospiracea\_incertae\_sedis.Otu2746**     | 0.50              |
|    **Lachnospiracea\_incertae\_sedis.Otu1990**     | 0.46              |
|              **Bacteroides.Otu0863**               | 0.35              |
|  **Erysipelotrichaceae\_incertae\_sedis.Otu0818**  | 0.32              |
|           **Clostridium\_XlVa.Otu0236**            | 0.32              |
|     **Unclassified\_Lachnospiraceae.Otu2512**      | 0.29              |
|               **Roseburia.Otu2728**                | 0.26              |
|         **Escherichia\_Shigella.Otu2393**          | 0.22              |
|    **Lachnospiracea\_incertae\_sedis.Otu0778**     | 0.19              |
|     **Unclassified\_Lachnospiraceae.Otu2222**      | 0.17              |
|    **Lachnospiracea\_incertae\_sedis.Otu0113**     | 0.16              |
|             **Lactobacillus.Otu1332**              | 0.15              |
|              **Bacteroides.Otu1201**               | 0.15              |
|              **Bacteroides.Otu2120**               | 0.15              |
|      **Unclassified\_Bacteroidales.Otu1168**       | 0.15              |
|               **Roseburia.Otu2084**                | 0.15              |
|              **Akkermansia.Otu1935**               | 0.12              |
|              **Bacteroides.Otu1260**               | 0.12              |
|              **Bacteroides.Otu0848**               | 0.09              |
|    **Lachnospiracea\_incertae\_sedis.Otu0651**     | 0.09              |
|              **Bacteroides.Otu2375**               | 0.09              |
|               **Sutterella.Otu1997**               | 0.09              |
|            **Parabacteroides.Otu1107**             | 0.09              |
|     **Unclassified\_Lachnospiraceae.Otu0790**      | 0.08              |
|              **Bacteroides.Otu1800**               | 0.08              |
|              **Bacteroides.Otu2038**               | 0.07              |
|              **Bacteroides.Otu0786**               | 0.07              |
|              **Bacteroides.Otu0775**               | 0.07              |
|              **Bacteroides.Otu2654**               | 0.06              |
|              **Bacteroides.Otu2460**               | 0.06              |
|                 **Dorea.Otu1142**                  | 0.06              |
|                **Gemmiger.Otu0907**                | 0.06              |
|     **Unclassified\_Lachnospiraceae.Otu0898**      | 0.06              |
|              **Bacteroides.Otu2482**               | 0.06              |
|              **Bacteroides.Otu0763**               | 0.06              |
|              **Bacteroides.Otu1411**               | 0.06              |
|              **Bacteroides.Otu1565**               | 0.06              |
|                **Blautia.Otu2475**                 | 0.05              |
|              **Bacteroides.Otu0480**               | 0.05              |
|              **Bacteroides.Otu2343**               | 0.05              |
|              **Bacteroides.Otu2437**               | 0.05              |
|               **Prevotella.Otu1994**               | 0.05              |
|              **Bacteroides.Otu1624**               | 0.05              |
|     **Unclassified\_Burkholderiales.Otu0023**      | 0.05              |
|              **Bacteroides.Otu1854**               | 0.05              |
|              **Bacteroides.Otu2566**               | 0.05              |
|              **Bacteroides.Otu0661**               | 0.04              |
|               **Sutterella.Otu0171**               | 0.04              |
|                **Blautia.Otu2495**                 | 0.04              |
|      **Unclassified\_Clostridiales.Otu1597**       | 0.04              |
|         **Unclassified\_Bacteria.Otu0435**         | 0.04              |
|              **Turicibacter.Otu0468**              | 0.04              |
|              **Bacteroides.Otu2534**               | 0.04              |
|              **Bacteroides.Otu2520**               | 0.03              |
|            **Faecalibacterium.Otu0067**            | 0.03              |
|              **Bacteroides.Otu2625**               | 0.03              |
|            **Clostridium\_XI.Otu1804**             | 0.03              |
|              **Bacteroides.Otu2765**               | 0.03              |
|              **Bacteroides.Otu2104**               | 0.03              |
|               **Prevotella.Otu2501**               | 0.03              |
|              **Bacteroides.Otu2069**               | 0.03              |
|              **Bacteroides.Otu2416**               | 0.03              |
|              **Bacteroides.Otu1669**               | 0.03              |
|               **Prevotella.Otu1563**               | 0.03              |
|              **Bacteroides.Otu0312**               | 0.03              |
|              **Bacteroides.Otu0001**               | 0.02              |
|              **Bacteroides.Otu0069**               | 0.02              |
|               **Alistipes.Otu1466**                | 0.02              |
|            **Faecalibacterium.Otu0751**            | 0.02              |
|               **Megamonas.Otu2657**                | 0.02              |
|               **Prevotella.Otu2327**               | 0.02              |
|                **Blautia.Otu0620**                 | 0.02              |
|            **Parabacteroides.Otu1736**             | 0.01              |
|               **Prevotella.Otu1987**               | 0.01              |
|              **Bacteroides.Otu2065**               | 0.01              |
|               **Prevotella.Otu1633**               | 0.01              |
|               **Alistipes.Otu2508**                | 0.01              |
|              **Bacteroides.Otu0006**               | 0.01              |
|               **Prevotella.Otu2602**               | 0.01              |
|               **Prevotella.Otu1803**               | 0.01              |
|               **Prevotella.Otu1319**               | 0.01              |
|            **Parabacteroides.Otu1378**             | 0.01              |
|              **Bacteroides.Otu1976**               | 0.01              |
|               **Prevotella.Otu2666**               | 0.01              |
|               **Prevotella.Otu0882**               | 0.01              |
|               **Prevotella.Otu0634**               | 0.01              |
|     **Unclassified\_Lachnospiraceae.Otu1734**      | 0.01              |
|              **Bacteroides.Otu2383**               | 0.01              |
|               **Prevotella.Otu2227**               | 0.01              |
|              **Enterococcus.Otu1019**              | 0.00              |
|              **Bacteroides.Otu0929**               | 0.00              |
|              **Bacteroides.Otu2431**               | 0.00              |
|              **Bacteroides.Otu2216**               | 0.00              |
|     **Unclassified\_Lactobacillales.Otu0504**      | 0.00              |
|            **Clostridium\_XI.Otu2681**             | 0.00              |
|              **Bacteroides.Otu0877**               | 0.00              |
|               **Roseburia.Otu1051**                | 0.00              |
|      **Unclassified\_Bacteroidales.Otu0696**       | 0.00              |
|     **Unclassified\_Lachnospiraceae.Otu2599**      | 0.00              |
|               **Prevotella.Otu0773**               | 0.00              |
|              **Bacteroides.Otu0752**               | 0.00              |







\(1.4.1.4.2.5\) <a name="figure.1592"></a>[`Figure 1592.`](#figure.1592) Selection probability for the top ranked variables. Probability cutoff=0.83 corresponds to per family error rate PFER=0.0491 (vertical line).  Image file: [`plots/935760600b9.png`](plots/935760600b9.png). High resolution image file: [`plots/935760600b9-hires.png`](plots/935760600b9-hires.png).
[![](plots/935760600b9.png)](plots/935760600b9-hires.png)


##### \(1.4.1.4.2.6\) PermANOVA (adonis) analysis of  normalized counts



Oksanen J, Blanchet FG, Kindt R, Legendre P, Minchin PR, O'Hara RB, Simpson GL, Solymos P, Stevens MHH and Wagner H
(2015). _vegan: Community Ecology Package_. R package version 2.2-1, <URL: http://CRAN.R-project.org/package=vegan>.


**Non-parametric multivariate test for association between
                           normalized counts and meta-data variables. Dissimilarity index is euclidean.**


\(1.4.1.4.2.6\)  Association with diet status unpaired by subject with formula count\~DietStatus .



```````

Call:
adonis(formula = as.formula(formula_str), data = m_a$attr, permutations = n.perm,      method = dist.metr, strata = if (!is.null(strata)) m_a$attr[,          strata] else NULL) 

Permutation: free
Number of permutations: 4000

Terms added sequentially (first to last)

           Df SumsOfSqs MeanSqs F.Model      R2 Pr(>F)
DietStatus  1     445.3  445.31  0.7486 0.02358 0.7628
Residuals  31   18440.8  594.86         0.97642       
Total      32   18886.1                 1.00000       
```````



\(1.4.1.4.2.6\) <a name="table.354"></a>[`Table 354.`](#table.354) Association with diet status unpaired by subject AOV Table. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.6-93547f37262.1.4.1.4.2.6.a.name..tsv`](data/1.4.1.4.2.6-93547f37262.1.4.1.4.2.6.a.name..tsv)




|      &nbsp;      | Df   | SumsOfSqs   | MeanSqs   | F.Model   | R2      | Pr(>F)   |
|:----------------:|:-----|:------------|:----------|:----------|:--------|:---------|
|  **DietStatus**  | 1    | 445.3       | 445.3     | 0.7486    | 0.02358 | 0.7628   |
|  **Residuals**   | 31   | 18440.8     | 594.9     | NA        | 0.97642 | NA       |
|    **Total**     | 32   | 18886.1     | NA        | NA        | 1.00000 | NA       |




\(1.4.1.4.2.6\)  Association with diet status paired by subject with formula count\~DietStatus with strata =  SubjectID.



```````

Call:
adonis(formula = as.formula(formula_str), data = m_a$attr, permutations = n.perm,      method = dist.metr, strata = if (!is.null(strata)) m_a$attr[,          strata] else NULL) 

Blocks:  strata 
Permutation: free
Number of permutations: 4000

Terms added sequentially (first to last)

           Df SumsOfSqs MeanSqs F.Model      R2 Pr(>F)
DietStatus  1     445.3  445.31  0.7486 0.02358 0.1915
Residuals  31   18440.8  594.86         0.97642       
Total      32   18886.1                 1.00000       
```````



\(1.4.1.4.2.6\) <a name="table.355"></a>[`Table 355.`](#table.355) Association with diet status paired by subject AOV Table. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.6-9353fdaa4d5.1.4.1.4.2.6.a.name..tsv`](data/1.4.1.4.2.6-9353fdaa4d5.1.4.1.4.2.6.a.name..tsv)




|      &nbsp;      | Df   | SumsOfSqs   | MeanSqs   | F.Model   | R2      | Pr(>F)   |
|:----------------:|:-----|:------------|:----------|:----------|:--------|:---------|
|  **DietStatus**  | 1    | 445.3       | 445.3     | 0.7486    | 0.02358 | 0.1915   |
|  **Residuals**   | 31   | 18440.8     | 594.9     | NA        | 0.97642 | NA       |
|    **Total**     | 32   | 18886.1     | NA        | NA        | 1.00000 | NA       |




\(1.4.1.4.2.6\)  Association with Drug use before diet and diet status with formula count\~Drug.Before.Diet \*  DietStatus .



```````

Call:
adonis(formula = as.formula(formula_str), data = m_a$attr, permutations = n.perm,      method = dist.metr, strata = if (!is.null(strata)) m_a$attr[,          strata] else NULL) 

Permutation: free
Number of permutations: 4000

Terms added sequentially (first to last)

                            Df SumsOfSqs MeanSqs F.Model      R2  Pr(>F)  
Drug.Before.Diet             1     972.4  972.37 1.66124 0.05149 0.05124 .
DietStatus                   1     445.7  445.68 0.76142 0.02360 0.74131  
Drug.Before.Diet:DietStatus  1     493.5  493.53 0.84316 0.02613 0.63159  
Residuals                   29   16974.5  585.33         0.89878          
Total                       32   18886.1                 1.00000          
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```````



\(1.4.1.4.2.6\) <a name="table.356"></a>[`Table 356.`](#table.356) Association with Drug use before diet and diet status AOV Table. Full dataset is also saved in a delimited text file [`data/1.4.1.4.2.6-9352b7bbcb3.1.4.1.4.2.6.a.name..tsv`](data/1.4.1.4.2.6-9352b7bbcb3.1.4.1.4.2.6.a.name..tsv)




|              &nbsp;               | Df   | SumsOfSqs   | MeanSqs   | F.Model   | R2      | Pr(>F)   |
|:---------------------------------:|:-----|:------------|:----------|:----------|:--------|:---------|
|       **Drug.Before.Diet**        | 1    | 972.4       | 972.4     | 1.6612    | 0.05149 | 0.05124  |
|          **DietStatus**           | 1    | 445.7       | 445.7     | 0.7614    | 0.02360 | 0.74131  |
|  **Drug.Before.Diet:DietStatus**  | 1    | 493.5       | 493.5     | 0.8432    | 0.02613 | 0.63159  |
|           **Residuals**           | 29   | 16974.5     | 585.3     | NA        | 0.89878 | NA       |
|             **Total**             | 32   | 18886.1     | NA        | NA        | 1.00000 | NA       |




**Count normalization method for abundance plots : [ drop.features:"other", method       :"norm.prop"]**



##### \(1.4.1.4.2.7\) Plots of Abundance.

[`Subreport`](./1.4.1.4.2.7-report.html)




\(1.4.1.4.2.7\) <a name="figure.1643"></a>[`Figure 1643.`](#figure.1643) Heatmap of abundance profile.  Image file: [`plots/9355997d1b6.png`](plots/9355997d1b6.png). High resolution image file: [`plots/9355997d1b6-hires.png`](plots/9355997d1b6-hires.png).
[![](plots/9355997d1b6.png)](plots/9355997d1b6-hires.png)


##### \(1.4.1.4.2.8\) Comparison and test of significant difference for profile dissimilarities within and between blocks defined by attribute SubjectID across groups defined by attribute DietStatus



**Count normalization method for Dist.Matr.Within.Between : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.clr"]**


**Dissimilarity index is euclidian. The matrix of 
  dissimilarities D is formed where rows correspond to observations with level after.diet
  of DietStatus, and columns - to level before.diet. The elements of 
  this matrix corresponding to rows and columns with the same 
  level of SubjectID are called "within" block dissimilarities, while
  the elements drawn from combinations of rows and columns
  where SubjectID are not equal are called "between" blocks dissimilarities. The null hypothesis is that the observed difference of "between" and "within" 
  block dissimilarities is consistent with what could be expected 
  if the block structure was assigned to the observations at random. The alternative hypothesis is that the "between"/"within" 
  difference is larger than would have been expected from a random block assignment. We simulate 8000 matrices in which both "between" and "within" 
  dissimilarities come from the null distribution 
  by permuting the SubjectID labels of the columns
  of matrix D. The rank biserial correlation (Grissom, R. J., and J. J. Kim. 
  "Effect Sizes for Research: Univariate 
  and Multivariate Applications, 2nd Edn New York." NY: Taylor and Francis (2012)) is
  computed between the observed "between" and "within" dissimilarities both in the observed and
  simulated samples. Positive values of this correlation statistic would indicate 
  "between" dissimilarities being stochastically larger than "within" dissimilarities.
  The p-value is estimated as the fraction of the simulated statistic values that are as high or higher 
  than the observed value. The estimated p-value was 0.000125 and the observed value of the statistic was 0.632337.**





\(1.4.1.4.2.8\) <a name="figure.1644"></a>[`Figure 1644.`](#figure.1644) Emprical distribution density plots of the 
             profile-profile
             dissimilarities observed between and within SubjectID blocks.
             Distances were computed only across levels of DietStatus variable.  Image file: [`plots/9357e4931ad.png`](plots/9357e4931ad.png). High resolution image file: [`plots/9357e4931ad-hires.png`](plots/9357e4931ad-hires.png).
[![](plots/9357e4931ad.png)](plots/9357e4931ad-hires.png)
