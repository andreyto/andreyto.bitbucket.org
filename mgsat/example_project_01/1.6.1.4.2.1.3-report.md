% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



##### \(1.6.1.4.2.1.3\) Plots of Abundance-based diversity indices (Hill numbers) With rarefication.



**Plots are shown with relation to various combinations of meta 
                   data variables and in different graphical representations. Lots of plots here.**



##### \(1.6.1.4.2.1.3.2\) Iterating over all combinations of grouping variables




##### \(1.6.1.4.2.1.3.2.1\) Grouping variables visit,Drug.Before.Diet




##### \(1.6.1.4.2.1.3.2.2\) Iterating over Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order




##### \(1.6.1.4.2.1.3.2.2.1\) Abundance-based diversity indices (Hill numbers) With rarefication. profile sorting order: original




##### \(1.6.1.4.2.1.3.2.2.2\) Iterating over dodged vs faceted bars



**The same data are shown in multiple combinations of graphical representations. 
                         This is the same data, but each plot highlights slightly different aspects of it.
                         It is not likely that you will need every plot - pick only what you need.**



##### \(1.6.1.4.2.1.3.2.2.2.1\) faceted bars. Iterating over orientation and, optionally, scaling




##### \(1.6.1.4.2.1.3.2.2.2.1.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry



\(1.6.1.4.2.1.3.2.2.2.1.1.0\) <a name="table.399"></a>[`Table 399.`](#table.399) Data table used for plots. Data grouped by visit,Drug.Before.Diet. Full dataset is also saved in a delimited text file [`data/1.6.1.4.2.1.3.2.2.2.1.1.0-9356c635c8f.1.6.1.4.2.1.3.2.2.2.tsv`](data/1.6.1.4.2.1.3.2.2.2.1.1.0-9356c635c8f.1.6.1.4.2.1.3.2.2.2.tsv)




|  &nbsp;   | .record.id   | visit   | Drug.Before.Diet   | feature   | index   |
|:---------:|:-------------|:--------|:-------------------|:----------|:--------|
|   **1**   | SM1          | 1       | TRUE               | N1        | 29.023  |
|   **2**   | SM10         | 3       | TRUE               | N1        | 59.438  |
|   **3**   | SM100        | 4       | TRUE               | N1        | 19.833  |
|   **4**   | SM11         | 2       | TRUE               | N1        | 27.808  |
|   **5**   | SM12         | 1       | TRUE               | N1        | 27.220  |
|   **6**   | SM13         | 1       | FALSE              | N1        | 24.434  |
|   **7**   | SM14         | 1       | FALSE              | N1        | 19.424  |
|   **8**   | SM16         | 4       | FALSE              | N1        | 26.089  |
|   **9**   | SM17         | 1       | TRUE               | N1        | 20.372  |
|  **10**   | SM18         | 2       | FALSE              | N1        | 13.589  |
|  **11**   | SM19         | 2       | FALSE              | N1        | 33.140  |
|  **12**   | SM2          | 1       | FALSE              | N1        | 10.815  |
|  **13**   | SM21         | 1       | FALSE              | N1        | 72.760  |
|  **14**   | SM22         | 1       | TRUE               | N1        | 51.516  |
|  **15**   | SM23         | 2       | TRUE               | N1        | 2.534   |
|  **16**   | SM24         | 1       | FALSE              | N1        | 16.703  |
|  **17**   | SM25         | 4       | FALSE              | N1        | 26.692  |
|  **18**   | SM29         | 3       | TRUE               | N1        | 7.623   |
|  **19**   | SM3          | 2       | TRUE               | N1        | 50.966  |
|  **20**   | SM30         | 4       | TRUE               | N1        | 32.264  |
|  **21**   | SM33         | 1       | TRUE               | N1        | 6.631   |
|  **22**   | SM36         | 4       | FALSE              | N1        | 20.070  |
|  **23**   | SM38         | 2       | TRUE               | N1        | 15.381  |
|  **24**   | SM39         | 1       | FALSE              | N1        | 38.334  |
|  **25**   | SM40         | 4       | FALSE              | N1        | 34.275  |
|  **26**   | SM41         | 1       | TRUE               | N1        | 43.435  |
|  **27**   | SM42         | 4       | FALSE              | N1        | 32.018  |
|  **28**   | SM43         | 4       | TRUE               | N1        | 40.840  |
|  **29**   | SM45         | 2       | FALSE              | N1        | 1.020   |
|  **30**   | SM49         | 1       | TRUE               | N1        | 17.214  |
|  **31**   | SM5          | 3       | TRUE               | N1        | 27.325  |
|  **32**   | SM50         | 3       | FALSE              | N1        | 19.112  |
|  **33**   | SM51         | 4       | TRUE               | N1        | 22.410  |
|  **34**   | SM52         | 1       | FALSE              | N1        | 36.776  |
|  **35**   | SM53         | 2       | TRUE               | N1        | 22.781  |
|  **36**   | SM54         | 3       | TRUE               | N1        | 16.282  |
|  **37**   | SM56         | 2       | TRUE               | N1        | 6.056   |
|  **38**   | SM57         | 1       | TRUE               | N1        | 11.360  |
|  **39**   | SM58         | 2       | FALSE              | N1        | 10.150  |
|  **40**   | SM59         | 3       | FALSE              | N1        | 46.524  |
|  **41**   | SM60         | 3       | TRUE               | N1        | 15.661  |
|  **42**   | SM62         | 1       | TRUE               | N1        | 7.024   |
|  **43**   | SM64         | 2       | TRUE               | N1        | 16.149  |
|  **44**   | SM65         | 3       | TRUE               | N1        | 47.077  |
|  **45**   | SM67         | 4       | TRUE               | N1        | 17.654  |
|  **46**   | SM68         | 4       | FALSE              | N1        | 22.704  |
|  **47**   | SM69         | 4       | TRUE               | N1        | 33.582  |
|  **48**   | SM71         | 3       | TRUE               | N1        | 27.731  |
|  **49**   | SM73         | 1       | FALSE              | N1        | 17.163  |
|  **50**   | SM75         | 3       | FALSE              | N1        | 23.746  |
|  **51**   | SM77         | 2       | TRUE               | N1        | 40.959  |
|  **52**   | SM79         | 2       | FALSE              | N1        | 25.459  |
|  **53**   | SM8          | 1       | FALSE              | N1        | 21.375  |
|  **54**   | SM82         | 4       | TRUE               | N1        | 14.298  |
|  **55**   | SM83         | 1       | TRUE               | N1        | 15.755  |
|  **56**   | SM84         | 1       | TRUE               | N1        | 52.367  |
|  **57**   | SM86         | 2       | TRUE               | N1        | 29.410  |
|  **58**   | SM87         | 1       | TRUE               | N1        | 33.964  |
|  **59**   | SM88         | 2       | TRUE               | N1        | 24.485  |
|  **60**   | SM9          | 2       | FALSE              | N1        | 28.387  |
|  **61**   | SM90         | 2       | FALSE              | N1        | 58.490  |
|  **62**   | SM91         | 3       | TRUE               | N1        | 48.783  |
|  **63**   | SM92         | 3       | FALSE              | N1        | 7.791   |
|  **64**   | SM93         | 1       | TRUE               | N1        | 26.643  |
|  **65**   | SM96         | 1       | FALSE              | N1        | 15.437  |
|  **66**   | SM98         | 3       | FALSE              | N1        | 23.669  |
|  **67**   | SM99         | 3       | FALSE              | N1        | 26.630  |
|  **68**   | SM1          | 1       | TRUE               | N2        | 12.976  |
|  **69**   | SM10         | 3       | TRUE               | N2        | 29.284  |
|  **70**   | SM100        | 4       | TRUE               | N2        | 5.886   |
|  **71**   | SM11         | 2       | TRUE               | N2        | 11.691  |
|  **72**   | SM12         | 1       | TRUE               | N2        | 10.833  |
|  **73**   | SM13         | 1       | FALSE              | N2        | 8.853   |
|  **74**   | SM14         | 1       | FALSE              | N2        | 5.662   |
|  **75**   | SM16         | 4       | FALSE              | N2        | 9.430   |
|  **76**   | SM17         | 1       | TRUE               | N2        | 8.511   |
|  **77**   | SM18         | 2       | FALSE              | N2        | 5.879   |
|  **78**   | SM19         | 2       | FALSE              | N2        | 9.964   |
|  **79**   | SM2          | 1       | FALSE              | N2        | 4.645   |
|  **80**   | SM21         | 1       | FALSE              | N2        | 31.502  |
|  **81**   | SM22         | 1       | TRUE               | N2        | 18.729  |
|  **82**   | SM23         | 2       | TRUE               | N2        | 1.809   |
|  **83**   | SM24         | 1       | FALSE              | N2        | 4.358   |
|  **84**   | SM25         | 4       | FALSE              | N2        | 8.753   |
|  **85**   | SM29         | 3       | TRUE               | N2        | 3.274   |
|  **86**   | SM3          | 2       | TRUE               | N2        | 23.223  |
|  **87**   | SM30         | 4       | TRUE               | N2        | 14.887  |
|  **88**   | SM33         | 1       | TRUE               | N2        | 3.638   |
|  **89**   | SM36         | 4       | FALSE              | N2        | 9.890   |
|  **90**   | SM38         | 2       | TRUE               | N2        | 8.158   |
|  **91**   | SM39         | 1       | FALSE              | N2        | 17.849  |
|  **92**   | SM40         | 4       | FALSE              | N2        | 17.748  |
|  **93**   | SM41         | 1       | TRUE               | N2        | 16.071  |
|  **94**   | SM42         | 4       | FALSE              | N2        | 11.029  |
|  **95**   | SM43         | 4       | TRUE               | N2        | 14.805  |
|  **96**   | SM45         | 2       | FALSE              | N2        | 1.006   |
|  **97**   | SM49         | 1       | TRUE               | N2        | 9.488   |
|  **98**   | SM5          | 3       | TRUE               | N2        | 9.904   |
|  **99**   | SM50         | 3       | FALSE              | N2        | 10.439  |
|  **100**  | SM51         | 4       | TRUE               | N2        | 9.384   |
|  **101**  | SM52         | 1       | FALSE              | N2        | 17.161  |
|  **102**  | SM53         | 2       | TRUE               | N2        | 9.481   |
|  **103**  | SM54         | 3       | TRUE               | N2        | 6.832   |
|  **104**  | SM56         | 2       | TRUE               | N2        | 2.207   |
|  **105**  | SM57         | 1       | TRUE               | N2        | 5.108   |
|  **106**  | SM58         | 2       | FALSE              | N2        | 4.100   |
|  **107**  | SM59         | 3       | FALSE              | N2        | 20.405  |
|  **108**  | SM60         | 3       | TRUE               | N2        | 7.376   |
|  **109**  | SM62         | 1       | TRUE               | N2        | 3.266   |
|  **110**  | SM64         | 2       | TRUE               | N2        | 6.298   |
|  **111**  | SM65         | 3       | TRUE               | N2        | 25.665  |
|  **112**  | SM67         | 4       | TRUE               | N2        | 9.249   |
|  **113**  | SM68         | 4       | FALSE              | N2        | 11.182  |
|  **114**  | SM69         | 4       | TRUE               | N2        | 17.743  |
|  **115**  | SM71         | 3       | TRUE               | N2        | 10.258  |
|  **116**  | SM73         | 1       | FALSE              | N2        | 8.419   |
|  **117**  | SM75         | 3       | FALSE              | N2        | 10.678  |
|  **118**  | SM77         | 2       | TRUE               | N2        | 15.586  |
|  **119**  | SM79         | 2       | FALSE              | N2        | 9.427   |
|  **120**  | SM8          | 1       | FALSE              | N2        | 9.005   |
|  **121**  | SM82         | 4       | TRUE               | N2        | 6.070   |
|  **122**  | SM83         | 1       | TRUE               | N2        | 7.989   |
|  **123**  | SM84         | 1       | TRUE               | N2        | 25.092  |
|  **124**  | SM86         | 2       | TRUE               | N2        | 9.858   |
|  **125**  | SM87         | 1       | TRUE               | N2        | 16.248  |
|  **126**  | SM88         | 2       | TRUE               | N2        | 11.275  |
|  **127**  | SM9          | 2       | FALSE              | N2        | 14.065  |
|  **128**  | SM90         | 2       | FALSE              | N2        | 26.813  |
|  **129**  | SM91         | 3       | TRUE               | N2        | 22.520  |
|  **130**  | SM92         | 3       | FALSE              | N2        | 3.727   |
|  **131**  | SM93         | 1       | TRUE               | N2        | 8.684   |
|  **132**  | SM96         | 1       | FALSE              | N2        | 4.425   |
|  **133**  | SM98         | 3       | FALSE              | N2        | 11.202  |
|  **134**  | SM99         | 3       | FALSE              | N2        | 7.721   |




\(1.6.1.4.2.1.3.2.2.2.1.1.1\) <a name="table.400"></a>[`Table 400.`](#table.400) Summary table. Data grouped by visit,Drug.Before.Diet. Full dataset is also saved in a delimited text file [`data/1.6.1.4.2.1.3.2.2.2.1.1.1-9356f51e7e0.1.6.1.4.2.1.3.2.2.2.tsv`](data/1.6.1.4.2.1.3.2.2.2.1.1.1-9356f51e7e0.1.6.1.4.2.1.3.2.2.2.tsv)




|  &nbsp;  | feature   | visit   | Drug.Before.Diet   | mean   | sd     | median   | incidence   |
|:--------:|:----------|:--------|:-------------------|:-------|:-------|:---------|:------------|
|  **1**   | N1        | 1       | FALSE              | 27.322 | 18.291 | 20.400   | 1           |
|  **2**   | N1        | 1       | TRUE               | 26.348 | 15.510 | 26.643   | 1           |
|  **3**   | N1        | 2       | FALSE              | 24.319 | 18.806 | 25.459   | 1           |
|  **4**   | N1        | 2       | TRUE               | 23.653 | 14.824 | 23.633   | 1           |
|  **5**   | N1        | 3       | FALSE              | 24.579 | 12.641 | 23.708   | 1           |
|  **6**   | N1        | 3       | TRUE               | 31.240 | 18.538 | 27.528   | 1           |
|  **7**   | N1        | 4       | FALSE              | 26.975 | 5.395  | 26.391   | 1           |
|  **8**   | N1        | 4       | TRUE               | 25.840 | 9.783  | 22.410   | 1           |
|  **9**   | N2        | 1       | FALSE              | 11.188 | 8.669  | 8.636    | 1           |
|  **10**  | N2        | 1       | TRUE               | 11.279 | 6.365  | 9.488    | 1           |
|  **11**  | N2        | 2       | FALSE              | 10.179 | 8.485  | 9.427    | 1           |
|  **12**  | N2        | 2       | TRUE               | 9.959  | 6.287  | 9.670    | 1           |
|  **13**  | N2        | 3       | FALSE              | 10.695 | 5.514  | 10.558   | 1           |
|  **14**  | N2        | 3       | TRUE               | 14.389 | 9.872  | 10.081   | 1           |
|  **15**  | N2        | 4       | FALSE              | 11.339 | 3.275  | 10.459   | 1           |
|  **16**  | N2        | 4       | TRUE               | 11.146 | 4.673  | 9.384    | 1           |







\(1.6.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1767"></a>[`Figure 1767.`](#figure.1767) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/9356a4e5c38.png`](plots/9356a4e5c38.png). High resolution image file: [`plots/9356a4e5c38-hires.png`](plots/9356a4e5c38-hires.png).
[![](plots/9356a4e5c38.png)](plots/9356a4e5c38-hires.png)




\(1.6.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1768"></a>[`Figure 1768.`](#figure.1768) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  violin plot.  Image file: [`plots/9353e88970e.png`](plots/9353e88970e.png). High resolution image file: [`plots/9353e88970e-hires.png`](plots/9353e88970e-hires.png).
[![](plots/9353e88970e.png)](plots/9353e88970e-hires.png)




\(1.6.1.4.2.1.3.2.2.2.1.1.1\) <a name="figure.1769"></a>[`Figure 1769.`](#figure.1769) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/93579753834.png`](plots/93579753834.png). High resolution image file: [`plots/93579753834-hires.png`](plots/93579753834-hires.png).
[![](plots/93579753834.png)](plots/93579753834-hires.png)


##### \(1.6.1.4.2.1.3.2.2.2.1.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.6.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1770"></a>[`Figure 1770.`](#figure.1770) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/9357ebd1c37.png`](plots/9357ebd1c37.png). High resolution image file: [`plots/9357ebd1c37-hires.png`](plots/9357ebd1c37-hires.png).
[![](plots/9357ebd1c37.png)](plots/9357ebd1c37-hires.png)




\(1.6.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1771"></a>[`Figure 1771.`](#figure.1771) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  violin plot.  Image file: [`plots/93531879fe2.png`](plots/93531879fe2.png). High resolution image file: [`plots/93531879fe2-hires.png`](plots/93531879fe2-hires.png).
[![](plots/93531879fe2.png)](plots/93531879fe2-hires.png)




\(1.6.1.4.2.1.3.2.2.2.1.2.1\) <a name="figure.1772"></a>[`Figure 1772.`](#figure.1772) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/9353f11c7d5.png`](plots/9353f11c7d5.png). High resolution image file: [`plots/9353f11c7d5-hires.png`](plots/9353f11c7d5-hires.png).
[![](plots/9353f11c7d5.png)](plots/9353f11c7d5-hires.png)


##### \(1.6.1.4.2.1.3.2.2.2.2\) dodged bars. Iterating over orientation and, optionally, scaling




##### \(1.6.1.4.2.1.3.2.2.2.2.1\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in flipped orientation, Y axis not scaled. Iterating over bar geometry






\(1.6.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1773"></a>[`Figure 1773.`](#figure.1773) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/9352856914c.png`](plots/9352856914c.png). High resolution image file: [`plots/9352856914c-hires.png`](plots/9352856914c-hires.png).
[![](plots/9352856914c.png)](plots/9352856914c-hires.png)




\(1.6.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1774"></a>[`Figure 1774.`](#figure.1774) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  violin plot.  Image file: [`plots/9354b5d2944.png`](plots/9354b5d2944.png). High resolution image file: [`plots/9354b5d2944-hires.png`](plots/9354b5d2944-hires.png).
[![](plots/9354b5d2944.png)](plots/9354b5d2944-hires.png)




\(1.6.1.4.2.1.3.2.2.2.2.1.1\) <a name="figure.1775"></a>[`Figure 1775.`](#figure.1775) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/93551405603.png`](plots/93551405603.png). High resolution image file: [`plots/93551405603-hires.png`](plots/93551405603-hires.png).
[![](plots/93551405603.png)](plots/93551405603-hires.png)


##### \(1.6.1.4.2.1.3.2.2.2.2.2\) Abundance-based diversity indices (Hill numbers) With rarefication. Plot is in original orientation, Y axis SQRT scaled. Iterating over bar geometry






\(1.6.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1776"></a>[`Figure 1776.`](#figure.1776) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  bar (sample mean) plot.  Image file: [`plots/93556cf08a1.png`](plots/93556cf08a1.png). High resolution image file: [`plots/93556cf08a1-hires.png`](plots/93556cf08a1-hires.png).
[![](plots/93556cf08a1.png)](plots/93556cf08a1-hires.png)




\(1.6.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1777"></a>[`Figure 1777.`](#figure.1777) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  violin plot.  Image file: [`plots/935323fae8d.png`](plots/935323fae8d.png). High resolution image file: [`plots/935323fae8d-hires.png`](plots/935323fae8d-hires.png).
[![](plots/935323fae8d.png)](plots/935323fae8d-hires.png)




\(1.6.1.4.2.1.3.2.2.2.2.2.1\) <a name="figure.1778"></a>[`Figure 1778.`](#figure.1778) Abundance-based diversity indices (Hill numbers) With rarefication. Data grouped by visit,Drug.Before.Diet.  boxplot plot.  Image file: [`plots/93572dcc4c0.png`](plots/93572dcc4c0.png). High resolution image file: [`plots/93572dcc4c0-hires.png`](plots/93572dcc4c0-hires.png).
[![](plots/93572dcc4c0.png)](plots/93572dcc4c0-hires.png)
