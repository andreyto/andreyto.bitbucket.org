% noone@mail.com
% Analysis of Dieting study 16S data
% Thu Apr 30 12:16:27 2015



#### \(1.5.1.4\) Taxonomic level: otu of Subset: Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples Additional tests



Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


Loaded OTU taxonomy file [`example_cons.taxonomy.file`](example_cons.taxonomy.file).


Loaded 100 records for 2669 features from count file [`example_otu.shared.file`](example_otu.shared.file) for taxonomic level otu 
                       with taxa name sanitize setting TRUE with count basis seq with taxa count source shared


Filtering abundance matrix with arguments [ list()]. 


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 100 records for 2669 features


After merging with metadata, 100 records left


After aggregating/subsetting, sample count is: 40


Filtering abundance matrix with arguments [ min_row_sum:2000, max_row_sum:4e+05]. sample


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 33 records for 1580 features


**Wrote counts and metadata for raw counts Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples Additional tests to files [`data/1.5.1.4.1-935398eca93samples.raw.16s.l.otu.count.tsv`](data/1.5.1.4.1-935398eca93samples.raw.16s.l.otu.count.tsv),[`data/1.5.1.4.1-935398eca93samples.raw.16s.l.otu.attr.tsv`](data/1.5.1.4.1-935398eca93samples.raw.16s.l.otu.attr.tsv)**


**Wrote counts and metadata for proportions counts Patients' samples at visits 1 (before diet) and 2 (after diet), only paired samples Additional tests to files [`data/1.5.1.4.1-9354d95966asamples.proportions.16s.l.otu.count.tsv`](data/1.5.1.4.1-9354d95966asamples.proportions.16s.l.otu.count.tsv),[`data/1.5.1.4.1-9354d95966asamples.proportions.16s.l.otu.attr.tsv`](data/1.5.1.4.1-9354d95966asamples.proportions.16s.l.otu.attr.tsv)**



##### \(1.5.1.4.2\) Data analysis



Filtering abundance matrix with arguments [ min_mean                :10, min_quant_incidence_frac:0.25, min_quant_mean_frac     :0.25]. feature


Note that some community richness estimators will not work correctly 
               if provided with abundance-filtered counts


After filtering, left 33 records for 102 features



##### \(1.5.1.4.2.2\) DESeq2 tests and data normalization



Love MI, Huber W and Anders S (2014). “Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2.”
_Genome Biology_, *15*, pp. 550. <URL: http://doi.org/10.1186/s13059-014-0550-8>, <URL:
http://dx.doi.org/10.1186/s13059-014-0550-8>.


\(1.5.1.4.2.2.1\) <a name="table.368"></a>[`Table 368.`](#table.368) DESeq2 results for task:;Drug.Before.Diet\+DietStatus;[ list\(\)];log2 fold change \(MAP\): DietStatus before.diet vs after.diet ;Wald test p-value: DietStatus before.diet vs after.diet . Full dataset is also saved in a delimited text file [`data/1.5.1.4.2.2.1-9353e1f2a72.1.5.1.4.2.2.1.a.nam.tsv`](data/1.5.1.4.2.2.1-9353e1f2a72.1.5.1.4.2.2.1.a.nam.tsv)




|  &nbsp;   | feature                                      | baseMean   | log2FoldChange   | lfcSE   | stat      | pvalue   | padj   |
|:---------:|:---------------------------------------------|:-----------|:-----------------|:--------|:----------|:---------|:-------|
|   **1**   | Bacteroides.Otu1201                          | 29.53564   | 2.371128         | 0.8838  | 2.682740  | 0.007302 | 0.1557 |
|   **2**   | Unclassified\_Lachnospiraceae.Otu2512        | 2.56052    | -3.072781        | 1.0145  | -3.028863 | 0.002455 | 0.1557 |
|   **3**   | Clostridium\_XlVa.Otu0236                    | 7.74648    | 1.800386         | 0.6446  | 2.793055  | 0.005221 | 0.1557 |
|   **4**   | Lachnospiracea\_incertae\_sedis.Otu1990      | 1.62920    | -2.951120        | 1.1647  | -2.533880 | 0.011281 | 0.1557 |
|   **5**   | Lachnospiracea\_incertae\_sedis.Otu2746      | 1.21215    | -3.076670        | 1.1810  | -2.605076 | 0.009185 | 0.1557 |
|   **6**   | Erysipelotrichaceae\_incertae\_sedis.Otu0818 | 30.40837   | 2.290720         | 0.9578  | 2.391652  | 0.016773 | 0.1929 |
|   **7**   | Lachnospiracea\_incertae\_sedis.Otu0113      | 4.91126    | -1.923520        | 0.8597  | -2.237407 | 0.025260 | 0.2402 |
|   **8**   | Bacteroides.Otu2534                          | 0.72903    | 3.849773         | 1.7503  | 2.199472  | 0.027844 | 0.2402 |
|   **9**   | Bacteroides.Otu2625                          | 49.21106   | 2.346327         | 1.2538  | 1.871413  | 0.061288 | 0.3249 |
|  **10**   | Unclassified\_Bacteroidales.Otu0696          | 16.06182   | -3.410602        | 1.7441  | -1.955525 | 0.050521 | 0.3249 |
|  **11**   | Bacteroides.Otu2120                          | 9.36688    | -1.978565        | 1.0759  | -1.838981 | 0.065918 | 0.3249 |
|  **12**   | Unclassified\_Lachnospiraceae.Otu2222        | 5.11386    | -2.438274        | 1.2029  | -2.026991 | 0.042663 | 0.3249 |
|  **13**   | Dorea.Otu1142                                | 6.53783    | 1.621711         | 0.8762  | 1.850793  | 0.064199 | 0.3249 |
|  **14**   | Bacteroides.Otu1976                          | 0.85226    | 3.418131         | 1.8422  | 1.855502  | 0.063525 | 0.3249 |
|  **15**   | Unclassified\_Burkholderiales.Otu0023        | 0.72340    | 3.303298         | 1.8431  | 1.792212  | 0.073099 | 0.3271 |
|  **16**   | Bacteroides.Otu1854                          | 3.29009    | 2.156011         | 1.2144  | 1.775342  | 0.075842 | 0.3271 |
|  **17**   | Lachnospiracea\_incertae\_sedis.Otu0651      | 9.01596    | -1.854948        | 1.1245  | -1.649505 | 0.099044 | 0.3797 |
|  **18**   | Sutterella.Otu1997                           | 0.47797    | 3.055186         | 1.8451  | 1.655832  | 0.097756 | 0.3797 |
|  **19**   | Unclassified\_Lachnospiraceae.Otu0790        | 4.96322    | 1.637226         | 1.0159  | 1.611637  | 0.107041 | 0.3887 |
|  **20**   | Bacteroides.Otu2460                          | 6.65199    | 1.415396         | 0.9072  | 1.560127  | 0.118730 | 0.3901 |
|  **21**   | Prevotella.Otu0882                           | 0.36495    | -2.923244        | 1.8536  | -1.577102 | 0.114772 | 0.3901 |
|  **22**   | Bacteroides.Otu2383                          | 0.67757    | 1.250648         | 0.8145  | 1.535567  | 0.124645 | 0.3909 |
|  **23**   | Bacteroides.Otu2065                          | 7.55606    | 2.446155         | 1.7891  | 1.367260  | 0.171544 | 0.4471 |
|  **24**   | Bacteroides.Otu2216                          | 2.74594    | 2.365614         | 1.7329  | 1.365134  | 0.172211 | 0.4471 |
|  **25**   | Lachnospiracea\_incertae\_sedis.Otu0778      | 13.16283   | -1.521383        | 1.1028  | -1.379579 | 0.167716 | 0.4471 |
|  **26**   | Parabacteroides.Otu1378                      | 4.72791    | 1.998589         | 1.4459  | 1.382253  | 0.166894 | 0.4471 |
|  **27**   | Bacteroides.Otu2566                          | 0.55677    | -1.861586        | 1.3724  | -1.356490 | 0.174943 | 0.4471 |
|  **28**   | Bacteroides.Otu2520                          | 162.24143  | 1.158345         | 0.9718  | 1.192012  | 0.233257 | 0.5550 |
|  **29**   | Faecalibacterium.Otu0067                     | 65.60238   | -0.821105        | 0.6874  | -1.194485 | 0.232288 | 0.5550 |
|  **30**   | Prevotella.Otu2666                           | 0.06977    | -1.662457        | 1.4539  | -1.143474 | 0.252842 | 0.5815 |
|  **31**   | Roseburia.Otu2084                            | 4.79865    | -0.929477        | 0.8409  | -1.105399 | 0.268987 | 0.5987 |
|  **32**   | Bacteroides.Otu0848                          | 50.10739   | 0.936616         | 0.8638  | 1.084306  | 0.278229 | 0.5999 |
|  **33**   | Prevotella.Otu1987                           | 69.90003   | -1.781272        | 1.6754  | -1.063164 | 0.287707 | 0.6016 |
|  **34**   | Bacteroides.Otu2416                          | 3.35926    | -0.896704        | 0.8960  | -1.000762 | 0.316942 | 0.6271 |
|  **35**   | Bacteroides.Otu1411                          | 0.72624    | 0.933040         | 0.9345  | 0.998429  | 0.318071 | 0.6271 |
|  **36**   | Bacteroides.Otu2343                          | 9.44107    | -1.164722        | 1.2679  | -0.918633 | 0.358287 | 0.6698 |
|  **37**   | Bacteroides.Otu2375                          | 9.40214    | -0.756141        | 0.8246  | -0.916971 | 0.359158 | 0.6698 |
|  **38**   | Akkermansia.Otu1935                          | 3.69114    | 1.385556         | 1.5639  | 0.885987  | 0.375625 | 0.6821 |
|  **39**   | Bacteroides.Otu2069                          | 3.83588    | -1.126534        | 1.3075  | -0.861609 | 0.388903 | 0.6881 |
|  **40**   | Parabacteroides.Otu1107                      | 1.51349    | 1.510771         | 1.8204  | 0.829896  | 0.406597 | 0.7014 |
|  **41**   | Bacteroides.Otu2765                          | 24.71841   | 0.569917         | 0.8286  | 0.687790  | 0.491585 | 0.7514 |
|  **42**   | Bacteroides.Otu2104                          | 11.75211   | 0.594560         | 0.8834  | 0.673003  | 0.500945 | 0.7514 |
|  **43**   | Roseburia.Otu1051                            | 8.15153    | -0.591732        | 0.8123  | -0.728468 | 0.466327 | 0.7514 |
|  **44**   | Unclassified\_Lachnospiraceae.Otu0898        | 3.57535    | -0.597922        | 0.8296  | -0.720709 | 0.471088 | 0.7514 |
|  **45**   | Bacteroides.Otu0752                          | 3.09850    | 0.503569         | 0.7190  | 0.700407  | 0.483673 | 0.7514 |
|  **46**   | Bacteroides.Otu1260                          | 2.56835    | -0.561594        | 0.7918  | -0.709303 | 0.478136 | 0.7514 |
|  **47**   | Unclassified\_Lachnospiraceae.Otu2599        | 6.08076    | -0.485064        | 0.7860  | -0.617162 | 0.537128 | 0.7885 |
|  **48**   | Bacteroides.Otu1669                          | 4.53057    | 0.562436         | 1.0055  | 0.559333  | 0.575934 | 0.8279 |
|  **49**   | other                                        | 431.65501  | 0.198424         | 0.3762  | 0.527493  | 0.597851 | 0.8419 |
|  **50**   | Bacteroides.Otu2437                          | 10.75273   | -0.683352        | 1.3706  | -0.498571 | 0.618082 | 0.8530 |
|  **51**   | Faecalibacterium.Otu0751                     | 15.83964   | -0.307983        | 0.7119  | -0.432637 | 0.665279 | 0.9001 |
|  **52**   | Alistipes.Otu1466                            | 14.19575   | 0.585331         | 1.4836  | 0.394533  | 0.693187 | 0.9025 |
|  **53**   | Blautia.Otu2475                              | 17.39992   | -0.328098        | 0.8045  | -0.407848 | 0.683385 | 0.9025 |
|  **54**   | Bacteroides.Otu0929                          | 53.58636   | 0.273006         | 1.0096  | 0.270404  | 0.786850 | 0.9525 |
|  **55**   | Bacteroides.Otu0069                          | 34.13484   | -0.246897        | 0.7894  | -0.312759 | 0.754464 | 0.9525 |
|  **56**   | Sutterella.Otu0171                           | 7.09507    | -0.447235        | 1.5909  | -0.281128 | 0.778612 | 0.9525 |
|  **57**   | Unclassified\_Lachnospiraceae.Otu1734        | 4.40561    | 0.246322         | 0.8591  | 0.286715  | 0.774330 | 0.9525 |
|  **58**   | Clostridium\_XI.Otu1804                      | 35.14789   | 0.221910         | 1.0196  | 0.217644  | 0.827707 | 0.9847 |
|  **59**   | Bacteroides.Otu2431                          | 38.76526   | 0.145615         | 0.8560  | 0.170112  | 0.864922 | 0.9947 |
|  **60**   | Bacteroides.Otu0877                          | 8.33918    | 0.151960         | 0.8118  | 0.187196  | 0.851507 | 0.9947 |
|  **61**   | Bacteroides.Otu0001                          | 283.40670  | -0.095638        | 0.8636  | -0.110742 | 0.911821 | 0.9982 |
|  **62**   | Bacteroides.Otu0661                          | 34.19711   | -0.005616        | 1.0027  | -0.005601 | 0.995531 | 0.9982 |
|  **63**   | Bacteroides.Otu0786                          | 8.76973    | 0.156111         | 1.2699  | 0.122931  | 0.902162 | 0.9982 |
|  **64**   | Bacteroides.Otu0006                          | 27.97890   | -0.063723        | 0.8247  | -0.077266 | 0.938412 | 0.9982 |
|  **65**   | Bacteroides.Otu1624                          | 0.15125    | 0.014026         | 1.2211  | 0.011487  | 0.990835 | 0.9982 |
|  **66**   | Bacteroides.Otu0775                          | 3.41934    | 0.040816         | 1.0310  | 0.039588  | 0.968422 | 0.9982 |
|  **67**   | Bacteroides.Otu0763                          | 3.10537    | 0.009189         | 0.6353  | 0.014464  | 0.988460 | 0.9982 |
|  **68**   | Blautia.Otu0620                              | 2.95192    | -0.002057        | 0.9342  | -0.002202 | 0.998243 | 0.9982 |
|  **69**   | Bacteroides.Otu1565                          | 0.40231    | -0.014888        | 1.3399  | -0.011112 | 0.991134 | 0.9982 |
|  **70**   | Parabacteroides.Otu1736                      | 13.58835   | -1.114967        | 1.2669  | -0.880102 | NA       | NA     |
|  **71**   | Lactobacillus.Otu1332                        | 18.61283   | -4.502164        | 1.6485  | -2.731005 | NA       | NA     |
|  **72**   | Bacteroides.Otu2038                          | 37.29750   | 0.578906         | 0.9346  | 0.619393  | NA       | NA     |
|  **73**   | Enterococcus.Otu1019                         | 12.88105   | 2.512698         | 1.5582  | 1.612583  | NA       | NA     |
|  **74**   | Bacteroides.Otu0863                          | 56.66263   | 3.237868         | 1.0165  | 3.185315  | NA       | NA     |
|  **75**   | Bacteroides.Otu2654                          | 90.13166   | 3.424679         | 1.1038  | 3.102501  | NA       | NA     |
|  **76**   | Prevotella.Otu1633                           | 77.35999   | -4.239555        | 1.4620  | -2.899743 | NA       | NA     |
|  **77**   | Alistipes.Otu2508                            | 32.46801   | 0.822853         | 1.4757  | 0.557592  | NA       | NA     |
|  **78**   | Escherichia\_Shigella.Otu2393                | 15.97920   | 5.365436         | 1.5371  | 3.490624  | NA       | NA     |
|  **79**   | Bacteroides.Otu0480                          | 4.98101    | -0.952757        | 1.5759  | -0.604565 | NA       | NA     |
|  **80**   | Megamonas.Otu2657                            | 9.08697    | -4.172154        | 1.8368  | -2.271403 | NA       | NA     |
|  **81**   | Prevotella.Otu2602                           | 12.87873   | -2.522403        | 1.8241  | -1.382821 | NA       | NA     |
|  **82**   | Unclassified\_Lactobacillales.Otu0504        | 9.10777    | 2.348854         | 1.7597  | 1.334812  | NA       | NA     |
|  **83**   | Prevotella.Otu1994                           | 0.00000    | 0.000000         | 0.0000  | 0.000000  | 1.000000 | NA     |
|  **84**   | Clostridium\_XI.Otu2681                      | 13.45978   | 2.074183         | 1.3582  | 1.527212  | NA       | NA     |
|  **85**   | Prevotella.Otu1803                           | 10.75817   | -2.533161        | 1.8246  | -1.388314 | NA       | NA     |
|  **86**   | Prevotella.Otu2501                           | 13.11423   | -4.660841        | 1.7679  | -2.636388 | NA       | NA     |
|  **87**   | Prevotella.Otu1319                           | 9.56738    | -2.524246        | 1.8250  | -1.383170 | NA       | NA     |
|  **88**   | Roseburia.Otu2728                            | 4.57693    | -3.237495        | 1.3204  | -2.451978 | NA       | NA     |
|  **89**   | Blautia.Otu2495                              | 6.10363    | -2.387088        | 0.9903  | -2.410538 | NA       | NA     |
|  **90**   | Unclassified\_Clostridiales.Otu1597          | 7.96135    | -0.229029        | 1.8244  | -0.125534 | NA       | NA     |
|  **91**   | Unclassified\_Bacteroidales.Otu1168          | 4.64084    | 2.433191         | 1.5202  | 1.600580  | NA       | NA     |
|  **92**   | Unclassified\_Bacteria.Otu0435               | 5.56445    | 4.179017         | 1.8366  | 2.275351  | NA       | NA     |
|  **93**   | Gemmiger.Otu0907                             | 3.90408    | 0.575839         | 1.0239  | 0.562398  | NA       | NA     |
|  **94**   | Bacteroides.Otu2482                          | 3.06073    | 0.678256         | 1.4845  | 0.456883  | NA       | NA     |
|  **95**   | Prevotella.Otu1563                           | 4.91030    | -1.520983        | 1.8241  | -0.833831 | NA       | NA     |
|  **96**   | Turicibacter.Otu0468                         | 1.70605    | 1.086978         | 1.5895  | 0.683867  | NA       | NA     |
|  **97**   | Prevotella.Otu0634                           | 2.07695    | 1.820084         | 1.4410  | 1.263070  | NA       | NA     |
|  **98**   | Prevotella.Otu2327                           | 4.93177    | -2.247796        | 1.7966  | -1.251157 | NA       | NA     |
|  **99**   | Bacteroides.Otu0312                          | 1.37751    | 1.938684         | 1.5015  | 1.291180  | NA       | NA     |
|  **100**  | Prevotella.Otu0773                           | 5.25313    | -3.593816        | 1.8342  | -1.959363 | NA       | NA     |
|  **101**  | Prevotella.Otu2227                           | 3.13661    | -1.586874        | 1.8255  | -0.869263 | NA       | NA     |
|  **102**  | Bacteroides.Otu1800                          | 0.76689    | 2.990790         | 1.6125  | 1.854707  | NA       | NA     |




**Count normalization method for data analysis (unless modified by specific methods) : [ drop.features:List of 1,  ..$ :"other", method.args  : list(), method       :"norm.clr"]**
